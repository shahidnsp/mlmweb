<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    protected $fillable=['title','description','users_id'];

    public function user(){
        return $this->belongsTo('App\User','users_id');
    }
}
