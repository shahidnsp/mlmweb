<?php
/**
 * Utility.php
 * User: Noushid P
 * Date: 10/3/18
 * Time: 4:19 PM
 */

/**
 * Remove alphetics and special charector from string
 *
*/
function clean($string) {
    $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', '', $string); // Replaces multiple hyphens with single one
}