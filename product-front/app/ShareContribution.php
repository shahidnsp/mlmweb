<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShareContribution extends Model
{
    protected $fillable = ['nodes_id','parentnodes_id', 'orders_id', 'level', 'amount', 'is_coordinator','active'];

    public function node()
    {
        return $this->belongsTo('App\Node','nodes_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order','orders_id');
    }
}
