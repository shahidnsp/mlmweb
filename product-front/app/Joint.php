<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joint extends Model
{
    protected $fillable = [ 'node_id','parentnode_id','amount'];

    public function child()
    {
        return $this->belongsTo('App\Node','node_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Node','parentnode_id');
    }
}
