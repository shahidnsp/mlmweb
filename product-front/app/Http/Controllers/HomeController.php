<?php

namespace App\Http\Controllers;

use Response;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Auth;
use LaravelMsg91;
use Validator;
use App\Node;
use Illuminate\Support\Facades\Mail;
use App\ShareContribution;

use App;
use PDF;
use Illuminate\Support\Facades\Log;

use App\Code;
use Mbarwick83\Shorty\Facades\Shorty;
use App\Product;



class HomeController extends Controller
{
    protected $productUrl = 'http://wayonnproducts.com/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//       $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($promocode="")
    {
        if ($promocode) {
            $promo = Code::where('pro_code', $promocode)->with('user','refer')->get()->first();
//            session()->put('code', $promo);
            session()->put('code', $promocode);
        }
        $products = Product::orderBy('created_at', 'DESC')->with('files')->get();
        return view('web.index', compact('products'));
    }


    public function promoapply(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'promocode' => 'required'
        ]);

        if ($validator->fails()) {
            $output['status'] = false;
            $output['error'] = $validator->errors();
            return response()->json($output, 401);
        }

        $promocode = Code::where('pro_code', $request->promocode);
        if ($promocode->count() > 0) {
            $output['status'] = true;
            $output['promocode'] = $request->promocode;
            session()->put('code', $request->promocode);

            return Response::json($output);
        }else{
            $output['status'] = false;
            $output['message'] = 'not_valid';
            return Response::json($output, 401);
        }
    }

    public function createInvoicePdf($invoice_id)
    {
        $invoice = App\Invoice::where('invoice_no', $invoice_id)->where('downloaded', 0)->with('order.pickup_address', 'product');
        if ($invoice->count() > 0) {
            $data['invoice'] = $invoice->get()->first();
            try{
                $pdf = PDF::loadView('pdf.invoice', $data);
                App\Invoice::where('invoice_no', $invoice_id)->update(['downloaded' => 1]);
                return $pdf->download('invoice.pdf');
            }
            catch(\Exception $e){
                Log::error($e->getMessage() . ' Line Number : ' . $e->getLine());
                Log::error($e->getTraceAsString());
            }
        }

    }



    public function viewInvoice()
    {
        $data['invoice'] = App\Invoice::where('id', 1)->with('order.pickup_address', 'product')->get()->first();

        return view('pdf.invoiceAll', $data);
    }

    public function resetSession(Request $request,$value)
    {
        $request->session()->forget($value);
    }


    /**
     * Add Email to News Letter from Web Site
     */
    public function addMySubscription(Request $request){
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $newsletter=Newsletter::where('email',$request->email)->get()->first();
        if($newsletter!=null){
            return redirect()->back()->with('success', 'Your Already Subscribed..Thank You');
        }

        $newsletter = new Newsletter($request->all());
        if ($newsletter->save()) {
            return redirect()->back()->with('success', 'Thank for Your Subscription');
        }
        return redirect()->back()->withErrors(['error' => ['Please Try Again']]);
    }


    public function test()
    {

        $url = "http://wayonn.in";

        var_dump(Shorty::shorten($url));
        exit;
        $node = Node::where('id', 8)->where('user_id', 13)->with('user');
        if ($node->count() > 0) {
            $data = $node->get()->first();
            $amount = ShareContribution::where('nodes_id', 8)->sum('amount');
//            $partners = Node::where('parentnode_id', 8)->count();

            $partners = Node::where('levels', 'like', '%#' . 8 . '#%')->count();

            $output['name'] = $data->user->name;
            $output['email'] = $data->user->email;
            $output['amount'] = $amount;
            $output['totalPartners'] = $partners;
            $output['refer_code'] = $data->ref_code;
            $output['share_url'] = url('join?ref=' . $data->ref_code);

            $success['status'] = true;
            $success['data'][] = $output;
            return response(['success' => $success]);
        }else{
            $success['status'] = false;
            $success['message'] = 'no_data';
            $success['data'] = [];
            return response(['success' => $success]);
        }
    }
}
