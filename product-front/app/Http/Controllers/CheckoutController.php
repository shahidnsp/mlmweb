<?php

namespace App\Http\Controllers;

use App\Product;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Code;



class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index(Request $request){
        if (session()->has('myproduct_id')) {
            return redirect(route('proceedToPayment'));
        }else
            return redirect('/');
    }

    public function purchase($id){

        Session::put('myproduct_id', $id);
        return redirect('/proceedToPayment');
    }

    public function viewCheckout($product_id)
    {
        if (session()->has('code')) {
            session()->put('myproduct_id', $product_id);
            $promocode = session()->get('code');
            $code = Code::where('pro_code', $promocode)->with('user', 'refer','node')->firstOrFail();
            session()->put('user', $code->user);

            $user = $code->user;
            $product = Product::findOrFail($product_id);
            return view('web.checkout', compact('user','product'));
        }else{
            return redirect('/');
        }
    }
}
