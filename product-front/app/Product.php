<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['name','brand','description','price','offerprice','pdctcode'];

    public function files(){
        return $this->hasMany('App\Photo','products_id');
    }
}
