<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{

    protected $fillable = ['user_id', 'pro_code', 'ref_node_id', 'node_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function refer()
    {
        return $this->belongsTo('App\Node', 'ref_node_id');
    }

    public function node()
    {
        return $this->belongsTo('App\Node', 'node_id');
    }
}
