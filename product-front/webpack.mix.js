let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js')//not using

mix.copyDirectory('resources/assets/front-end/js', 'public/js')
    .copyDirectory('resources/assets/front-end/css', 'public/css')
    .copyDirectory('resources/assets/front-end/img', 'public/img')
    .copyDirectory('resources/assets/front-end/fonts', 'public/fonts');


