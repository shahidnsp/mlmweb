
@extends('web.layouts.app')

@section('title', 'Wayonn | Payment Failed')

@section('content')

        <!-- Page Parallax Header -->
        <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="{{asset('img/backgrounds/shop-header-bg.jpg')}}">
            <div class="ws-overlay">
                <div class="ws-parallax-caption">
                    <div class="ws-parallax-holder">
                        <h1>Payment Failed</h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Parallax Header -->


@endsection



