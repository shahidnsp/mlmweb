<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('images/{filename}', function ($filename)
{
    if (\Illuminate\Support\Facades\Storage::exists($filename)) {
        $file = \Illuminate\Support\Facades\Storage::get($filename);
        return response($file, 200)->header('Content-Type', 'image/jpeg');
    }
    //return base64_encode($file);
});





Route::get('payment/response', function () {
    return redirect('/');
});
Route::post('payment/response', 'OrderController@successResponse');
//Route::post('wayonn/failure_response', 'Auth\OrderController@failureResponse');


Route::get('/payment_failed', function () {
    $data['status'] = false;
    $payment['payment'] = $data;

    session()->forget('user');
    session()->forget('join');
    session()->forget('newuser');
    return redirect('products')->with($payment);
});

Route::get('/payment_success', function () {
    if (session()->has('status') and session()->get('status') == false) {
        $data['status'] = false;
    }else{
        $data['status'] = true;

    }
    $data['phone'] = session('user.phone');
    $data['email'] = session('user.email');
    $data['invoice'] = session('invoice');

    $payment['payment'] = $data;

    session()->forget('user');
    session()->forget('join');
    session()->forget('newuser');
    session()->forget('invoice');
    return redirect('products')->with($payment);
});

//WEB PAGE ROUTE
Route::get('/', 'HomeController@index');

//Route::get('/checkout', 'CheckoutController@index');
Route::get('/purchase/{id}', 'CheckoutController@purchase');



Route::get('home', 'HomeController@index');

Route::get('/single-product/{id}', function ($id) {
    $product=\App\Product::findOrfail($id);
    if($product) {
        $sharelink=Share::load(url('/single-product/'.$id), 'Shop and Earn')->services('twitter', 'facebook', 'pinterest','gplus','linkedin');
        return view('web.single-product', compact('product','sharelink'));
    }
    else
        return view('web.index');
});

Route::get('checkout/{id}', 'CheckoutController@viewCheckout')->name('checkout');


Route::group(['prefix' => 'web'], function () {

    Route::post('addmysubscription', 'HomeController@addMySubscription')->name('addmysubscription');
    Route::post('promocodeApply', 'HomeController@promoapply')->name('promocodeApply');


});


Route::post('proceedToPayment', 'OrderController@proceedToPayment')->name('proceedToPayment');
Route::post('proceedToPayment/validate', 'OrderController@validateInput');

//Route::get('proceedToPayment', function () {
//    var_dump('test');
//    exit;
//    return redirect('/');
//});


Route::get('/test', 'HomeController@test');

Route::get('/{promocode}', 'HomeController@index');

Route::get('way8089/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

