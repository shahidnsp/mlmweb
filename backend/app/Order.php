<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['date', 'orderno', 'amount', 'users_id', 'pickup_addresses_id', 'order_status', 'parentrefer_code', 'status', 'failurereason', 'paymentstatus', 'transaction_id', 'order_status', 'node_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }

    public function pickup_address()
    {
        return $this->belongsTo('App\PickupAddress', 'pickup_addresses_id');
    }
    public function order_items()
    {
        return $this->hasMany('App\OrderItem', 'orders_id');
    }

    public function invoice()
    {
        return $this->hasOne('App\Invoice');
    }
}
