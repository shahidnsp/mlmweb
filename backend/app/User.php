<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'lastname',
        'username',
        'email',
        'pancard',
        'father',
        'dateofbirth',
        'gender',
        'phone',
        'parentcode',
        'pincode',
        'postoffice',
        'state',
        'district',
        'housename',
        'taluk',
        'address',
        'city',
        'nominee',
        'nomineerelation',
        'ifsccode',
        'accountno',
        'bankname',
        'branch',
        'pancopy',
        'bankproof',
        'prooftype',
        'proofnumber',
        'proofcopy',
        'placement',
        'placement_id',
        'usertype',
        'refercode',
        'secretecode',
        'photo',
        'active',
        'parent_id',
        'default_pwd',
        'password',
        'created_at',
        'updated_at',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

   /* public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] =bcrypt($password);
    }*/

    public function nodes()
    {
        return $this->hasMany('App\Node','user_id');
    }
}
