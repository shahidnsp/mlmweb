<?php

namespace App\Http\Controllers\Auth;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class ContactController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'description' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return Contact::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $contact = new Contact($request->all());
        if ($contact->save()) {
            return $contact;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $contact = Contact::find($id);
        if ($contact->update($request->all())) {
            return $contact;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Contact::destroy($id)) {
            return Response::json(array('msg' => 'Contact record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changeContactStatus(Request $request){
        $id=$request->id;
        $contact=Contact::findOrfail($id);
        if($contact){
            if($contact->notified==1)
                $contact->notified=0;
            else
                $contact->notified=1;
            if($contact->save())
                return $contact;
        }
    }

    public function sendMessage(Request $request){
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $contact = new Contact($request->all());
        if ($contact->save()) {
            return redirect()->back()->with('success', 'Thank You..We Contact You Shortly');
        }
        return redirect()->back()->withErrors(['error' => ['Please Try Again']]);
    }
}
