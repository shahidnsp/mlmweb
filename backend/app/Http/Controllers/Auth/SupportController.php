<?php

namespace App\Http\Controllers\Auth;

use App\Support;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class SupportController extends Controller
{

    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required',
            'description' => 'required',
            'user_id' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Support::with('user')->orderBy('notified','asc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $support = new Support($request->all());
        if ($support->save()) {
            return $support;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $support = Support::find($id);
        if ($support->update($request->all())) {
            return $support;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Support::destroy($id)) {
            return Response::json(array('msg' => 'Support record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changeSupportStatus(Request $request){
        $id=$request->id;
        $support=Support::findOrfail($id);
        if($support){
            if($support->notified==1)
                $support->notified=0;
            else
                $support->notified=1;
            if($support->save())
                return $support;
        }
    }
}
