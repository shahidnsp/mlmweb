<?php

namespace App\Http\Controllers\Auth;

use App\Newsletter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Validator;
use Response;

class NewsletterController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Newsletter::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $newsletter = new Newsletter($request->all());
        if ($newsletter->save()) {
            return $newsletter;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $newsletter = Newsletter::find($id);
        if ($newsletter->update($request->all())) {
            return $newsletter;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Newsletter::destroy($id)) {
            return Response::json(array('msg' => 'Newsletter record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    /**
     * Change Status of News Letter Email
     */
    public function changeNewsLetterStatus(Request $request){
        $id=$request->id;
        $newsletter=Newsletter::findOrfail($id);
        if($newsletter){
            if($newsletter->active==1)
                $newsletter->active=0;
            else
                $newsletter->active=1;
            if($newsletter->save())
                return $newsletter;
        }
    }

    /**
     * Send News Letter to Email List
     */
    public function sendNewsLetter(Request $request){
        $message=$request->message;
        $subject=$request->subject;


        $data['html']=$message;
        $emails=Newsletter::all();
        foreach($emails as $email) {
            Mail::send('email.send', ['data' => $data], function ($message) use ($subject,$email) {
                $message->from('noreply@scienceinstituteonline.in', 'WayOnn Marketing LLP');
                $message->to($email->email, '')->subject($subject);
            });
        }

        return 'Ok';
    }

    /**
     * Add Email to News Letter from Web Site
     */
    public function addMySubscription(Request $request){
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $newsletter=Newsletter::where('email',$request->email)->get()->first();
        if($newsletter!=null){
            return redirect()->back()->with('success', 'Your Already Subscribed..Thank You');
        }

        $newsletter = new Newsletter($request->all());
        if ($newsletter->save()) {
            return redirect()->back()->with('success', 'Thank for Your Subscription');
        }
        return redirect()->back()->withErrors(['error' => ['Please Try Again']]);
    }
}
