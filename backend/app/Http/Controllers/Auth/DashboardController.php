<?php
/**
 * DashboardController.php
 * User: Noushid P
 * Date: 17/3/18
 * Time: 7:57 PM
 */

namespace App\Http\Controllers\Auth;
use App\Contact;
use App\Node;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use App\Order;
use App\User;
use Illuminate\Support\Facades\DB;
use App\ShareContribution;
use Carbon\Carbon;

/**
 *
 */
class DashboardController extends Controller
{
    /**
     *
     */
    public function searchReferId(Request $request)
    {
        if ($request->get('field') == 'referCode') {
            $node = Node::where('ref_code', $request->value);
            if ($node->count() > 0) {
                return $node->get()->first();
            }
        }
    }


    /**
     *
     * Load total income,members,etc
    */
    public function utility()
    {
        $total_income = Order::where('paymentstatus', 1)->sum('amount');
        $users = User::whereNotIn('id', [1, 3, 4])->count();
        $nodes = Node::whereNotIn('id', [1, 5])->count();
        $orders = Order::where('paymentstatus', 1)->count();
        $today_order = Order::whereDate('created_at', DB::raw('CURDATE()'))->count();
        $share = ShareContribution::sum('amount');

        $output['total_income'] = $total_income;
        $output['users'] = $users;
        $output['nodes'] = $nodes;
        $output['orders'] = $orders;
        $output['today_order'] = $today_order;
        $output['share'] = $share;

        return $output;

    }

    /**
     * Get chart data
     *
     * @return Response;
     */
    public function chartData(Request $request)
    {
        if ($request->period == 'last-six-month') {
            $data = Node::where("created_at", ">", Carbon::now()->subMonths(6))->groupBy(DB::raw())->count();
            var_dump($data);
        }
        if ($request->period == 'last-week') {
            $now = Carbon::now();
            $weekStart = $now->subDays($now->dayOfWeek)->setTime(0, 0);
            $data = Node::where('created_at', '>=', $weekStart)->count();
        }
        if ($request->period == 'last-month') {
            $today = Carbon::now();
            var_dump($today->startOfMonth());
            exit;
            $data = Node::whereDat('created_at', [$today->startOfMonth(), $today->endOfMonth()])->count();
            var_dump($data);
        }
    }
}
