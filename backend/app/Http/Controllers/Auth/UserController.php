<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpParser\Node\Expr\Array_;
use Psy\Exception\RuntimeException;
use Illuminate\Support\Carbon;
use Validator;
use Response;
use App\Node;
use App\ShareContribution;
class UserController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'address' => 'required',
            'place' => 'required',
            'state' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:8|max:15',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:14|unique:users,phone',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->type == 'executive') {
            $users= User::where('usertype', 'executive')->withCount('nodes')->get();
            foreach ($users as $user) {
                $user->total_partners = Node::where('levels', 'like', '%#' . $user->id . '#%')->count();
            }
            return Response::json($users);
        }
        elseif ($request->type == 'user') {
            $users = User::where('usertype', 'user')->withCount('nodes')->get();
            foreach ($users as $user) {
                $user->total_partners = Node::where('levels', 'like', '%#' . $user->id . '#%')->count();
            }
            return Response::json($users);
        }
        elseif ($request->type == 'both') {
            $data['users'] = User::where('usertype', 'user')->withCount('nodes')->get();
            foreach ($data['users'] as $value) {
                $value->total_partners = Node::where('levels', 'like', '%#' . $value->id . '#%')->count();
            }
            $data['executives'] = User::where('usertype', 'executive')->withCount('nodes')->get();

            foreach ($data['executives'] as $value1) {
                $value1->total_partners = Node::where('levels', 'like', '%#' . $value1->id . '#%')->count();
            }

            return Response::json($data);
        }
        else{
            return User::get();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//var_dump($request->all());
//        exit;
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $password = $request->password;
        $request->merge(['password' => bcrypt($request->password)]);

        $user = new User($request->all());

//        $refercode=$this->randomString(10);
//        if($this->checkReferCodeExist($refercode)){
//            $user->refercode=$refercode;
//        }else{
//            $istrue=true;
//            while($istrue){
//                $refercode=$this->randomString(10);
//                if($this->checkReferCodeExist($refercode)){
//                    $user->refercode=$refercode;
//                    $istrue=false;
//                }
//            }
//        }

//        $secretecode=$this->randomString(15);

//        if($this->checkSecretCodeExist($secretecode)){
//            $user->secretecode=$secretecode;
//        }else{
//            $istrue=true;
//            while($istrue){
//                $secretecode=$this->randomString(15);
//                if($this->checkSecretCodeExist($secretecode)){
//                    $user->secretecode=$secretecode;
//                    $istrue=false;
//                }
//            }
//        }

        $user->photo='profile.png';


        if ($user->save()) {
            if ($user->usertype == 'executive') {

                $last_node = Node::orderBy('created_at', 'desc')->where('user_id', $user->id);
                if ($last_node->count() > 0) {
                    $node_name = 'Account-' . (int)$last_node->first()->node_name + 1;
                }else{
                    $node_name = 'Account-1';
                }
                /*Generate Ref_code*/
                while (1) {
                    $ref_code = str_random(6);
                    if (Node::where('ref_code', $ref_code)->count() < 1){
                        break;
                    }
                }
                /*end*/

                $node_data = [];
                $node_data['node_name'] = $node_name;
                $node_data['user_id'] = $user->id;
                $node_data['ref_code'] = $ref_code;
                $node_data['levels'] = '';
                $node_data['active'] = 1;

                $node = new Node($node_data);
                $node->save();

            }
            return $user;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Generate a more truly "random" alpha-numeric string.
     *
     * @param  int  $length
     * @return string
     *
     * @throws \RuntimeException
     */
    private  function randomString($length = 16)
    {
        if ( ! function_exists('openssl_random_pseudo_bytes'))
        {
            throw new RuntimeException('OpenSSL extension is required.');
        }

        $bytes = openssl_random_pseudo_bytes($length * 2);

        if ($bytes === false)
        {
            throw new RuntimeException('Unable to generate random string.');
        }

        return substr(str_replace(array('/', '+', '='), '', base64_encode($bytes)), 0, $length);
    }

    /***
     *
     * Generate Refer Code
     */
    private function checkReferCodeExist($code){
        $users=User::where('refercode',$code)->get()->count();
        if($users==0)
            return true;
        else
            return false;
    }

    /**
     * Generate Secret Code
     */
    private function checkSecretCodeExist($code){
        $users=User::where('secretecode',$code)->get()->count();
        if($users==0)
            return true;
        else
            return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        /**
         * Validate the Request using own validation method
         *
         */
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $request->id,
            'username' => 'required|unique:users,username,' . $request->id,
            'password' => 'nullable|min:8|max:15',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:14|unique:users,phone,' . $request->id,
        ]);

        if ($request->password) {
            $password = $request->password;
            $request->merge(['password' => bcrypt($request->password)]);
        }

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $user = User::find($id);
        if ($user->update($request->all())) {
            return $user;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (User::destroy($id)) {
            return Response::json(array('msg' => 'Member record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changeUserStatus(Request $request){
        $id=$request->id;
        $user=User::findOrfail($id);
        if($user){
            if($user->active==1)
                $user->active=0;
            else
                $user->active=1;
            if($user->save())
                return $user;
        }
    }


    public function countUser()
    {
        $user=User::where(['name' => 'user'])->get()->count();

        if($user)
            return $user;

        else
            return Response::json(array('error' => 'Record Not found'), 400);
    }

    public function Today_Users()
    {
        $mytime = Carbon::now();
        $date=$mytime->toDateTimeString();

        $users=User::where(['created_at' => $date])->get()->count();

        if($users>=0)
            return $users;

        else
            return Response::json(array('error' => 'Record Not found'), 400);
    }


    public function getDetails(Request $request)
    {
        $child = Node::where('parentnode_id', $request->id);
        if ($child->count() > 0) {
            $data['directpartners'] = $child->count();
        }
        $data['totalpartners'] = Node::where('levels', 'like', '%#' . $request->id . '#%')->count();
        $data['income'] = ShareContribution::where('nodes_id', $request->id)->sum('amount');
        return Response::json($data);
    }


}
