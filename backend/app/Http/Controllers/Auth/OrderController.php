<?php

namespace App\Http\Controllers\Auth;

use App\Helper\MyShare;
use App\Joint;
use App\Node;
use App\Order;
use App\OrderItem;
use App\PickupAddress;
use App\Product;
use App\ShareContribution;
use Faker\Provider\zh_CN\DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use Indipay;
use App\User;
use LaravelMsg91;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App;
use PDF;
use Illuminate\Support\Facades\Log;
use App\Invoice;

class OrderController extends Controller
{

    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'user_id' => 'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return Order::with('user')->with('pickup_address')->where('paymentstatus', 1)->with('order_items.product')->get();
        return Order::with('user')->with('pickup_address')->where('paymentstatus', 1)->where('order_status', 'new')->with('order_items.product', 'invoice')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $order = new Order($request->all());
        if ($order->save()) {
            return $order;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $order = Order::find($id);
        if ($order->update($request->all())) {
            return $order;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Order::destroy($id)) {
            return Response::json(array('msg' => 'Order record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }


    public function changeOrderStatus(Request $request)
    {
        $id = $request->id;
        $order = Order::findOrfail($id);
        if($order){
            $order->order_status = $request->status;
            if ($order->save()) {
                return $order;
            }
        }
    }

    public function searchOrder(Request $request)
    {
        if ($request->status == 'all') {
            return Order::with('user')->with('pickup_address')->where('paymentstatus', 1)->with('order_items.product','invoice')->get();
        }
        if ($request->status == 'today') {
            return Order::whereDate('created_at', DB::raw('CURDATE()'))->with('user')->with('pickup_address')->where('paymentstatus', 1)->where('order_status', 'new')->with('order_items.product','invoice')->get();
        }

        if ($request->status == 'neworder') {
            return Order::where('paymentstatus', 1)->where('order_status', 'new')->with('user')->with('pickup_address')->with('order_items.product','invoice')->get();
        }

        if ($request->status == 'packing') {
            return Order::where('paymentstatus', 1)->where('order_status', 'packing')->with('user')->with('pickup_address')->with('order_items.product','invoice')->get();
        }


        if ($request->status == 'packing') {
            return Order::where('paymentstatus', 1)->where('order_status', 'packing')->with('user')->with('pickup_address')->with('order_items.product','invoice')->get();
        }

        if ($request->status == 'shipping') {
            return Order::where('paymentstatus', 1)->where('order_status', 'shipping')->with('user')->with('pickup_address')->with('order_items.product','invoice')->get();
        }
        if ($request->status == 'delivered') {
            return Order::where('paymentstatus', 1)->where('order_status', 'delivered')->with('user')->with('pickup_address')->with('order_items.product','invoice')->get();
        }
        if ($request->status == 'canceled') {
            return Order::where('paymentstatus', 1)->where('order_status', 'canceled')->with('user')->with('pickup_address')->with('order_items.product','invoice')->get();
        }

        return Order::where('status', $request->status)->with('user')->with('pickup_address')->get();

    }


    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function ValidatorPayment(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'postoffice' => 'required',
            'district' => 'required',
            'state' => 'required',
            'pincode' => 'required',
            'products_id' => 'required',
        ]);
    }

    /**
     * Proceed To Payment after select product...........................
     */
    public function proceedToPayment(Request $request){

        $validator = $this->ValidatorPayment($request->all());
        if ($validator->fails()) {
//            return Response::json($validator->errors(), 400);
            return redirect()->back()->withInput();
        }

        $phone = session()->get('user.phone');
        $parent_refer = session()->get('join.ref_code');
        if (session()->has('join.node_id')) {
            $user = User::where('phone', $phone);
            if ($user->count() == 0) {
                $confirmation_code = str_random(30);

                $password = str_random(8);
                $user_data = $request->all();

                $user_data['password'] = bcrypt($password);
                $user_data['username'] = $request->email;
                $user_data['usertype'] = 'user';
                $user_data['confirmation_code'] = $confirmation_code;

                $user = new User($user_data);

                try {
                    $user->save();
                    $result = LaravelMsg91::message($user->phone, 'Thank you for registration. Your Username : ' . $user_data['username'] . ' And Password : ' . $password . '. Download our Application and manage your network. https://goo.gl/dEdv6T');

                    if ($result->type != 'success') {
                        throw new \Exception("Message sending failed");
                    }

                } catch (\Exception $e) {
                    Log::error($e->getMessage() . ' Line Number : ' . $e->getLine());
                    Log::error($e->getTraceAsString());
                    $user->destroy($user->id);
                    return redirect('checkout')->withInput()->with(['status' => false, 'message' => 'Sorry! Something Went wrong.Please try again']);
                }

                session()->put('newuser.create', true);
                session()->put('newuser.password', $password);

                $mail_data['name'] = $user->name;
                $mail_data['email'] = $user->email;
                $mail_data['url'] = url('verify-email/' . $confirmation_code);

                Mail::send('email.emailVerification', $mail_data, function ($message) use ($mail_data) {
                    $message->to($mail_data['email'], $mail_data['name'])->subject('Wayonn Verification');
                });
            } else {
                $user = $user->get()->first();
                session()->put('newuser.create', false);
            }

            session()->put('user.email', $user->email);

            $pickupaddress = new PickupAddress($request->all());
            $pickupaddress->users_id = $user->id;

            try {
                $pickupaddress->save();
                $product = Product::find($request->products_id);
                if ($product) {

                    /*Create $order*/
                    $order = new Order();
                    $order->date = Carbon::now();
                    $order->orderno = $this->generateOrderNo();
                    $order->amount = $product->price;
                    $order->users_id = $user->id;
                    $order->pickup_addresses_id = $pickupaddress->id;
                    $order->parentrefer_code = $parent_refer;
                    $order->status = 1;
                    $order->save();
                    /*end*/

                    /*create order item*/
                    $order_item = new OrderItem();
                    $order_item->orders_id = $order->id;
                    $order_item->products_id = $product->id;
                    $order_item->qty = 1;
                    $order_item->amount = $product->price;
                    $order_item->total = $product->price;
                    $order_item->save();
                    /*end*/

                    /*Send to payment*/
                    $tid = time() . rand(111, 999);
                    $parameters = [
                        'tid' => $tid,
                        'order_id' => $order->orderno,
                        'firstname' => $user->name,
                        'email' => $user->email,
                        'phone' => $pickupaddress->phone,
                        'productinfo' => $product->id,
                        'amount' => $order->amount,
                        'udf1' => $tid,
                        'udf2' => $request->products_id,
                        'udf3' => $user->id,
                        'udf4' => $order->id,
                        'udf5' => $order->orderno
                    ];

                    $orderPayu = Indipay::prepare($parameters);
                    return Indipay::process($orderPayu);
                } else {
                    Log::error('Product id : ' . $request->products_id . '. Not found .->function proceedToPayment()');
                    return redirect('checkout')->withInput()->with(['status' => false, 'message' => 'Sorry! Something Went wrong.Please try again']);
                }
            } catch (\Exception $e) {
                Log::error($e->getMessage() . 'Line number :' . $e->getLine());
                Log::error($e->getTraceAsString());
                return redirect('checkout')->withInput()->with(['status' => false, 'message' => 'Sorry! Something Went wrong.Please try again']);
            }
        }else{
            Log::error('Parent node_id Not found->session(join.node_id)');
            return redirect('checkout')->withInput()->with(['status' => false, 'message' => 'Sorry! Something Went wrong.Please try again']);
        }

//        return redirect('checkout')->withInput()->with(['status' => false, 'message' => 'Sorry! Something Went wrong.Please try again']);
    }

    private function generateOrderNo(){
        $today = date("Ymd");
        $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
        return $today.$rand;
    }


    public function successResponse(Request $request)

    {
        // For default Gateway
        $response = Indipay::response($request);

        //return $response;

        $product_id = $response['udf2'];
        $user_id = $response['udf3'];
        $order_id = $response['udf4'];
        $status = $response['status'];
        $reason = $response['field9'];
        $amount = $response['amount'];
//        $transactionId = $response['txnid'];
        $transactionId = $response['payuMoneyId'];

        $order = Order::find($order_id);//edited by noushid old:findOrFaild()
        if($order){
            $order->paymentstatus=1;
            $order->status=$status;
            $order->failurereason=$reason;
            $order->transaction_id = $transactionId;
            $order->order_status = 'new';
            try{
                $order->save();

                $currentOrderID=$order->id;//FOR USE IN SHARE CONTRIBUTION

                $last_node = Node::orderBy('created_at', 'desc')->where('user_id', $user_id);
                if ($last_node->count() > 0) {
                    $node_name = 'Product-' . ($last_node->count() + 1);
                }else{
                    $node_name = 'Product-1';
                }
                /*Generate Ref_code*/
                while (1) {
                    $ref_code = rand(101020, 999999);

                    if (Node::where('ref_code', $ref_code)->count() < 1){
                        break;
                    }
                }
                /*end*/
                $myparentlevels = '';
                $myparentnode = Node::find(session('join.node_id'));//changed from findOrFail noushid
                if($myparentnode){
                    $myparentlevels = $myparentnode->levels;
                }

                $myparentlevels = $myparentlevels . session('join.node_id') . '#';

                /*Create Node data*/
                $data = [];
                $data['node_name'] = $node_name;
                $data['user_id'] = $user_id;
                $data['product_id'] = $product_id;
                $data['parentnode_id'] =  session('join.node_id');
                $data['ref_code'] = $ref_code;
                $data['levels'] = $myparentlevels;
                $data['active'] = 1;
                $data['order_id'] = $order_id;

                $user = User::where('id', $user_id)->get()->first();

                $node = new Node($data);
                if ($node->save()) {
                    ini_set('max_execution_time', 0);
                    ini_set('max_input_time', 0);
                    if (session()->has('join.node_id')) {

                        /*create join structure */
                        $joint_data = [];
                        $joint_data['node_id'] = $node->id;
                        $joint_data['parentnode_id'] = session('join.node_id');
                        $joint_data['amount'] = $amount;
                        $joint = new Joint($joint_data);
                        $joint->save();

                        $parentNodesLists = explode("#", $myparentlevels);

                        /*Delete null element from array*/
                        $parentNodesLists = array_filter($parentNodesLists);

//                        var_dump($parentNodesLists);
//                        exit;
//                        /*Delete first index if null*/
//                        if ($parentNodesLists[0] == '') {
//                            unset($parentNodesLists[0]);
//                        }
//                        /*Delete last index if null*/
//                        if ($parentNodesLists[count($parentNodesLists) - 1] == '') {
//                            unset($parentNodesLists[count($parentNodesLists) - 1]);
//                        }
                        $reversedParentLists = array_reverse($parentNodesLists);

                        $executiveFound = 0;
                        $firstFound = 0;
                        $myshare = 0;

                        foreach($reversedParentLists as $index => $reversedParentList) {
                            if ($index < 10) {
                                if($reversedParentList!='') {
                                    $is_coordinator=0;
                                    $levelnode = Node::with('user')->where('active', 1)->where('id', $reversedParentList)->get()->first();
                                    if ($levelnode != null) {
                                        if($executiveFound==0) {
                                            if ($levelnode->user->usertype == 'executive') {
                                                if($index==0){
                                                    $myshare =100+50+50; //(MyShare::$coordinator_share)+(MyShare::$ordinary_share+MyShare::$ordinary_share);
                                                }else{
                                                    $myshare =100+50;
                                                }

                                                $is_coordinator = 1;
                                                $executiveFound = 1;
                                            } else {
                                                if($index==0) {
                                                    $myshare = 50+50;
                                                }else{
                                                    $myshare = 50;
                                                }
                                            }
                                        }else{
                                            if($index==0) {
                                                $myshare = 50+50;//(MyShare::$ordinary_share+MyShare::$ordinary_share);
                                            }else{
                                                if ($levelnode->user->usertype == 'executive') {
//                                                    $myshare = 100;//MyShare::$ordinary_share;//noushid
                                                    $myshare = 50;//MyShare::$ordinary_share;
                                                }else{
                                                    $myshare=50;
                                                }
                                            }
                                        }

                                        //Contribute Share to each Nodes............................................................
                                        $contribution = new ShareContribution();
                                        $contribution->nodes_id = $levelnode->id;
                                        $contribution->parentnodes_id = $levelnode->parentnode_id;
                                        $contribution->orders_id = $currentOrderID;
                                        $contribution->level = 'Level ' . ($index + 1);
                                        $contribution->amount = $myshare;
                                        $contribution->is_coordinator = $is_coordinator;
                                        $contribution->save();
                                    }
                                }
                            }else{
                                Log::info('Level over 10');
                            }
                        }

                    }else{
                        throw new \Exception("Parent join_id not found ");
                    }

                    $username = $user->username;
                    $password = session()->get('newuser.password');

                    $result = LaravelMsg91::message($user->phone, 'Thank you.Your Order was completed. Your Reference Code : ' . $node->ref_code . ' Or share : ' . url('join?ref=' . $node->ref_code));
                    $data['password'] = $password;
                    if ($result->type != 'success') {
                        throw new \Exception("Message sending failed");
                    }

                    $product = Product::find($product_id);
                    $data['username'] = $username;
                    $data['phone'] = session('user.phone');
                    $data['email'] = session('user.email');
                    $data['transaction_no'] = $transactionId;
                    $data['order_id'] = $order_id;
                    $data['product_name'] = $product->name;

                    $invoice = new Invoice();

                    $invoice->invoice_no = Invoice::max('invoice_no') + 1;
                    $invoice->amount = $amount;
                    $invoice->product_id = $product_id;
                    $invoice->order_id = $order_id;

                    if ($invoice->save()) {
                        $invoice = Invoice::where('id', $invoice->id)->with('order')->with('product')->get()->first();
                        session()->put('invoice.create', true);
                        session()->put('invoice', $invoice->toArray());
                    }else{
                        session()->put('invoice.create', false);
                        Log::error(' Invoice Creation error');
                    }

//                Mail::send('email.loginDetails', $data, function ($message) use ($data) {
//                    $message->to($data['email'])->subject('Wayonn login details');
//                });

                    return redirect('/payment_success');

                }else{
                    //TODO Refund amount
                    /*paymentstatus = 3 (Payment is succes but error in node creation so refund this)*/
                    $order->paymentstatus = 3;
                    $order->save();
                    throw new \Exception("Node creation error");

                }


//                return redirect('/payment_success');
            }
            catch(\Exception $e){
                Log::error($e->getMessage() . ' Line Number : ' . $e->getLine());
                Log::error($e->getTraceAsString());
                session()->flush('join');
                session()->flush('user');
                session()->flush('newuser');
                return redirect('checkout')->withInput()->with(['status' => true, 'message' => 'Sorry! Payment Success! Login details and refer code send soon']);
            }
        }else{
            try{
                //TODO Refund amount

                throw new \Exception("order_id not found");
            }
            catch(\Exception $e1){
                Log::error($e1->getMessage() . ' Line Number : ' . $e1->getLine());
                Log::error($e1->getTraceAsString());
                return redirect('checkout')->withInput()->with(['status' => true, 'message' => 'Sorry! Payment Success! Login details and refer code send soon']);
            }
        }



    }

    public function failureResponse(Request $request){
        // For default Gateway
        $response = Indipay::response($request);

        //return $response;

        $order_id=$response['udf4'];
        $status=$response['status'];
        $reason=$response['field9'];
        $user_id = $response['udf3'];
        $unmappedReason=$response['unmappedstatus'];

        session()->forget('user');
        session()->forget('join');
        session()->forget('newuser');

//        User::destroy($user_id);

        if($unmappedReason!='userCancelled'){
            $order=Order::findOrfail($order_id);//Changed to find or fail
            if($order){
                $order->paymentstatus=2;
                $order->status=$status;
                $order->failurereason=$reason;
                if($order->save()){
                    return redirect('/payment_failed');
                }
            }
        }
        return redirect('/checkout')->with(['status' => false, 'message' => 'Canceled By User']);
    }

    public function Order()
    {

        $order = Order::count();
        if($order)
            return $order;
        else
            return Response::json(array('error' => 'Record Not found'), 400);
    }


    public function Todays_orders()
    {
        $mytime = Carbon::now();
       $date=$mytime->toDateTimeString();

        $orders=Order::where(['created_at' => $date])->get()->count();

        if($orders>=0)
            return $orders;

        else
            return Response::json(array('error' => 'Record Not found'), 400);
    }


    public function validateInput(Request $request)
    {
        $temp = ($request->has('user_id') ? ',' . $request->user_id : '');
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email' . $temp,
            'pincode' => 'required|numeric',
            'postoffice' => 'required',
            'name' => 'required',
            'district' => 'required',
            'address' => 'required',
            'state' => 'required',
            'phone' => 'required',
            'products_id' => 'required',
        ]);

        if (!session()->has('user.phone')) {
            return response()->json(['message' => 'something went wrong'], 400);
        }

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }else{
            return response()->json(['status' => true]);
        }
    }


    /**
     *
     * Export TO PDf
    */

    public function exportToPdf(Request $request)
    {

        if ($request->status == 'all') {
            $orders = Order::with('user')->with('pickup_address')->where('paymentstatus', 1)->with('order_items.product')->get();
            $status = 'All';
        }
        if ($request->status == 'today') {
            $orders = Order::whereDate('created_at', DB::raw('CURDATE()'))->with('user')->with('pickup_address')->where('paymentstatus', 1)->where('order_status', 'new')->with('order_items.product')->get();
            $status = 'Today';
        }

        if ($request->status == 'neworder') {
            $orders = Order::where('paymentstatus', 1)->where('order_status', 'new')->with('user')->with('pickup_address')->with('order_items.product')->get();
            $status = 'New Order';
        }

        if ($request->status == 'packing') {
            $orders= Order::where('paymentstatus', 1)->where('order_status', 'packing')->with('user')->with('pickup_address')->with('order_items.product')->get();
            $status = 'Packing';
        }


        if ($request->status == 'packing') {
            $orders= Order::where('paymentstatus', 1)->where('order_status', 'packing')->with('user')->with('pickup_address')->with('order_items.product')->get();
            $status = 'Packing';
        }

        if ($request->status == 'shipping') {
            $orders = Order::where('paymentstatus', 1)->where('order_status', 'shipping')->with('user')->with('pickup_address')->with('order_items.product')->get();
            $status = 'Shipping';
        }
        if ($request->status == 'delivered') {
            $orders = Order::where('paymentstatus', 1)->where('order_status', 'delivered')->with('user')->with('pickup_address')->with('order_items.product')->get();
            $status = 'Delivered';
        }
        if ($request->status == 'canceled') {
            $orders = Order::where('paymentstatus', 1)->where('order_status', 'canceled')->with('user')->with('pickup_address')->with('order_items.product')->get();
            $status = 'Canceled';
        }

        if ($orders->count() > 0) {
            $data = '<html><head><style>table, td, th {  border: 1px solid rgba(34, 36, 37, 0.91);  }  table {  border-collapse: collapse;  width: 100%;  }  th {  height: 50px;  }</style></head><body>';
            $data .= '<h2 style="text-align:center;"> Order List</h2>';
            $data .= '<h4 style="text-align:center;">' . Carbon::now()->format('d-m-Y') . '</h4><br/><h5 style="text-align:right;">' . date('d-M-Y h:i',time()) . '</h5><hr/>';
            $data .= '<h4 style="text-align:center;"><u> Order ('.$status.')</u></h4><br/>';
            $data .= '<table width="100%" border="0.5"><thead><tr><th>SlNo</th><th>Invoice No</th><th>Product Name and code</th><th>Order No</th><th width="85px">Date</th><th>Address</th><th>Status</th></tr></thead>';
            $data .= '<tbody>';
            $id = 1;
            foreach ($orders as $value) {
                $data .= '<tr><td>&nbsp;&nbsp;' . $id . '</td><td>'.($value->invoice != null ? $value->invoice->invoice_no : '').'</td><td>' . $value->order_items[0]->product->name .'('.$value->order_items[0]->product->pdctcode . ')</td><td>' . $value->orderno. '</td><td width="50px">' . $value->date . '</td><td>' . $value->user->name.','. $value->pickup_address->address .', PH: '.$value->pickup_address->phone. '</td><td></td></tr>';
                $id = $id + 1;
            }
//                $data .= '<tr style="font-size:20px;"><td colspan="4">Total Attendance</td><td>&nbsp;</td><td>&nbsp;</td><td></td><td>&nbsp;</td><td></td></tr>';
            $data .= '</tbody>';
            $data .= '</table><br/><br/>';


            $data .= '<div style="font-size:22px;"><hr>Total : ' . $orders->count() . '<hr></div>';
            $data .= '<br/><br/><h3 style="text-align:right;">Sign</h3>';
            $data .= '</body></html>';
        }


        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($data);
        $pdf->output();
        return response($pdf->stream('List'), 200)->header('Content-Type', 'application/pdf');
    }


    public function customCreation(Request $request)

    {
        // For default Gateway
//        $response = Indipay::response($request);

        //return $response;

        /* $product_id = $response['udf2'];
         $user_id = $response['udf3'];
         $order_id = $response['udf4'];
         $status = $response['status'];
         $reason = $response['field9'];
         $amount = $response['amount'];
         $transactionId = $response['txnid'];*/

        $product_id = 9;
        $user_id = 8;
        $order_id = 23;
        $status = 'success';
        $reason = 'Transaction Completed Successfully';
        $amount = 2100;
        $transactionId = '182938702';

        $order = Order::find($order_id);//edited by noushid old:findOrFaild()
        if($order){
            $order->paymentstatus=1;
            $order->status=$status;
            $order->failurereason=$reason;
            $order->transaction_id = $transactionId;
            try{
                $order->save();

                $currentOrderID=$order->id;//FOR USE IN SHARE CONTRIBUTION

                $last_node = Node::orderBy('created_at', 'desc')->where('user_id', $user_id);
                if ($last_node->count() > 0) {
                    $node_name = 'Product-' . ($last_node->count() + 1);
                }else{
                    $node_name = 'Product-1';
                }
                /*Generate Ref_code*/
                while (1) {
                    $ref_code = str_random(6);

                    if (Node::where('ref_code', $ref_code)->count() < 1){
                        break;
                    }
                }
                /*end*/
                $myparentlevels = '';
//                $myparentnode = Node::findOrfail(session('join.node_id'));
                $myparentnode = Node::findOrfail(3);
                if($myparentnode){
                    $myparentlevels = $myparentnode->levels;
                }

//                $myparentlevels = $myparentlevels .  session('join.node_id');
                $myparentlevels = $myparentlevels .  3;

                /*Create Node data*/
                $data = [];
                $data['node_name'] = $node_name;
                $data['user_id'] = $user_id;
                $data['product_id'] = $product_id;
//                $data['parentnode_id'] =  session('join.node_id');
                $data['parentnode_id'] = 3;
                $data['ref_code'] = $ref_code;
                $data['levels'] = $myparentlevels;
                $data['active'] = 1;

                $user = User::where('id', $user_id)->get()->first();

                $node = new Node($data);
                if ($node->save()) {
                    ini_set('max_execution_time', 0);
                    ini_set('max_input_time', 0);
//                    if (session()->has('join.node_id')) {
                    if (1) {

                        /*create join structure */
                        $joint_data = [];
                        $joint_data['node_id'] = $node->id;
//                        $joint_data['parentnode_id'] = session('join.node_id');
                        $joint_data['parentnode_id'] = 3;
                        $joint_data['amount'] = $amount;
                        $joint = new Joint($joint_data);
                        $joint->save();

                        $parentNodesLists = explode("#", $myparentlevels);
                        $reversedParentLists=array_reverse($parentNodesLists);
                        $executiveFound=0;
                        $firstFound=0;
                        $myshare=0;
                        foreach($reversedParentLists as $index => $reversedParentList) {
                            if ($index < 10) {
                                if($reversedParentList!='') {
                                    $is_coordinator=0;
                                    $levelnode = Node::with('user')->where('active', 1)->where('id', $reversedParentList)->get()->first();
                                    if ($levelnode != null) {
                                        if($executiveFound==0) {
                                            if ($levelnode->user->usertype == 'executive') {
                                                if($index==0){
                                                    $myshare =100+50+50; //(MyShare::$coordinator_share)+(MyShare::$ordinary_share+MyShare::$ordinary_share);
                                                }else{
                                                    $myshare =100+50;
                                                }

                                                $is_coordinator = 1;
                                                $executiveFound = 1;
                                            } else {
                                                if($index==0) {
                                                    $myshare = 50+50;
                                                }else{
                                                    $myshare = 50;
                                                }
                                            }
                                        }else{
                                            if($index==0) {
                                                $myshare = 50+50;//(MyShare::$ordinary_share+MyShare::$ordinary_share);
                                            }else{
                                                if ($levelnode->user->usertype == 'executive') {
//                                                    $myshare = 100;//MyShare::$ordinary_share;//noushid
                                                    $myshare = 50;//MyShare::$ordinary_share;
                                                }else{
                                                    $myshare=50;
                                                }
                                            }
                                        }

                                        //Contribute Share to each Nodes............................................................
                                        $contribution = new ShareContribution();
                                        $contribution->nodes_id = $levelnode->id;
                                        $contribution->parentnodes_id = $levelnode->parentnode_id;
                                        $contribution->orders_id = $currentOrderID;
                                        $contribution->level = 'Level ' . ($index + 1);
                                        $contribution->amount = $myshare;
                                        $contribution->is_coordinator = $is_coordinator;
                                        $contribution->save();
                                    }
                                }
                            }
                        }

                    }else{
                        throw new \Exception("Parent join_id not found ");
                    }

                    $username = $user->username;
//                    $password = session()->get('newuser.password');

                    /*temp */
                    /*if (session()->get('newuser.create') == true) {
                        $result = LaravelMsg91::message($user->phone, 'Thank you.Your Username : ' . $username . ' And Password : ' . $password . '. Your Reference Code : ' . $node->ref_code . ' Or share : ' . url('join?ref=' . $node->ref_code) .'App link : https://goo.gl/dEdv6T');
                        $data['password'] = $password;
                        if ($result->type != 'success') {
                            throw new \Exception("Message sending failed");
                        }
                    }else{
                        $result = LaravelMsg91::message($user->phone, 'Thank you.Your Username : ' . $username . ' And You can use your current password. App link : https://goo.gl/dEdv6T');
                        $data['password'] = 'Use Your Current Password';
                        if ($result->type != 'success') {
                            throw new \Exception("Message sending error");
                        }
                    }*/
                    /*temp start*/

                    $data['username'] = $username;
//                    $data['phone'] = session('user.phone');
//                    $data['email'] = session('user.email');

                    $data['phone'] = '917736739385';
                    $data['email'] = 'molutysufira@gemail.com';

//                Mail::send('email.loginDetails', $data, function ($message) use ($data) {
//                    $message->to($data['email'])->subject('Wayonn login details');
//                });
                    return redirect('/payment_success');

                }else{
                    //TODO Refund amount
                    /*paymentstatus = 3 (Payment is succes but error in node creation so refund this)*/
                    $order->paymentstatus = 3;
                    $order->save();
                    throw new \Exception("Node creation error");

                }

//                session()->flush('join');
//                session()->flush('user');
//                session()->flush('newuser');
//                return redirect('/payment_success');
            }
            catch(\Exception $e){
                Log::error($e->getMessage() . ' Line Number : ' . $e->getLine());
                Log::error($e->getTraceAsString());
                return redirect('checkout')->withInput()->with(['status' => true, 'message' => 'Sorry! Payment Success! Login details and refer code send soon']);
            }
        }else{
            try{
                //TODO Refund amount

                throw new \Exception("order_id not found");
            }
            catch(\Exception $e1){
                Log::error($e1->getMessage() . ' Line Number : ' . $e1->getLine());
                Log::error($e1->getTraceAsString());
                return redirect('checkout')->withInput()->with(['status' => true, 'message' => 'Sorry! Payment Success! Login details and refer code send soon']);
            }
        }



    }

    public function invoicePdf(Request $request)
    {

        $data['invoice'] = App\Invoice::where('id', $request->invoice_id)->get()->first();
        $pdf = PDF::loadView('pdf.invoice', $data);
        $pdf->output();

        return response($pdf->stream('PDf'), 200)->header('Content-Type', 'application/pdf');
    }


    public function printAll(Request $request)
    {
        if ($request->status == 'neworder') {
            $orders = Order::where('status', null)->with('invoice');
            foreach ($orders as $invoice) {
                $invoices[] = $invoice->invoice;
            }
            $invoices = array_filter($invoices);
        }elseif ($request->status == 'all') {
            $orders = Order::with('user')->with('pickup_address')->where('paymentstatus', 1)->with('invoice')->get();
            foreach ($orders as $invoice) {
                $invoices[] = $invoice->invoice;
            }
            $invoices = array_filter($invoices);
        }elseif($request->status == 'today') {
            $status = 'Today order';
            $orders = Order::whereDate('created_at', DB::raw('CURDATE()'))->where('paymentstatus', 1)->with('invoice');
            foreach ($orders as $invoice) {
                $invoices[] = $invoice->invoice;
            }
            $invoices = array_filter($invoices);
        }else{
            $orders = Order::where('status', $request->status)->with('invoice');
            $orders = Order::whereDate('created_at', DB::raw('CURDATE()'))->where('paymentstatus', 1)->with('invoice');
            foreach ($orders as $invoice) {
                $invoices[] = $invoice->invoice;
            }
            $invoices = array_filter($invoices);
        }

        if ($orders->count() > 0) {
            $data['invoices'] = $invoices;
            $pdf = PDF::loadView('pdf.invoiceAll', $data);

            $pdf->output();
            return response($pdf->stream('List'), 200)->header('Content-Type', 'application/pdf');
        }
    }


}



/*
 *
                        $executiveFound=0;
                        $myshare=0;
                        //Level 1
                        $level1node=Node::with('user')->where('active',1)->where('id',session('join.node_id'))->get()->first();
                        if($level1node!=null){
                            $is_coordinator=0;
                            if($level1node->user->usertype=='executive'){
                                $myshare=MyShare::$coordinator_share;
                                $is_coordinator=1;
                                $executiveFound=1;
                            }else{
                                $myshare=MyShare::$ordinary_share;
                            }

                            //Contribute Share to each Nodes............................................................
                            $contribute1=new ShareContribution();
                            $contribute1->nodes_id=$level1node->id;
                            $contribute1->parentnodes_id=session('join.node_id');
                            $contribute1->orders_id=$currentOrderID;
                            $contribute1->level='Level 1';
                            $contribute1->amount=$myshare;
                            $contribute1->is_coordinator=$is_coordinator;
                            if($contribute1->save()){
                                //Level 2
                                $level2node=Node::with('user')->where('active',1)->where('id',$level1node->parentnodes_id)->get()->first();
                                if($level2node!=null){
                                    $is_coordinator=0;
                                    if($executiveFound==0) {
                                        if ($level2node->user->usertype == 'executive') {
                                            $myshare = MyShare::$coordinator_share;
                                            $is_coordinator = 1;
                                            $executiveFound = 1;
                                        } else {
                                            $myshare = MyShare::$ordinary_share;
                                        }
                                    }else{
                                        $myshare = MyShare::$ordinary_share;
                                    }

                                    //Contribute Share to each Nodes............................................................
                                    $contribute2=new ShareContribution();
                                    $contribute2->nodes_id=$level2node->id;
                                    $contribute2->parentnodes_id=$level1node->id;
                                    $contribute2->orders_id=$currentOrderID;
                                    $contribute2->level='Level 2';
                                    $contribute2->amount=$myshare;
                                    $contribute2->is_coordinator=$is_coordinator;
                                    if($contribute2->save()){
                                        //Level 3
                                        $level3node=Node::with('user')->where('active',1)->where('id',$level2node->parentnodes_id)->get()->first();
                                        if($level3node!=null){
                                            $is_coordinator=0;
                                            if($executiveFound==0) {
                                                if ($level3node->user->usertype == 'executive') {
                                                    $myshare = MyShare::$coordinator_share;
                                                    $is_coordinator = 1;
                                                    $executiveFound = 1;
                                                } else {
                                                    $myshare = MyShare::$ordinary_share;
                                                }
                                            }else{
                                                $myshare = MyShare::$ordinary_share;
                                            }

                                            //Contribute Share to each Nodes............................................................
                                            $contribute3=new ShareContribution();
                                            $contribute3->nodes_id=$level3node->id;
                                            $contribute3->parentnodes_id=$level2node->id;
                                            $contribute3->orders_id=$currentOrderID;
                                            $contribute3->level='Level 3';
                                            $contribute3->amount=$myshare;
                                            $contribute3->is_coordinator=$is_coordinator;
                                            if($contribute3->save()){
                                                // Level 4
                                                $level4node=Node::with('user')->where('active',1)->where('id',$level3node->parentnodes_id)->get()->first();
                                                if($level4node!=null){
                                                    $is_coordinator=0;
                                                    if($executiveFound==0) {
                                                        if ($level4node->user->usertype == 'executive') {
                                                            $myshare = MyShare::$coordinator_share;
                                                            $is_coordinator = 1;
                                                            $executiveFound = 1;
                                                        } else {
                                                            $myshare = MyShare::$ordinary_share;
                                                        }
                                                    }else{
                                                        $myshare = MyShare::$ordinary_share;
                                                    }

                                                    //Contribute Share to each Nodes............................................................
                                                    $contribute4=new ShareContribution();
                                                    $contribute4->nodes_id=$level4node->id;
                                                    $contribute4->parentnodes_id=$level3node->id;
                                                    $contribute4->orders_id=$currentOrderID;
                                                    $contribute4->level='Level 4';
                                                    $contribute4->amount=$myshare;
                                                    $contribute4->is_coordinator=$is_coordinator;
                                                    if($contribute4->save()){
                                                        // Level 5
                                                        $level5node=Node::with('user')->where('active',1)->where('id',$level4node->parentnodes_id)->get()->first();
                                                        if($level5node!=null){
                                                            $is_coordinator=0;
                                                            if($executiveFound==0) {
                                                                if ($level5node->user->usertype == 'executive') {
                                                                    $myshare = MyShare::$coordinator_share;
                                                                    $is_coordinator = 1;
                                                                    $executiveFound = 1;
                                                                } else {
                                                                    $myshare = MyShare::$ordinary_share;
                                                                }
                                                            }else{
                                                                $myshare = MyShare::$ordinary_share;
                                                            }
                                                            //Contribute Share to each Nodes............................................................
                                                            $contribute5=new ShareContribution();
                                                            $contribute5->nodes_id=$level5node->id;
                                                            $contribute5->parentnodes_id=$level4node->id;
                                                            $contribute5->orders_id=$currentOrderID;
                                                            $contribute5->level='Level 5';
                                                            $contribute5->amount=$myshare;
                                                            $contribute5->is_coordinator=$is_coordinator;
                                                            if($contribute5->save()){
                                                                // Level 6
                                                                $level6node=Node::with('user')->where('active',1)->where('id',$level5node->parentnodes_id)->get()->first();
                                                                if($level6node!=null){
                                                                    $is_coordinator=0;
                                                                    if($executiveFound==0) {
                                                                        if ($level6node->user->usertype == 'executive') {
                                                                            $myshare = MyShare::$coordinator_share;
                                                                            $is_coordinator = 1;
                                                                            $executiveFound = 1;
                                                                        } else {
                                                                            $myshare = MyShare::$ordinary_share;
                                                                        }
                                                                    }else{
                                                                        $myshare = MyShare::$ordinary_share;
                                                                    }

                                                                    //Contribute Share to each Nodes............................................................
                                                                    $contribute6=new ShareContribution();
                                                                    $contribute6->nodes_id=$level6node->id;
                                                                    $contribute6->parentnodes_id=$level5node->id;
                                                                    $contribute6->orders_id=$currentOrderID;
                                                                    $contribute6->level='Level 6';
                                                                    $contribute6->amount=$myshare;
                                                                    $contribute6->is_coordinator=$is_coordinator;
                                                                    if($contribute6->save()){
                                                                        // Level 7
                                                                        $level7node=Node::with('user')->where('active',1)->where('id',$level6node->parentnodes_id)->get()->first();
                                                                        if($level7node!=null){
                                                                            $is_coordinator=0;
                                                                            if($executiveFound==0) {
                                                                                if ($level7node->user->usertype == 'executive') {
                                                                                    $myshare = MyShare::$coordinator_share;
                                                                                    $is_coordinator = 1;
                                                                                    $executiveFound = 1;
                                                                                } else {
                                                                                    $myshare = MyShare::$ordinary_share;
                                                                                }
                                                                            }else{
                                                                                $myshare = MyShare::$ordinary_share;
                                                                            }

                                                                            //Contribute Share to each Nodes............................................................
                                                                            $contribute7=new ShareContribution();
                                                                            $contribute7->nodes_id=$level7node->id;
                                                                            $contribute7->parentnodes_id=$level6node->id;
                                                                            $contribute7->orders_id=$currentOrderID;
                                                                            $contribute7->level='Level 7';
                                                                            $contribute7->amount=$myshare;
                                                                            $contribute7->is_coordinator=$is_coordinator;
                                                                            if($contribute7->save()){
                                                                                // Level 8
                                                                                $level8node=Node::with('user')->where('active',1)->where('id',$level7node->parentnodes_id)->get()->first();
                                                                                if($level8node!=null){
                                                                                    $is_coordinator=0;
                                                                                    if($executiveFound==0) {
                                                                                        if ($level8node->user->usertype == 'executive') {
                                                                                            $myshare = MyShare::$coordinator_share;
                                                                                            $is_coordinator = 1;
                                                                                            $executiveFound = 1;
                                                                                        } else {
                                                                                            $myshare = MyShare::$ordinary_share;
                                                                                        }
                                                                                    }else{
                                                                                        $myshare = MyShare::$ordinary_share;
                                                                                    }

                                                                                    //Contribute Share to each Nodes............................................................
                                                                                    $contribute8=new ShareContribution();
                                                                                    $contribute8->nodes_id=$level8node->id;
                                                                                    $contribute8->parentnodes_id=$level7node->id;
                                                                                    $contribute8->orders_id=$currentOrderID;
                                                                                    $contribute8->level='Level 8';
                                                                                    $contribute8->amount=$myshare;
                                                                                    $contribute8->is_coordinator=$is_coordinator;
                                                                                    if($contribute8->save()){
                                                                                        // Level 9
                                                                                        $level9node=Node::with('user')->where('active',1)->where('id',$level8node->parentnodes_id)->get()->first();
                                                                                        if($level9node!=null){
                                                                                            $is_coordinator=0;
                                                                                            if($executiveFound==0) {
                                                                                                if ($level9node->user->usertype == 'executive') {
                                                                                                    $myshare = MyShare::$coordinator_share;
                                                                                                    $is_coordinator = 1;
                                                                                                    $executiveFound = 1;
                                                                                                } else {
                                                                                                    $myshare = MyShare::$ordinary_share;
                                                                                                }
                                                                                            }else{
                                                                                                $myshare = MyShare::$ordinary_share;
                                                                                            }

                                                                                            //Contribute Share to each Nodes............................................................
                                                                                            $contribute9=new ShareContribution();
                                                                                            $contribute9->nodes_id=$level9node->id;
                                                                                            $contribute9->parentnodes_id=$level8node->id;
                                                                                            $contribute9->orders_id=$currentOrderID;
                                                                                            $contribute9->level='Level 9';
                                                                                            $contribute9->amount=$myshare;
                                                                                            $contribute9->is_coordinator=$is_coordinator;
                                                                                            if($contribute9->save()){
                                                                                                // Level 10
                                                                                                $level10node=Node::with('user')->where('active',1)->where('id',$level9node->parentnodes_id)->get()->first();
                                                                                                if($level10node!=null){
                                                                                                    $is_coordinator=0;
                                                                                                    if($executiveFound==0) {
                                                                                                        if ($level10node->user->usertype == 'executive') {
                                                                                                            $myshare = MyShare::$coordinator_share;
                                                                                                            $is_coordinator = 1;
                                                                                                            $executiveFound = 1;
                                                                                                        } else {
                                                                                                            $myshare = MyShare::$ordinary_share;
                                                                                                        }
                                                                                                    }else{
                                                                                                        $myshare = MyShare::$ordinary_share;
                                                                                                    }

                                                                                                    //Contribute Share to each Nodes............................................................
                                                                                                    $contribute10=new ShareContribution();
                                                                                                    $contribute10->nodes_id=$level10node->id;
                                                                                                    $contribute10->parentnodes_id=$level9node->id;
                                                                                                    $contribute10->orders_id=$currentOrderID;
                                                                                                    $contribute10->level='Level 10';
                                                                                                    $contribute10->amount=$myshare;
                                                                                                    $contribute10->is_coordinator=$is_coordinator;
                                                                                                    $contribute10->save();
                                                                                                }//Level 10
                                                                                            }
                                                                                        }//Level 9
                                                                                    }
                                                                                }//Level 8
                                                                            }
                                                                        }//Level 7
                                                                    }
                                                                }//Level 6
                                                            }
                                                        }//Level 5
                                                    }
                                                }//Level 4
                                            }
                                        }//Level 3
                                    }

                                }//Level 2
                            }
                        }//Level 1
 *
 */