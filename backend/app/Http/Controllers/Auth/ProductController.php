<?php

namespace App\Http\Controllers\Auth;

use App\Photo;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Validator;
use Response;
use Image;
use App;
use PDF;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'brand' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
            'pdctcode' => 'required|unique:products,pdctcode',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::with('files')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $product = new Product($request->all());
        if ($product->save()) {
            $photos = $request->photos;
            if ($photos != null) {
                foreach ($photos as $photo) {
                    $myphoto=new Photo();
                    $myphoto->name = $this->savePhoto($photo['data']);
                    $myphoto->products_id=$product->id;
                    $myphoto->save();
                }
            }else{
                $myphoto=new Photo();
                $myphoto->name ='product.png';
                $myphoto->product_id=$product->id;
                $myphoto->save();
            }
            return Product::with('files')->find($product->id);
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Save Baseuri image to Server....................................
     */
    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'product'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(510, 652)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'brand' => 'required',
            'description' => 'required',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $product = Product::find($id);
        $product->fill($request->all());
        if ($product->update()) {
            $photos = $request->photos;
            if ($photos != null) {
                $items=Photo::where('products_id',$id)->get();
                if($items){
                    foreach($items as $item) {
                        if ($item->name != 'product.png') {
                            $exist = Storage::disk('local')->exists($item->name);
                            if ($exist)
                                Storage::delete($item->name);
                        }
                    }
                }
                Photo::where('products_id',$product->id)->delete();
                foreach ($photos as $photo) {
                    $myphoto=new Photo();
                    $myphoto->name = $this->savePhoto($photo['data']);
                    $myphoto->products_id=$product->id;
                    $myphoto->save();
                }
            }
            return Product::with('files')->find($product->id);
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items=Photo::where('products_id',$id)->get();
        if($items){
            foreach($items as $item) {
                if ($item->name != 'product.png') {
                    $exist = Storage::disk('local')->exists($item->name);
                    if ($exist)
                        Storage::delete($item->name);
                }
            }
            Photo::where('products_id',$id)->delete();
        }
        if (Product::destroy($id)) {
            return Response::json(array('msg' => 'Product record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    public function changeProductStatus(Request $request){
        $id=$request->id;
        $product=Product::findOrfail($id);
        if($product){
            if($product->active==1)
                $product->active=0;
            else
                $product->active=1;
            if($product->save())
                return $product;
        }
    }


    public function loadProducts(Request $request){
        $output = '';
        $id = $request->id;

        $products=\App\Product::where('id','>',$id)->orderBy('created_at','DESC')->with('files')->limit(8)->get();

        if(!$products->isEmpty())
        {
            foreach($products as $product)
            {

                $output .='<div class="col-sm-6 col-md-3 ws-works-item"><a href="/single-product/'.$product->id.'"><div class="ws-item-offer"><figure>';

                if($product->files->count()>0)
                    $output .='<img src="'.url('images/'.$product->files[0]->name).'" alt="Alternative Text" class="img-responsive">';

                $output .='</figure></div><div class="ws-works-caption"><div class="ws-item-category">'.$product->brand.'</div><h3 class="ws-item-title">'.$product->name.'</h3></div></a></div>';
            }

            $output .= '<div id="remove-row" class="ws-more-btn-holder col-sm-12">
                            <button id="btn-more" data-id="'.$products->last()->id.'" class="btn ws-more-btn"> Load More</button>
                        </div>';

            echo $output;
        }
    }


    /**
     *
     * Export TO PDf
     */

    public function exportToPdf()
    {

        $products = Product::where('active', 1)->with('files')->get();
        if ($products->count() > 0) {
            $data = '<html><head><style>table, td, th {  border: 1px solid rgba(34, 36, 37, 0.91);  }  table {  border-collapse: collapse;  width: 100%;  }  th {  height: 50px;  }</style></head><body>';
            $data .= '<h2 style="text-align:center;"> Order List</h2>';
            $data .= '<h4 style="text-align:center;">' . Carbon::now()->format('d-m-Y') . '</h4><br/><h5 style="text-align:right;">' . date('d-M-Y h:i',time()) . '</h5><hr/>';
            $data .= '<h4 style="text-align:center;"><u> Product List </u></h4><br/>';
            $data .= '<table width="100%" border="0.5"><thead><tr><th>SlNo</th><th>Name</th><th>product Code</th><th>Remarks</th></tr></thead>';
            $data .= '<tbody>';
            $id = 1;
            foreach ($products as $value) {
                $data .= '<tr><td>&nbsp;&nbsp;' . $id . '</td><td>' . $value->name . '</td><td>' . $value->pdctcode . '</td><td>     </td></tr>';
//                $data .= '<tr><td>&nbsp;&nbsp;' . $id . '</td><td>' . $value->name . '</td><td>' . $value->pdctcode . '</td><td><img src="images/' . $value->files[0]->photo->name . '" style="width: 80px;height: 80px;padding-right: 5px;padding-bottom: 5px;" alt=""/></td><td>     </td></tr>';
                $id = $id + 1;
            }
//                $data .= '<tr style="font-size:20px;"><td colspan="4">Total Attendance</td><td>&nbsp;</td><td>&nbsp;</td><td></td><td>&nbsp;</td><td></td></tr>';
            $data .= '</tbody>';
            $data .= '</table><br/><br/>';


            $data .= '<div style="font-size:22px;"><hr>Total : ' . $products->count() . '<hr></div>';
            $data .= '<br/><br/><h3 style="text-align:right;">Sign</h3>';
            $data .= '</body></html>';
        }


        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($data);
        $pdf->output();
        return response($pdf->stream('List'), 200)->header('Content-Type', 'application/pdf');
    }



}
