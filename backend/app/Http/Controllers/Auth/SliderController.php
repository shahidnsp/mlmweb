<?php

namespace App\Http\Controllers\Auth;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Image;
use Validator;
use Response;

class SliderController extends Controller
{
    /**
     * Validate Request Data.
     * @param array
     * @return validator
     */

    public function Validator(array $data)
    {
        return Validator::make($data, [
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Slider::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $slider = new Slider($request->all());
        $count=Slider::get()->count();
        $slider->order=$count+1;
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $slider->name = $this->savePhoto($photo['data']);
            }
        }
        if ($slider->save()) {
            return $slider;
        }
        return Response::json(['Error' => 'Server Down'], 500);
    }

    /**
     * Save Baseuri image to Server....................................
     */
    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'slider'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                // $img=Image::make($data)->resize(1920, 1280)->stream();
                $img=Image::make($data)->stream();
                //$img->resize(120, 120);
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $slider = Slider::find($id);
        $slider->fill($request->all());
        $photos = $request->photos;
        if ($photos != null) {
            foreach ($photos as $photo) {
                $slider->name = $this->savePhoto($photo['data']);
            }
        }
        if ($slider->update()) {
            return $slider;
        }
        return Response::json(['error' => 'Server down'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items=Slider::find($id);
        if($items){
            if ($items->name != 'profile.png') {
                $exist = Storage::disk('local')->exists($items->name);
                if ($exist)
                    Storage::delete($items->name);
            }
        }
        if (Slider::destroy($id)) {
            return Response::json(array('msg' => 'Slider record deleted'));
        } else {
            return Response::json(array('error' => 'Record Not found'), 400);
        }
    }

    /**
     * Change Order of Slider....................
     */
    public function changeSliderOrder(Request $request){
        $id=$request->id;
        $order=$request->order;

        $slider=Slider::findOrfail($id);
        if($slider){
            $slider->order=$order;
            if($slider->save())
                return $slider;
        }
    }
}
