<?php

namespace App\Http\Controllers;

//use Illuminate\Foundation\Auth\User;
use App\User;
use Illuminate\Http\Request;
use Auth;
use LaravelMsg91;
use Validator;
use App\Node;
use Illuminate\Support\Facades\Mail;
use App\ShareContribution;

use App;
use PDF;
use Illuminate\Support\Facades\Log;

use App\Code;
use Mbarwick83\Shorty\Facades\Shorty;



class HomeController extends Controller
{

    protected $productUrl = '';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//       $this->middleware('auth');
        if (env("APP_ENV", "") == 'production') {
            $this->productUrl = 'http://wayonnproducts.com/';
        }elseif (env("APP_ENV", "") == 'test') {
            $this->productUrl = 'http://test.wayonnproducts.com/';
        }else{
            $this->productUrl = 'http://localhost:8080/';
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            if(Auth::user()->usertype=='admin')
                return view('app.index');
            else
                return view('web.index');
        }
        return view('web.index');
    }


    public function sendOtp(Request $request)
    {
        $request->merge(['phone' => clean($request->phone)]);
        $validator = Validator::make($request->all(), [
            'phone' => 'required|digits:12'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 401);
        }
        if (!$request->session()->has('join.ref_code')) {
            return response()->json(['status' => false, 'message' => 'invalid_reference_code'], 400);
        }

        $otp = mt_rand(11234, 99999);
        $result = LaravelMsg91::sendOtp($request->phone, $otp, 'Enter ' . $otp . ' as your verification code to verify your mobile number. Do not share it with anyone.');



        /*temp */
//        $result['type'] = 'success';
//        $result['message'] = 'dddddddddd';
//        $result = (object)$result;
        /*temp */

        if ($result->type == 'success') {
            $request->session()->put('user.phone', $request->phone);
            return response()->json(['status' => true, 'phone' => $request->phone]);
        }else{
            return response()->json(['status' => false, 'message' => $result->message], 400);
        }
    }


    public function verifyOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required|digits:5'
        ]);

        if ($validator->fails()) {
            return response($validator->errors(), 401);
        }

        $phone = $request->session()->get('user.phone');
        $request->session()->put('user.verifyOtp', true);
        $result = LaravelMsg91::verifyOtp($phone, $request->get('otp'), ['raw' => true]);

//        /*temp */
//        $result['type'] = 'success';
//        $result['message'] = 'otp_verified';
//        $result = (object)$result;
//        /*temp */

        if ($result->type == 'success') {
            $user = User::where('phone', $phone);
            $request->session()->put('user.verifyOtp', true);
            if ($user->count() > 0) {
                return response()->json([
                    'status' => true,
                    'message' => 'verified',
                    'user_exist' => true,
                    'data' => $user->get()->first()
                ]);
            }
            return response()->json([
                'status' => true,
                'message' => 'verified',
                'user_exist' => false,
                'data' => ['phone' => $phone]
            ]);
        }else{
            return response()->json(['status' => false, 'message' => $result->message], 400);
        }
    }


    /**
     *
     */
    public function join(Request $request)
    {
        if ($request->has('order') && $request->get('order') == 'success' && $request->get('invoice') == 'false') {
            session()->forget('join');
            session()->forget('user');
            $error = 'Invoice Not generated. Please Contact support Team.';
            return view('web.join', compact('invoice', 'error'));
        }elseif ($request->has('order') && $request->get('order') == 'success' && $request->get('invoice')) {
            $invoice_id = $request->get('invoice');
            $invoice = App\Invoice::with('product', 'order.user', 'order.pickup_address')->find($invoice_id);
            session()->forget('join');
            session()->forget('user');
            return view('web.join', compact('invoice'));
        }elseif($request->has('ref')) {
            $reference = $request->ref;
            try{
                $node = Node::where('ref_code', $reference)->with('user');
                if ($node->count() > 0) {
                    $request->session()->put('join.node_name', $node->first()->node_name);
                    $request->session()->put('join.ref_code', $node->first()->ref_code);
                    $request->session()->put('join.node_id', $node->first()->id);
                    $request->session()->put('join.user.name', $node->first()->user->name);
                }
                return view('web.join');
            }
            catch(\Exception $e){
                $request->session()->forget('join');
                Log::error($e->getMessage() . ' Line Number : ' . $e->getLine());
                Log::error($e->getTraceAsString());
                return view('web.join');
            }
        }
        return view('web.join');
    }


    /**
     *Verify reference code
     *
     */

    public function refCodeVerify(Request $request)
    {
        $reference = $request->reference;
        $node = Node::where('ref_code', $reference)->with('user');
        if ($node->count() > 0) {
            if ($node->get()->first()->user == null) {
                return response(['status' => false, 'message' => 'invalid_reference_code'], 400);
            }
            $request->session()->put('join.node_name', $node->first()->node_name);
            $request->session()->put('join.ref_code', $node->first()->ref_code);
            $request->session()->put('join.node_id', $node->first()->id);
            $request->session()->put('join.user.name', $node->first()->user->name);
            return response(['status' => true, 'message' => 'reference_code_verified', 'data' => ['username' => $node->first()->user->name]]);
        }else{
            return response(['status' => false, 'message' => 'invalid_reference_code'], 400);
        }
    }


    public function verifyEmail(Request $request)
    {
        $code = $request->code;
        User::where('confirmation_code', $code)->update(['verify_email' => 1, 'confirmation_code' => null]);
        return redirect('/');
    }


    public function createInvoicePdf($invoice_id)
    {
        $invoice = App\Invoice::where('invoice_no', $invoice_id)->where('downloaded', 0)->with('order.pickup_address', 'product');
        if ($invoice->count() > 0) {
            $data['invoice'] = $invoice->get()->first();
            try{
                $pdf = PDF::loadView('pdf.invoice', $data);
                App\Invoice::where('invoice_no', $invoice_id)->update(['downloaded' => 1]);
                return $pdf->download('invoice.pdf');
            }
            catch(\Exception $e){
                Log::error($e->getMessage() . ' Line Number : ' . $e->getLine());
                Log::error($e->getTraceAsString());
            }
        }

    }


    public function createInvoicePdfFull()
    {
        $data['invoice'] = App\Invoice::where('id', 1)->with('order.pickup_address', 'product')->get()->first();
        $pdf = PDF::loadView('pdf.invoiceAll', $data);
        return $pdf->download('invoice.pdf');
//        return view('pdf.invoiceAll', $data);
    }

    public function viewInvoice()
    {
        $data['invoice'] = App\Invoice::where('id', 1)->with('order.pickup_address', 'product')->get()->first();

        return view('pdf.invoiceAll', $data);
    }

    public function resetSession(Request $request,$value)
    {
        $request->session()->forget($value);
    }


    public function validateInput(Request $request)
    {
        $temp = ($request->has('user_id') ? ',' . $request->user_id : '');
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email' . $temp,
            'pincode' => 'required|numeric',
            'postoffice' => 'required',
            'name' => 'required',
            'district' => 'required',
            'address' => 'required',
            'state' => 'required',
            'phone' => 'required',
            'city' => 'required',
        ]);

        if (!session()->has('user.phone')) {
            return response()->json(['message' => 'something went wrong'], 400);
        }

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }else{
            return response()->json(['status' => true]);
        }
    }


    public function cancelRegistration(Request $request)
    {
        session()->forget('user');
        return response()->json(['status' => true]);
    }
    /**
     *
     */
    public function register(Request $request)
    {
        /*Get data from session*/
        $phone = session()->get('user.phone');
        $parent_refer = session()->get('join.ref_code');

        if (session()->has('join.node_id')) {
            $join_sess = session()->get('join');
            $user = User::where('phone', $phone);
            /*If new user*/
            if ($user->count() == 0) {
                /*Create new user */
                $confirmation_code = str_random(30);
                $password = str_random(8);
                $user_data = $request->all();

                $user_data['password'] = bcrypt($password);
                $user_data['username'] = $request->email;
                $user_data['usertype'] = 'user';
                $user_data['confirmation_code'] = $confirmation_code;

                $user = new User($user_data);

                try {
                    $user->save();

                    /*Generate promo_code*/
                    while (1) {
                        $promo_code = rand(101020, 999999);

                        if (Code::where('pro_code', $promo_code)->count() < 1){
                            break;
                        }
                    }
                    /*end*/


                    /*Create node */
                    $last_node = Node::orderBy('created_at', 'desc')->where('user_id', $user->id);
                    if ($last_node->count() > 0) {
                        $node_name = 'Product-' . ($last_node->count() + 1);
                    }else{
                        $node_name = 'Product-1';
                    }
                    /*Generate Ref_code*/
                    while (1) {
                        $ref_code = rand(101020, 999999);

                        if (Node::where('ref_code', $ref_code)->count() < 1){
                            break;
                        }
                    }
                    /*end*/
                    $myparentlevels = '';
                    $myparentnode = Node::find(session('join.node_id'));//changed from findOrFail noushid
                    if($myparentnode){
                        $myparentlevels = $myparentnode->levels;
                    }

                    $myparentlevels = $myparentlevels . session('join.node_id') . '#';

                    /*Create Node data*/
                    $node_data['node_name'] = $node_name;
                    $node_data['user_id'] = $user->id;
                    $node_data['parentnode_id'] =  session('join.node_id');
                    $node_data['ref_code'] = $ref_code;
                    $node_data['levels'] = $myparentlevels;
                    $node_data['active'] = 0;

                    $node = new Node($node_data);
                    $node->save();

                    /*Create codes */
                    if (!empty($user->id)) {
                        $code_data['user_id'] = $user->id;
                    }
                    $code_data['ref_node_id'] = $join_sess['node_id'];
                    $code_data['pro_code'] = $promo_code;
                    if (!empty($node->id)) {
                        $code_data['node_id'] = $node->id;
                    }
                    $code = new Code($code_data);

                    $code->save();


                    $regMsg = LaravelMsg91::message($user->phone, 'Thank you for registration. Your Username : ' . $user_data['username'] . ' And Password : ' . $password . '. Download our Application and manage your network. https://goo.gl/dEdv6T');
                    $url = $this->productUrl . $promo_code;
                    if (env("APP_ENV", "") == 'local') {
                        $urlShort = false;
                    }else{
//                        $urlShort = Shorty::shorten($url);
                        $urlShort = false;
                    }
                    //TODO url shortner
                    $promoMsg = LaravelMsg91::message($user->phone, 'Your Promo code :' . $promo_code . '. or click : ' . ($urlShort ? $urlShort : $url));


                    if ($regMsg->type != 'success') {
                        throw new \Exception("registration Message sending failed");
                    }

                    if ($promoMsg->type != 'success') {
                        throw new \Exception("Promo code sending failed");
                    }

                } catch (\Exception $e) {
                    Log::error($e->getMessage() . ' Line Number : ' . $e->getLine());
                    Log::error($e->getTraceAsString());
                    $user->destroy($user->id);
                    return redirect('join')->withInput()->with(['status' => false, 'message' => 'Sorry! Something Went wrong.Please try again']);
                }

                $mail_data['name'] = $user->name;
                $mail_data['email'] = $user->email;
                $mail_data['url'] = url('verify-email/' . $confirmation_code);

                Mail::send('email.emailVerification', $mail_data, function ($message) use ($mail_data) {
                    $message->to($mail_data['email'], $mail_data['name'])->subject('Wayonn Verification');
                });
                return redirect($url);

            } else {
                try {

                    $user = $user->get()->first();

                    /*Create node */
                    $last_node = Node::orderBy('created_at', 'desc')->where('user_id', $user->id);
                    if ($last_node->count() > 0) {
                        $node_name = 'Product-' . ($last_node->count() + 1);
                    }else{
                        $node_name = 'Product-1';
                    }
                    /*Generate Ref_code*/
                    while (1) {
                        $ref_code = rand(101020, 999999);

                        if (Node::where('ref_code', $ref_code)->count() < 1){
                            break;
                        }
                    }
                    /*end*/
                    $myparentlevels = '';
                    $myparentnode = Node::find(session('join.node_id'));//changed from findOrFail noushid
                    if($myparentnode){
                        $myparentlevels = $myparentnode->levels;
                    }

                    $myparentlevels = $myparentlevels . session('join.node_id') . '#';

                    /*Create Node data*/
                    $node_data['node_name'] = $node_name;
                    $node_data['user_id'] = $user->id;
                    $node_data['parentnode_id'] =  session('join.node_id');
                    $node_data['ref_code'] = $ref_code;
                    $node_data['levels'] = $myparentlevels;
                    $node_data['active'] = 0;

                    $node = new Node($node_data);
                    $node->save();

                    /*Create codes */
                    /*Generate promo_code*/
                    while (1) {
                        $promo_code = rand(101020, 999999);

                        if (Code::where('pro_code', $promo_code)->count() < 1) {
                            break;
                        }
                    }
                    /*end*/
                    $code_data['user_id'] = $user->id;
                    $code_data['ref_node_id'] = $join_sess['node_id'];
                    $code_data['pro_code'] = $promo_code;
                    $code_data['node_id'] = $node->id;

                    $code = new Code($code_data);
                    if ($code->save()) {
                        $url = $this->productUrl . $promo_code;
                        if (env("APP_ENV", "") == 'local') {
                            $urlShort = false;
                        }else{
//                            $urlShort = Shorty::shorten($url);
                            $urlShort = false;
                        }
                        $promoMsg = LaravelMsg91::message($user->phone, 'Your Promo code :' . $promo_code . '. or click : ' . ($urlShort ? $urlShort : $url));
                        if ($promoMsg->type != 'success') {
                            throw new \Exception("Promo code sending failed");
                        }
                    }else{
                        throw new \Exception("Code Creation error");
                    }

                } catch (\Exception $e){
                    Log::error($e->getMessage() . ' Line Number : ' . $e->getLine());
                    Log::error($e->getTraceAsString());
                    $user->destroy($user->id);
                    return redirect('join')->withInput()->with(['status' => false, 'message' => 'Sorry! Something Went wrong.Please try again']);
                }

                return redirect($url);

            }
        }
    }


    public function test()
    {


        $url = "http://wayonn.in";

        var_dump(Shorty::shorten($url));
        exit;
        $node = Node::where('id', 8)->where('user_id', 13)->with('user');
        if ($node->count() > 0) {
            $data = $node->get()->first();
            $amount = ShareContribution::where('nodes_id', 8)->sum('amount');
//            $partners = Node::where('parentnode_id', 8)->count();

            $partners = Node::where('levels', 'like', '%#' . 8 . '#%')->count();

            $output['name'] = $data->user->name;
            $output['email'] = $data->user->email;
            $output['amount'] = $amount;
            $output['totalPartners'] = $partners;
            $output['refer_code'] = $data->ref_code;
            $output['share_url'] = url('join?ref=' . $data->ref_code);

            $success['status'] = true;
            $success['data'][] = $output;
            return response(['success' => $success]);
        }else{
            $success['status'] = false;
            $success['message'] = 'no_data';
            $success['data'] = [];
            return response(['success' => $success]);
        }
    }
}
