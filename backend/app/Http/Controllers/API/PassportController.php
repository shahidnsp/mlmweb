<?php

namespace App\Http\Controllers\API;

use App\Node;
use Illuminate\Console\Parser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Response;
use App\ShareContribution;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

/**
 *
 */
class PassportController extends Controller
{
    //

    /**
     *Login API
     *
     * @return \illuminate\Http\Response
     *
     */
    public function login(Request $request)
    {
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            $user = Auth::user();
            $success['status'] = true;
            $success['pwd_state'] = ($user->default_pwd == true ? true : false);
            $success['token']=$user->createToken('MyApp')->accessToken;
            return Response::json(['success' => $success]);
        } else {
            $failed = ['status' => false, 'message' => 'Unauthorised'];
            return Response::json(['success' => $failed]);
        }
    }

    /**
     *Logout
     */
    public function logout(Request $request)
    {
        if ($request->user()->token()->revoke()) {
            return Response::json(['success' => 'logout']);
        }
    }

    /**
     *
     */
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|max:15',
            'confirmPassword' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            $success['status'] = false;
            $success['errors'] = $validator->errors();

            return response(['success' => $success]);
        }
        $user_id = Auth::user()->id;

        $user = User::find($user_id);
        $data['password'] = bcrypt($request->password);
        $data['default_pwd'] = false;

        if ($user->update($data)) {
            $success['status'] = true;
            $success['message'] = 'success';
            return response()->json(['success' => $success]);
        }else{
            $success['status'] = false;
            $success['message'] = 'update_not_success';
            return response()->json(['success' => $success]);
        }
    }


    /**
     * Get List of Node by Authenticated User.........
     */
    public function getMyNodeList(){
        $user_id = Auth::user()->id;
        $nodes = Node::where('user_id', $user_id)->get();
        if ($nodes->count()>0) {
            $success['status'] = true;
            $success['data'] = $nodes;
            return response(['success' => $success]);
        }else{
            $success['status'] = false;
            $success['message'] = 'no_data';
            $success['data'] = [];
            return response(['success' => $success]);
        }

    }

    /**
     * Get Home page details
     * @param Request $request (node_id)
     * @internal
     * @return  Response
     */
    public function getNodeDetails(Request $request)
    {
        $user = Auth::user();
        $node = Node::where('id', $request->node_id)->where('user_id', $user->id)->where('active', 1)->with('user');
        if ($node->count() > 0) {

            $data = $node->get()->first();

            /*get Total income*/
            $amount = ShareContribution::where('nodes_id', $request->node_id)->sum('amount');

            /*Get direct child partners*/
            $childresult = Node::where('parentnode_id', $request->node_id)->where('active', 1)->with('user')->get();

            $childData = null;
            if ($childresult->count() > 0) {
                $childData = $childresult->last();
                $count = $childresult->count() - 1;
                $child['id'] = $childData->id;
                $child['name'] = (strlen($childData->user->name) > 9 ? substr($childData->user->name, 0, 8) . '..+' . $count : $childData->user->name);
                $child['avatar'] = url('avatars/' . $childData->user->photo);
            }

            if($childData == null){
                $child['id'] = '';
                $child['name'] = 'No activity';
                $child['avatar'] = url('avatars/profile.png');
            }

            /*Get all child partners count*/
            $partners = Node::where('levels', 'like', '%#' . $request->node_id . '#%')->where('active', 1)->count();

            /*get parent count*/
//            $level = $data->levels;
//            $parent = count(array_filter(explode("#", $level)));

            /*Get Parent details*/
            $parentData = Node::where('id', $data->parentnode_id)->where('active', 1)->with('user')->get()->first();
            $parent['id'] = $parentData->id;
            $parent['name'] = $parentData->user->name;
            $parent['avatar'] = url('avatars/' . $parentData->user->photo);

            /*get recent added partners count*/
            $recentList = Node::where('levels', 'like', '%#' . $request->node_id . '#%')->where('active', 1)->with('user')->get()->last();

            if ($recentList != null) {
                $recent['id'] = $recentList->id;
                $recent['name'] = $recentList->user->name;
                $recent['avatar'] = url('avatars/' . $recentList->user->photo);
            }

            if ($recentList == null) {
                $recent['id'] = '';
                $recent['name'] = 'No activity';
                $recent['avatar'] = url('avatars/profile.png');
            }

            $output['name'] = $data->user->name;
            $output['email'] = $data->user->email;
            $output['avatar'] = url('avatars/' . $data->user->photo);
            $output['amount'] = $amount;
            $output['totalPartners'] = $partners;
            $output['child'] = $child;
            $output['recent'] = $recent;
            $output['parent'] = $parent;
            $output['refer_code'] = $data->ref_code;
            $output['share_url'] = url('join?ref=' . $data->ref_code);

            $success['status'] = true;
            $success['data'][] = $output;
            return response(['success' => $success]);
        }else{
            $success['status'] = false;
            $success['message'] = 'no_data';
            $success['data'] = [];
            return response(['success' => $success]);
        }
    }


    /**
     * Get Home page details
     * @param Request $request (parentnode_id,currentnode_id)
     * @internal
     * @return  Response
     */
    public function getChild(Request $request)
    {
        if ($request->currentnode_id == 1) {
            $current_parent = Node::where('id', 1)->get()->first();
            $parent = Node::where('id', $request->parentnode_id)->with('user');

            if ($parent->count() > 0) {
                if ($request->parentnode_id == $request->currentnode_id) {
                    $levels[] = $parent->get()->first()->parentnode_id;
                    $levels[] = $request->parentnode_id;
                } elseif ($current_parent->parentnode_id == $request->parentnode_id) {
                    $success['status'] = true;
                    $success['message'] = 'not_permission';
                    return response(['success' => $success]);
                } else {
                    $data = $parent->get()->first()->toArray();
                    $levels = explode('#', $data['levels']);

                    /*Delete null index and reset*/
                    $levels = array_merge(array_filter($levels));
                    array_unshift($levels, $request->parentnode_id);
                }

                $nodes = Node::whereIn('id', $levels)->with('user')->get();
                foreach ($nodes as $node) {
                    if ($node->id == $request->currentnode_id) {
                        $node->node_name = 'me';
                    } else {
                        $node->node_name = $node->user->name;
                    }
                }


                $output['levels'][] = $nodes;

                $child = Node::where('parentnode_id', $request->parentnode_id);
                if ($child->count() > 0) {
                    $data = $child->with('user')->get();
                    foreach ($data as $value) {
                        $value->node_name = $value->user->name;
                    }
                    $output['nodes'] = $data;
                }

                $success['status'] = true;
                $success['data'][] = $output;
                return response(['success' => $success]);
            }else{
                $success['status'] = false;
                $success['message'] = 'no_parent';
                return response(['success' => $success]);
            }
        }else {
            if ($request->parentnode_id) /*Get Parent Node*/ {
                try {
                    $current_parent = Node::where('id', $request->currentnode_id)->where('active', 1)->get()->first();
                    $parent = Node::where('id', $request->parentnode_id)->where('active', 1)->with('user');

                    if ($parent->count() > 0) {
                        if ($request->parentnode_id == $request->currentnode_id) {
                            $levels[] = $parent->get()->first()->parentnode_id;
                            $levels[] = $request->parentnode_id;
                        } elseif ($current_parent->parentnode_id == $request->parentnode_id) {
                            $success['status'] = true;
                            $success['message'] = 'not_permission';
                            return response(['success' => $success]);
                        } else {

                            $data = $parent->get()->first()->toArray();
                            $levels = explode('#', $data['levels']);

                            /*Delete null index and reset*/
                            $levels = array_merge(array_filter($levels));
                            /*Parent filter (only get one parent)*/
                            foreach ($levels as $key => $level) {
                                if ($levels[$key] != $current_parent->parentnode_id) {
                                    unset($levels[$key]);
                                } else {
                                    break;
                                }
                            }
                            array_unshift($levels, $request->parentnode_id);
                        }
                        $nodes = Node::whereIn('id', $levels)->where('active', 1)->with('user')->get();
                        foreach ($nodes as $node) {
                            $node->join = date('d-m-Y', strtotime($node->created_at));
                            $node->username = $node->user->name;
                            $node->email = $node->user->email;
                            $node->phone = $node->user->phone;
                            $node->avatar = url('avatars/' . $node->user->photo);

                            if ($node->id == $request->currentnode_id) {
                                $node->node_name = 'me';
                            } else {
                                $node->node_name = $node->user->name;
                            }
                        }

                        $output['levels'][] = $nodes;
                    }

                    $child = Node::where('parentnode_id', $request->parentnode_id)->where('active', 1);
                    if ($child->count() > 0) {
                        $data = $child->with('user')->get();
                        foreach ($data as $value) {
                            $value->node_name = $value->user->name;
                            $value->join = date('d-m-Y', strtotime($value->created_at));
                            $value->username = $value->user->name;
                            $value->email = $value->user->email;
                            $value->phone = $value->user->phone;
                            $value->avatar = url('avatars/' . $value->user->photo);
                        }
                        $output['nodes'] = $data;
                        $success['status'] = true;
                    }else{
                        $success['status'] = false;
                    }


                    $success['data'][] = $output;
                    return response(['success' => $success]);
                } catch (\Exception $e) {
                    Log::error($e->getMessage() . ' Line Number : ' . $e->getLine());
                    Log::error($e->getTraceAsString());
                    $success['status'] = false;
                    $success['message'] = 'failed';
                    return response(['success' => $success]);
                }
            } else {
                $success['status'] = false;
                $success['message'] = 'parent_id_not_found';
                return response(['success' => $success]);
            }
        }
    }

    /**
     *
     */
    public function profile()
    {
        $user = Auth::user();
        $output['name'] = $user->name;
        $output['lastname'] = $user->lastname;
        $output['email'] = $user->email;
        $output['phone'] = $user->phone;
        $output['avatar'] = url('avatars/' . $user->photo);

        $success['status'] = true;
        $success['data'][] = $output;
        return response(['success' => $success]);
    }


    /**
     *
     */
    public function UpdateProfile(Request $request)
    {
        $user_id = Auth::user()->id;
        /*$validator = Validator::make($request->all(), [
            'password' => 'nullable|min:8|max:15',
            'confirmPassword' => 'nullable|same:password',
        ]);


        if ($validator->fails()) {
            $success['status'] = false;
            $success['errors'] = $validator->errors();
            return response(['success' => $success]);
        }*/
        $user = User::find($user_id);

        if ($user) {
            $user->name = $request->name;
            $user->lastname = $request->lastname;
            $user->email = $request->email;
            $user->phone = $request->phone;
            /*if ($request->has('password')) {
                $user->password = bcrypt($request->phone);
            }*/
            try{
                $user->save();
                $success['status'] = true;
                $success['message'] = 'updated_success';
                $success['data'] = $request->all();
                return response(['success' => $success]);
            }
            catch(\Exception $e){
                Log::error($e->getMessage() . ' Line Number : ' . $e->getLine() . 'user_id => ' . $user_id);
                Log::error($e->getTraceAsString());
                $success['status'] = false;
                $success['message'] = 'user_update_failed';
                return response(['success' => $success]);
            }
        }
    }

    public function uploadProfile(Request $request)
    {

        $userData = Auth::user();
        if ($request->has('image')) {
            $photoName = time() . '.' . $request->image->getClientOriginalExtension();
//            $request->image->move(public_path('avatars'), $photoName);
            $photo = Image::make($request->image->getRealPath())->resize(100, 100);
            $photo->save(public_path('avatars/') . $photoName, 80);

            $user = User::find($userData->id);
            $user->photo = $photoName;
            try{
                $user->save();
                $success['status'] = true;
                $success['message'] = 'upload_success';
                return response(['success' => $success]);
            }
            catch(\Exception $e){
                Log::error($e->getMessage() . ' Line Number : ' . $e->getLine() . 'user_id => ' . $userData->id);
                Log::error($e->getTraceAsString());
                $success['status'] = false;
                $success['message'] = 'upload_failed';
                return response(['success' => $success]);
            }
        }
        else{
            Log::error('Field not found.  user_id => ' . $userData->id);
            $success['status'] = false;
            $success['message'] = 'file_not_found';
            return response(['success' => $success]);
        }
    }

    /**
     *
     */
    public function getDetails()
    {
        //return 'jj';
        $user = Auth::user();
        return Response::json(['success' => $user]);
    }



}
