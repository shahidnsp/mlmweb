<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index(Request $request){
        if (!$request->session()->exists('myproduct_id')) {
            // myproduct_id value cannot be found in session
            return redirect('/products');
        }
        return view('web.checkout');
    }

    public function purchase($id){

        Session::put('myproduct_id', $id);

        return redirect('/checkout');
    }
}
