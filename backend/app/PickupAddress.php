<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickupAddress extends Model
{
    protected $fillable=['users_id','name','lastname','phone','email','address','postoffice','district','state','pincode'];
}
