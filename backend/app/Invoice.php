<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = ['invoice_no', 'product_id', 'amount', 'order_id', 'downloaded'];

    function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
