<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{

    protected $fillable = ['node_name', 'user_id', 'product_id', 'parentnode_id', 'ref_code', 'levels', 'active', 'order_id'];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product','product_id');
    }
}
