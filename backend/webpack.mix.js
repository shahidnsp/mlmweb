let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js')//not using


/*mix.sass('resources/assets/sass/app.scss', 'public/css')
    .copyDirectory('resources/assets/img', 'public/img')
    .copyDirectory('resources/assets/css', 'public/css/login')
    .copyDirectory('resources/assets/font-awesome', 'public/font-awesome')
    .copyDirectory('resources/assets/js', 'public/js')
    .copyDirectory('node_modules/angular-loading-bar/build/loading-bar.css', 'public/css')
    .copyDirectory('resources/assets/css/angular-confirm.min.css', 'public/css')
    .copyDirectory('resources/assets/ckeditor', 'public/ckeditor');

mix.copyDirectory('resources/assets/front-end/js', 'public/js')
    .copyDirectory('resources/assets/front-end/css', 'public/css')
   .copyDirectory('resources/assets/front-end/img', 'public/img')
    .copyDirectory('resources/assets/front-end/fonts', 'public/fonts');*/

mix.combine(['resources/assets/js/angular/angular.min.js',
    'resources/assets/js/angular/angular-resource.min.js',
    'resources/assets/js/angular/angular-route.min.js',
    //'resources/assets/js/angular/ui-bootstrap-tpls.min.js',
    'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
    'resources/assets/js/angular/angular-notify.js',
    /*              'resources/assets/js/angular/textAngular-rangy.min.js',
     'resources/assets/js/angular/textAngular-sanitize.min.js',
     'resources/assets/js/angular/textAngular.min.js',*/
    'resources/assets/js/angular/angular-ckeditor.js',
    'node_modules/chart.js/dist/Chart.js',
    'node_modules/angular-chart.js/dist/angular-chart.js',
    'node_modules/angular-loading-bar/build/loading-bar.js',
    'node_modules/angular-utils-pagination/dirPagination.js',
    'resources/assets/js/angular/angular-confirm.js',

    'resources/assets/js/angular/angularscript.js',
    'resources/assets/js/angular/controller/*',
    'resources/assets/js/angular/service/*'
], 'public/js/app.js');

