(function ($, window, document, undefined) {
    'use strict';
    
	$(document).ready(function() {		

	  	// Parallax Background
	  	jQuery('.contact-parallax').parallax;

	  	// Owl Carousel		
		jQuery("#ws-items-carousel").owlCarousel({
			items :4,		
			navigation : true
		});

		// Products Carousel 
		jQuery("#ws-products-carousel").owlCarousel({
			navigation : true,
			slideSpeed : 300,
			paginationSpeed : 400,
			singleItem:true
		});	
		
		// Page Loader
	    jQuery("#preloader").delay(1500).fadeOut(500);       	
		 
		// Scroll Reveal
		window.sr = new scrollReveal();

		// Hover Dropdown
		
		
		// Hero Image	
		jQuery("#ws-hero").show().revolution({
			sliderType:"hero",
			jsFileLocation:"assets/js/plugins/revolution/js/",
			fullScreenOffsetContainer:".ws-header-static,.ws-topbar",
			sliderLayout:"fullscreen",
			dottedOverlay:"none",
			delay:9000,
			navigation: {
			},
			responsiveLevels:[1240,1024,778,480],
			gridwidth:[1240,1024,778,480],
			gridheight:[600,500,400,300],
			lazyType:"none",
			parallax: {
				type:"mouse",
				origo:"slidercenter",
				speed:1000,
				levels:[2,3,4,5,6,7,12,16,10,50],
			},
			shadow:0,
			spinner:"off",
			autoHeight:"off",
			disableProgressBar:"on",
			hideThumbsOnMobile:"off",
			hideSliderAtLimit:0,
			hideCaptionAtLimit:0,
			hideAllCaptionAtLilmit:0,
			debugMode:false,
			fallbacks: {
				simplifyAll:"off",
				disableFocusListener:false,
			}
		});
		
		// Hero FullScreen	
		jQuery("#ws-hero-fullscreen").show().revolution({
			sliderType:"hero",
			jsFileLocation:"assets/js/plugins/revolution/js/",
			fullScreenOffsetContainer:".ws-topbar",
			sliderLayout:"fullscreen",
			dottedOverlay:"none",
			delay:9000,
			navigation: {
			},
			responsiveLevels:[1240,1024,778,480],
			gridwidth:[1240,1024,778,480],
			gridheight:[600,500,400,300],
			lazyType:"none",
			parallax: {
				type:"mouse",
				origo:"slidercenter",
				speed:1000,
				levels:[2,3,4,5,6,7,12,16,10,50],
			},
			shadow:0,
			spinner:"off",
			autoHeight:"off",
			disableProgressBar:"on",
			hideThumbsOnMobile:"off",
			hideSliderAtLimit:0,
			hideCaptionAtLimit:0,
			hideAllCaptionAtLilmit:0,
			debugMode:false,
			fallbacks: {
				simplifyAll:"off",
				disableFocusListener:false,
			}
		});	

		// Fullscreen Slider
		jQuery("#ws-fullscreen-slider").revolution({
			sliderType:"standard",
			fullScreenOffsetContainer:".ws-topbar",
			sliderLayout:"fullscreen",
			delay:9000,
			navigation: {
				arrows:{enable:true}				
			},			
			gridwidth:1230,
			spinner:"off",
			disableProgressBar:"on",
			gridheight:720		
		});	

		// Fullwidth
		jQuery("#ws-fullwidth-slider").revolution({
			sliderType:"standard",
			fullScreenOffsetContainer:".ws-topbar",
			sliderLayout:"fullscreen",
			delay:9000,
			navigation: {
				arrows:{enable:true}				
			},			
			gridwidth:1230,
			spinner:"off",
			disableProgressBar:"on",
			fullWidth:"on",
			forceFullWidth:"on",
			gridheight:500		
		});	

		// 3D Parallax Slider	
	    jQuery("#ws-3d-parallax").show().revolution({
	        sliderType: "hero",
	        fullScreenOffsetContainer:".ws-topbar, .ws-header-static",
	        jsFileLocation: "assets/js/plugins/revolution/js/",            
	        sliderLayout: "fullscreen",
	        dottedOverlay: "none",
	        delay: 9000,
	        navigation: {},
	        responsiveLevels: [1240, 1024, 778, 480],
	        visibilityLevels: [1240, 1024, 778, 480],
	        gridwidth: [1400, 1240, 778, 480],
	        gridheight: [768, 768, 960, 720],
	        lazyType: "none",
	        parallax: {
	            type: "3D",
	            origo: "slidercenter",
	            speed: 1000,
	            levels: [5, 10, 15, 20, 25, 30, 5, 0, 45, 50, 47, 48, 49, 50, 51, 55],
	            ddd_shadow: "off",
	            ddd_bgfreeze: "off",
	            ddd_overflow: "hidden",
	            ddd_layer_overflow: "visible",
	            ddd_z_correction: 65,
	        },
	        spinner: "off",
	        autoHeight: "off",
	        disableProgressBar: "on",
	        hideThumbsOnMobile: "off",
	        hideSliderAtLimit: 0,
	        hideCaptionAtLimit: 0,
	        hideAllCaptionAtLilmit: 0,
	        debugMode: false,
	        fallbacks: {
	            simplifyAll: "off",
	            disableFocusListener: false,
	        }
	    });    

    });

})(jQuery, window, document);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {

    /*OTP send*/
    $('#phoneForm').submit(function (e) {
        e.preventDefault();

        var form = this;
        /*start spinner*/
        var btn = $("button", this);
        $(btn).buttonLoader('start');
        /*end*/

        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.post(url, data)
            .done(function (response) {
                $("input[name='phone']", form).attr("disabled", true);
                $('#otpForm').removeClass('hidden');
                $(btn).buttonLoader('stop');
                btn.attr("disabled", true);
                setTimeout(function () {
                    $('#anc-resend').removeClass('hidden');
                }, 7000);
            })
            .fail(function (response) {
                $(btn).buttonLoader('stop');
                if (response.responseJSON.message == "invalid_reference_code") {
                    $("#refCodeForm input[name='reference']").focus();
                    $('#refCodeForm #error').html('Invalid Reference Code');
                }
            })
    });


    /*OTP Verify*/

    $('#otpForm').submit(function (e) {
        e.preventDefault();

        $.preloader.start({
            modal: true,
            src: 'sprites1.png'
        });
        $('#otpError').html('');

        var form = this;

        /*start spinner*/
        var btn = $("button", this);
        $(btn).buttonLoader('start');
        /*end*/

        var url = $(this).attr('action');
        var data = $(this).serialize();
        $.post(url, data)
            .done(function (response) {
                $(btn).buttonLoader('stop');
                $('#f-d-form').addClass('hidden');
                $('#checkout-form').removeClass('hidden');
                $("#checkout-form input[name='phone']").val(response.data.phone);
                $("#checkout-form input[name='phone']").attr('disabled', true);
                var phone = response.data.phone;
                $('#checkout-form').append(
                    $('<input>', {
                        type: 'hidden',
                        name: 'phone',
                        value: phone//response.data.phone
                    })
                );

                if(response.user_exist == true) {
                    $("#checkout-form input[name='name']").val(response.data.name).attr('disabled', true);
                    $("#checkout-form input[name='lastname']").val(response.data.lastname).attr('disabled', true);
                    $("#checkout-form input[name='email']").val(response.data.email).attr('disabled', true);

                    $('<input>').attr({type: 'hidden',name: 'name',value: response.data.name}).appendTo('#checkout-form');
                    $('<input>').attr({type: 'hidden',name: 'firstname',value: response.data.firstname}).appendTo('#checkout-form');
                    $('<input>').attr({type: 'hidden',name: 'email',value: response.data.email}).appendTo('#checkout-form');
                    $('<input>').attr({type: 'hidden',name: 'user_id',value: response.data.id}).appendTo('#checkout-form');

                }
            })
            .fail(function () {
                $('#otpError').html('Invalid OTP');
                $(btn).buttonLoader('stop');
            })
            .always(function () {
                $.preloader.stop();
            });

    });

    $('#resendOtp').click(function (e) {
        e.preventDefault();

        $.preloader.start({
            modal: true,
            src: 'sprites1.png'
        });
        $("#phoneForm input[name='phone']").attr("disabled", false);
        //$("#phoneForm button").attr("disabled", false);

        $('#anc-resend').addClass('hidden');
        var form = $('#phoneForm');
        /*start spinner*/
        var btn = $("button", form);
        $(btn).buttonLoader('start');
        /*end*/

        var url = $(form).attr('action');
        var data = $(form).serialize();

        $.post(url, data)
            .done(function (response) {
                $("input[name='phone']", form).attr("disabled", true);
                $('#otpForm').removeClass('hidden');
                $(btn).buttonLoader('stop');
                btn.attr("disabled", true);
                setTimeout(function () {
                    $('#anc-resend').removeClass('hidden');
                }, 7000);
            })
            .fail(function () {
                console.log("error");
                $(btn).buttonLoader('stop');
            })
            .always(function () {
                $.preloader.stop();
            });
    });

    $('#refCodeForm').submit(function (e) {
        e.preventDefault();
        $.preloader.start({
            modal: true,
            src: 'sprites1.png'
        });

        var form = $(this);
        $('#error', form).html('');

        var url = $(form).attr('action');
        var data = $(form).serialize();

        $.post(url, data)
            .done(function (response) {
                $("input[name='reference']", form).val(response.data.username);
                $("input[name='reference']", form).attr("disabled", true);
                $("button", form).attr("disabled", true);
                $('reference', form).addClass('hidden');
            })
            .fail(function () {
                $('#error', form).html('Invalid Reference Code');
            })
            .always(function () {
                $.preloader.stop();
            });
    });

/*Send OTP TO other No*/

    $('#otpToOther').click(function(e) {
        e.preventDefault();
        $("#phoneForm input[name='phone']").val("+91");
        $("#phoneForm input[name='phone']").attr("disabled", false);
        $("#phoneForm button").attr("disabled", false);

    })

});

