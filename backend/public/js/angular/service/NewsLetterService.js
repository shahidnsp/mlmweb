/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('NewsLetterService',[]).factory('NewsLetter',['$resource',
    function($resource){
        return $resource('/app/newsletter/:newsletterId',{
            newsletterId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);