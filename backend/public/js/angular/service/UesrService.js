/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('UserService',[]).factory('User',['$resource',
    function($resource){
        return $resource('/app/user/:userId',{
            userId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);