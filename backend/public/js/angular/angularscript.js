

var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ui.bootstrap',
        'ngNotify',
        'ckeditor',
        'UserService',
        'SliderService',
        'ProductService',
        'TestimonialService',
        'ContactService',
        'SupportService',
        'NewsLetterService',
        'OrderService',
        'chart.js',
        'angularRandomString',
        'angular-loading-bar',
        'angularUtils.directives.dirPagination'
    ]);

/*app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});*/
app.config(function($routeProvider, $locationProvider,cfpLoadingBarProvider) {


    //cfpLoadingBarProvider.parentSelector = '#loading';
    /*
     * loading bar configuration
     * https://github.com/chieffancypants/angular-loading-bar
     * */
    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner"></div>';
    cfpLoadingBarProvider.latencyThreshold = 500;

    //$locationProvider.html5Mode(true);
    $routeProvider
        .when('/', {
            templateUrl: 'template/dashboard',
            controller: 'DashboardController'
        })
        .when('/dashboard', {
            templateUrl: 'template/dashboard',
            controller: 'DashboardController'
        })
        .when('/executive', {
            templateUrl: 'template/executives',
            controller: 'ExecutiveController'
        })
        .when('/slider', {
            templateUrl: 'template/slider',
            controller: 'SliderController'
        })
        .when('/product', {
            templateUrl: 'template/product',
            controller: 'ProductController'
        })
        .when('/testimonial', {
            templateUrl: 'template/testimonial',
            controller: 'TestimonialController'
        })
        .when('/contact', {
            templateUrl: 'template/contact',
            controller: 'ContactController'
        })
        .when('/support', {
            templateUrl: 'template/support',
            controller: 'SupportController'
        })
        .when('/newsletter', {
            templateUrl: 'template/newsletter',
            controller: 'NewsLetterController'
        })
        .when('/sendnewsletter', {
            templateUrl: 'template/sendnewsletter',
            controller: 'SendNewsLetterController'
        })
        .when('/appuser', {
            templateUrl: 'template/appuser',
            controller: 'AppUserController'
        })
        .when('/orderlist', {
            templateUrl: 'template/order',
            controller: 'OrderController'
        })
        .otherwise({
            redirectTo: 'template/dashboard'
        });
});

app.directive('showDuringResolve', function($rootScope) {

    return {
        link: function(scope, element) {

            element.addClass('ng-hide');

            var unregister = $rootScope.$on('$routeChangeStart', function() {
                element.removeClass('ng-hide');
            });

            scope.$on('$destroy', unregister);
        }
    };
});

app.filter('pagination', function() {
    return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
            var start = (currentPage-1)*pageSize;
            var end = currentPage*pageSize;
            return input.slice(start, end);
        }
    };
});

app.filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {
        return $filter('number')(input * 100, decimals) + '%';
    };
}]);
app.filter('sum', function(){
    return function(items, prop){
        return items.reduce(function(a, b){
            return a + b[prop];
        }, 0);
    };
});



app.directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var isMultiple = attrs.multiple;
            var modelSetter = model.assign;
            element.bind("change", function (changeEvent) {
                var values = [];

                for (var i = 0; i < element[0].files.length; i++) {
                    var reader = new FileReader();

                    reader.onload = (function (i) {
                        return function(e) {
                            var value = {
                                lastModified: changeEvent.target.files[i].lastModified,
                                lastModifiedDate: changeEvent.target.files[i].lastModifiedDate,
                                name: changeEvent.target.files[i].name,
                                size: changeEvent.target.files[i].size,
                                type: changeEvent.target.files[i].type,
                                data: e.target.result
                            };
                            values.push(value);
                        }

                    })(i);

                    reader.readAsDataURL(changeEvent.target.files[i]);
                }


                scope.$apply(function () {
                    if (isMultiple) {
                        modelSetter(scope, values);
                    } else {
                        modelSetter(scope, values[0]);
                    }
                });
            });
        }
    }
}]);

angular.module('angularRandomString', []).factory(
    'randomString',
    ['$window',
        function randomStringFactory(w){

            var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            var Math = w.Math;

            return function randomString(length) {
                length = length || 10;
                var string = '', rnd;
                while (length > 0) {
                    rnd = Math.floor(Math.random() * chars.length);
                    string += chars.charAt(rnd);
                    length--;
                }
                return string;
            };
        }
    ]
);

