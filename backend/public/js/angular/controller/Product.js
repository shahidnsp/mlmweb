app.controller('ProductController', function($scope,$http,$anchorScroll,ngNotify,Product){

    $scope.productedit=false;
    $scope.products=[];

    // Editor options.
    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false
    };

    Product.query(function(product){
        $scope.products=product;
        console.log(product);
    });

    $scope.newProduct = function () {
        $scope.productedit = true;
        $scope.newproduct = new Product();
        $scope.curProduct = {};
    };

    $scope.editProduct = function (thisProduct) {
        $scope.productedit = true;
        $scope.curProduct = thisProduct;
        $scope.newproduct = angular.copy(thisProduct);
        $anchorScroll();
    };

    $scope.addProduct = function () {

        if ($scope.curProduct.id) {
            $scope.newproduct.$update(function(product){
                angular.extend($scope.curProduct, $scope.curProduct, product);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Product Updated Successfully');
            });
        } else{
            $scope.newproduct.$save().
                then(function (response) {
                    $scope.products.push(response);
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });
                    ngNotify.set('Product Saved Successfully');

                    $scope.newproduct = new Product();
                }, function (response) {
                    $scope.validationErrors = response.data;
                });
        }
        $scope.productedit = false;
    };

    $scope.deleteProduct = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.products.indexOf(item);
                $scope.products.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Product Removed Successfully');
            });
        }
    };

    $scope.cancelProduct=function(){
        $scope.productedit=false;
        $scope.newproduct = new Product();
    };

    $scope.changeProductStatus = function (product) {
        $http.post('/app/changeProductStatus', {id: product.id}).
            success(function (data, status, headers, config) {
                angular.extend(product, product, data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };


    $scope.exportToPdf = function () {

        $http.post('app/product/exportToPdf', {}, {
            responseType: 'arraybuffer',
            headers: {
                //accept: 'application/pdf'
            }
        })
            .then(function (response) {
                var file = new Blob([response.data], {type: 'application/pdf'});
                var fileURL = window.URL.createObjectURL(file);

                //window.open(fileURL);
                //Create anchor
                var a = window.document.createElement('a');
                a.href = fileURL;
                a.download = 'Product.pdf';
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);

            }, function (response) {
                console.log(response.data || 'Request failed');
            });
    };


});