/**
 * Created by psybo-03 on 18/1/18.
 */
app.controller('OrderController', function ($scope, $http, $anchorScroll, ngNotify, User, Order, $window) {

    $scope.orderedit = false;
    $scope.users = [];
    $scope.orders = [];

    User.query(function (user) {
        $scope.users = user;
        console.log(user);
    });

    Order.query(function (order) {
        $scope.orders = order;
        console.log(order);
    });

    $scope.orderStatusUpdate = function (order, status) {
        $http.post('/app/changeOrderStatus', {id: order.id, status: status}).
            success(function (data, status, headers, config) {
                angular.extend(order, order, data);
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
    };


    $scope.searchOrder = function (search) {
        $http.post('/app/searchOrder', {status: search}).
            success(function (data, status, headers, config) {
                $scope.orders = data;
            }).error(function (data, status, headers, config) {
            });
    };

    $scope.exportToExcel = function (status) {
        alert('excel');
    };
    $scope.exportToPdf = function (status) {

        $http.post('app/order/exportToPdf', {status: status}, {
            responseType: 'arraybuffer',
            headers: {
                accept: 'application/pdf'
            }
        })
            .then(function (response) {
                var file = new Blob([response.data], {type: 'application/pdf'});
                var fileURL = window.URL.createObjectURL(file);

                //window.open(fileURL);
                //Create anchor
                var a = window.document.createElement('a');
                a.href = fileURL;
                a.download = 'Orders.pdf';
                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);

            }, function (response) {
                console.log(response.data || 'Request failed');
            });
    };

    $scope.printInvoice = function () {
        //var printContents = document.getElementById('print').innerHTML;
        //var popupWin = window.open('', '_blank', 'width=300,height=300');
        //popupWin.document.open();
        //popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="test.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
        //popupWin.document.close();

        $http.post('app/invoice/pdf', {invoice_id: 1}, {
            responseType: 'arraybuffer',
            headers: {
                accept: 'application/pdf'
            }
        })
            .then(function (response) {
                //var file = new Blob([response.data], {type: 'application/pdf'});
                //var fileURL = window.URL.createObjectURL(file);
                //
                //window.open(fileURL);
                ////window.print(fileURL);
                ////Create anchor
                //var a = window.document.createElement('a');
                //a.href = fileURL;
                //a.download = 'Orders.pdf';
                //document.body.appendChild(a);
                //a.click();
                //
                //// Remove anchor from body
                //document.body.removeChild(a);

                var pdfFile = new Blob([response.data], {
                    type: 'application/pdf'
                });
                var pdfUrl = URL.createObjectURL(pdfFile);
                var printwWindow = $window.open(pdfUrl);
                printwWindow.print();

            }, function (response) {
                console.log(response.data || 'Request failed');
            });
    };

    $scope.printAll = function (status) {
        $http.post('app/order/printAll', {status: status}, {
            responseType: 'arraybuffer',
            headers: {
                accept: 'application/pdf'
            }
        })
            .then(function (response) {
                var file = new Blob([response.data], {type: 'application/pdf'});
                var fileURL = window.URL.createObjectURL(file);

                //window.open(fileURL);
                //Create anchor
                var a = window.document.createElement('a');
                a.href = fileURL;
                a.download = 'InvoiceAll.pdf';

                document.body.appendChild(a);
                a.click();

                // Remove anchor from body
                document.body.removeChild(a);

            }, function (response) {
                console.log(response.data || 'Request failed');
            });
    };


});