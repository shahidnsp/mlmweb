app.controller('HomeController', function($scope,$http,$location,User,Order){
    /* paggination */
    //TODO factory
    $scope.extra=false;
    //if (localStorage.getItem("itemPerPage") === null) {
    //    localStorage.setItem("itemPerPage", 10);
    //}
    //console.log(localStorage.getItem("itemPerPage"));
    //function getPerPage(){
    //    return parseInt(localStorage.itemPerPage);
    //}
    //
    //$scope.changeNum = function (itemNum) {
    //    localStorage.itemPerPage = itemNum;
    //    $scope.numPerPage = getPerPage();
    //};

    $scope.name="";


    $scope.currentPage = 1;
    $scope.itemPerPage = [5, 10, 25, 50, 100]
    $scope.numPerPage = 10;

    /* Nav menu */


    $scope.menuClass = function(page) {
        var current = $location.path().substring(1);
        return page === current ? "active" : "";
    };

    //countUser();
    //function countUser() {
    //    $scope.count = 12;
    //}

    DashboardGrid();

    function DashboardGrid() {
        $http.get('/app/utility')
            .then(function successCallback(response) {
                console.log(response);
                $scope.totalIncome = response.data.total_income;
                $scope.users = response.data.users;
                $scope.membres = response.data.nodes;
                $scope.orders = response.data.orders;
                $scope.todayOrder = response.data.today_order;
            }, function errorCallback(response) {
                console.log(response);
                $scope.found = false;
            });
    }


    /*CHART START*/

    $scope.labels = ['Jan', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Jan', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug','Jan', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Jan', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug'];

    $scope.data = [
        65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40
    ];
    $scope.options = {
        barThickness: 10
    };

    /*CHART END*/

    $scope.refer = {};
    $scope.searchReferId = function () {
        $http.post('/app/searchReferId', $scope.refer)
            .then(function successCallback(response) {
                console.log(response);
                $scope.found = true;
            }, function errorCallback(response) {
                console.log(response);
                $scope.found = false;
            });
    };
});