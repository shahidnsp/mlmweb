app.controller('ContactController', function($scope,$http,$anchorScroll,ngNotify,Contact){

    $scope.contactedit=false;
    $scope.contacts=[];

    Contact.query(function(contact){
        $scope.contacts=contact;
        console.log(contact);
    });
    
    $scope.deleteContact = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.contacts.indexOf(item);
                $scope.contacts.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Contact Removed Successfully');
            });
        }
    };


    $scope.changeContactStatus=function(contact){
        $http.post('/app/changeContactStatus',{id:contact.id}).
            success(function(data,status,headers,config){
                angular.extend(contact,contact,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };


});