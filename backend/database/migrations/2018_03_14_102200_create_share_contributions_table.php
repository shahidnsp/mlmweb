<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('share_contributions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nodes_id');
            $table->integer('parentnodes_id')->nullable();
            $table->integer('orders_id');
            $table->string('level');
            $table->decimal('amount',8,2);
            $table->boolean('is_coordinator')->default(0);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('share_contributions');
    }
}
