<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname')->nullable();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('email');
            $table->string('pancard')->nullable();
            $table->string('father')->nullable();
            $table->string('dateofbirth')->nullable();
            $table->string('gender')->nullable();
            $table->string('phone')->nullable()->unique();
            $table->string('parentcode')->nullable();
            $table->string('pincode')->nullable();
            $table->string('postoffice')->nullable();
            $table->string('state')->nullable();
            $table->string('district')->nullable();
            $table->string('housename')->nullable();
            $table->string('taluk')->nullable();
            $table->string('address')->nullable();
            $table->string('nominee')->nullable();
            $table->string('nomineerelation')->nullable();
            $table->string('ifsccode')->nullable();
            $table->string('accountno')->nullable();
            $table->string('bankname')->nullable();
            $table->string('branch')->nullable();
            $table->string('pancopy')->nullable();
            $table->string('bankproof')->nullable();
            $table->string('prooftype')->nullable();
            $table->string('proofnumber')->nullable();
            $table->string('proofcopy')->nullable();
            $table->string('placement')->nullable();
            $table->string('placement_id')->nullable();
            $table->string('usertype');
//            $table->string('refercode');
//            $table->string('secretecode');
            $table->string('photo')->default('profile.png');
            $table->boolean('active')->default(1);
            $table->integer('parent_id')->nullable();
            $table->integer('verify_email')->default(0);
            $table->boolean('default_pwd')->default(true);
            $table->string('confirmation_code')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
