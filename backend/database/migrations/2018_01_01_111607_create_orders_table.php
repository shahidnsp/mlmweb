<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->string('orderno');
            $table->decimal('amount',8,2);
            $table->integer('users_id');
            $table->integer('pickup_addresses_id');
            $table->string('status')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('failurereason')->nullable();
            $table->integer('paymentstatus')->default(0);
            $table->string('parentrefer_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
