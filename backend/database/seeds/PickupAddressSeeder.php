<?php

use Illuminate\Database\Seeder;

class PickupAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pickup=factory(App\PickupAddress::class,10)->create();
    }
}
