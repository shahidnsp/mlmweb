<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $info = $this->command;
        Model::unguard();


        \App\User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'username' => 'admin@admin.com',
            'address' => 'address 0215',
            'phone' => 8089183626,
            'state' => 'manjeri',
            'photo' => 'profile.png',
            'usertype' => 'admin',
            'password' => bcrypt('admin'),
            'remember_token' => str_random(10),
        ]);

        \App\User::create([
            'name' => 'user',
            'email' => 'user@admin.com',
            'username' => 'user@admin.com',
            'address' => 'address 0215',
            'phone' => 80891836268,
            'state' => 'manjeri',
            'photo' => 'profile.png',
            'usertype' => 'user',
            'password' => bcrypt('admin'),
            'remember_token' => str_random(10),
        ]);
        \App\User::create([
            'name' => 'user1',
            'email' => 'user@user.com',
            'username' => 'user@user.com',
            'address' => 'address 0215',
            'phone' => 80891836267,
            'state' => 'manjeri',
            'photo' => 'profile.png',
            'usertype' => 'user',
            'password' => bcrypt('admin123'),
            'remember_token' => str_random(10),
        ]);

        $info->info('User table seeding started...');
        $this->call('UserSeeder');

        $info->info('Photo table seeding started...');
        $this->call('PhotoSeeder');

        $info->info('Slider table seeding started...');
        $this->call('SliderSeeder');

        $info->info('Member table seeding started...');
        $this->call('MemberSeeder');

        $info->info('Product table seeding started...');
        $this->call('ProductSeeder');

        $info->info('Testimonial table seeding started...');
        $this->call('TestimonialSeeder');

        $info->info('Contact table seeding started...');
        $this->call('ContactSeeder');

        $info->info('PickupAddress table seeding started...');
        $this->call('PickupAddressSeeder');

        $info->info('Support table seeding started...');
        $this->call('SupportSeeder');

        $info->info('Newsletter table seeding started...');
        $this->call('NewsletterSeeder');

        $info->info('OrderItem table seeding started...');
        $this->call('OrderItemSeeder');

        $info->info('Order table seeding started...');
        $this->call('OrderSeeder');

        $info->info('Node table seeding started...');
        $this->call('NodeSeeder');

        $info->info('Joint table seeding started...');
        $this->call('JointSeeder');

        $info->error('Seeding Completed.........');
        Model::reguard();

    }
}
