<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'username' => $faker->unique()->userName,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'state' => $faker->city,
        'photo' => 'profile.png',
        'usertype' => $faker->randomElement(['admin', 'subAdmin', 'user', 'executive']),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

//Photo Seed faker
$factory->define(App\Photo::class,function($faker){
    return [
        'name'=>$faker->randomElement(['pd1.jpg','pd2.jpg','pd3.png','pd4.png']),
        'products_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]),
    ];
});

//Slider Seed faker
$factory->define(App\Slider::class,function($faker){
    return [
        'name'=>$faker->randomElement(['slide.jpg','slide1.jpg','slide2.jpg','slide3.jpg']),
        'order'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'title'=>$faker->sentence,
        'subtitle'=>$faker->sentence,
    ];
});

//Member Seed faker
$factory->define(App\Member::class,function($faker){
    return [
        'users_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'parent_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'amount'=>'50',
    ];
});

//Product Seed faker
$factory->define(App\Product::class,function($faker){
    return [
        'name'=>$faker->name,
        'description'=>$faker->sentence,
        'brand'=>'Product Brand',
        'price'=>'50',
        'offerprice'=>'50',
        'pdctcode'=>$faker->randomElement([0213,1365]),
        'active'=>$faker->randomElement([0,1]),
    ];
});

//Testimonial Seed faker
$factory->define(App\Testimonial::class,function($faker){
    return [
        'name'=>$faker->name,
        'content'=>$faker->name,
        'designation'=>$faker->name,
        'photo' => 'profile.png',
        'active'=>$faker->randomElement([0,1]),
    ];
});

//Contact Seed faker
$factory->define(App\Contact::class,function($faker){
    return [
        'name'=>$faker->name,
        'email'=>$faker->email,
        'description'=>$faker->sentence,
        'phone'=>$faker->phoneNumber,
        'notified'=>$faker->randomElement([0,1]),
    ];
});

//Pickup Address Seed faker
$factory->define(App\PickupAddress::class,function($faker){
    return [
        'users_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'name'=>$faker->name,
        'lastname'=>$faker->name,
        'phone'=>$faker->phoneNumber,
        'email'=>$faker->email,
        'address'=>$faker->address,
        'postoffice'=>$faker->city,
        'district'=>$faker->city,
        'state'=>$faker->city,
        'pincode'=>$faker->postcode,
    ];
});

//Support Seed faker
$factory->define(App\Support::class,function($faker){
    return [
        'title'=>$faker->address,
        'description'=>$faker->sentence,
        'users_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'notified'=>$faker->randomElement([0,1]),
    ];
});

//News Letter Seed faker
$factory->define(App\Newsletter::class,function($faker){
    return [
        'email'=>$faker->email,
        'active'=>$faker->randomElement([0,1]),
    ];
});

//News Letter Seed faker
$factory->define(App\OrderItem::class,function($faker){
    return [
        'orders_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'products_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'qty'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'amount'=>$faker->randomElement([10,20,30,40,50,60]),
        'total'=>$faker->randomElement([10,20,30,40,50,60]),
    ];
});

//News Letter Seed faker
$factory->define(App\Order::class,function($faker){
    return [
        'date'=>$faker->date,
        'orderno'=>$faker->numberBetween(1000,10000),
        'amount'=>$faker->numberBetween(1000,10000),
        'users_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'pickup_addresses_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
    ];
});


//Joint Seed faker
$factory->define(App\Joint::class,function($faker){
    return [
        'node_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'parentnode_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9,10]),
        'amount'=>$faker->numberBetween(1000,10000),
    ];
});

//Nodes Seed faker
$factory->define(App\Node::class,function($faker){
    return [
        'user_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'product_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
        'active' => $faker->randomElement([1, 0]),
        'ref_code' => str_random(6),
        'node_name' => str_random(6)
    ];
});