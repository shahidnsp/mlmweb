
@extends('web.layouts.app')

@section('title', 'Wayonn | Contact Us')

@section('content')


        <!-- Page Content -->
        <div class="container ws-page-container">
            <div class="row">
                <div class="ws-contact-page">

                    <!-- Office Location -->
                    <div class="col-sm-12">
                        <div class="text-center">
                            <!-- Title -->
                            <h2 class="all-header">get-in touch</h2>
                            <div class="ws-separator"></div>
                            <p>You've got questions, we've got answers. <br />Our staff are help you implement your choices - You are in control.</p>
                            <div class="row">
                                <!-- phone -->
                                <div class="col-sm-6 col-sm-4 ws-contact-office-item" data-sr='wait 0.1s, ease-in 20px'>
                                    <div class="ws-contact-info shadower text-center">
                                        <i class="fa fa-phone" style="padding: 11px 16.5px"></i>
                                        <p>+91 (756) 093-1458</p>
                                        
                                    </div>
                                </div>
                                <!-- City -->
                                <div class="col-sm-6 col-sm-4 ws-contact-office-item" data-sr='wait 0.2s, ease-in 20px'>
                                    <div class="ws-contact-info shadower text-center">
                                        <i class="fa fa-map-marker"></i>
                                        <p>WAYONN MARKETING LLP  KIZHAKKETHALA,</p>
                                        <p>ROOM No. 24/139-T, MATTIL MALL,</p>
                                        <p>VENGARA ROAD, DOWNHILL POST,</p>
                                        <p>Malappuram, Kerala, 676519, India.</p>
                                        
                                    </div>
                                </div>
                                <!-- mail -->
                                <div class="col-sm-6 col-sm-4 ws-contact-office-item" data-sr='wait 0.3s, ease-in 20px'>
                                    <div class="ws-contact-info shadower text-center">
                                        <i class="fa fa-envelope" style="padding: 14px"></i>
                                        <p>WAYONNMARKETING@GMAIL.COM</p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Office Location -->

                    <!-- Office Location -->
                    <div class="col-sm-12 section-top">
                        <div class="text-center">
                            <!-- Title -->
                            <h2 class="all-header">mail us</h2>
                            <div class="ws-separator"></div>
                            <p>
                                Have a suggestion, have a question, or just want to say “hello”? Fill out the contact form below <br />and someone on my team will get back to you.
                            </p>
                        </div>
                        <div class="row">
                            <form action="{{ route('sendmessage') }}" method="post" class="section-top form-horizontal ws-contact-form">
                                {{ csrf_field() }}
                                <div class="col-md-offset-1 col-sm-5" data-sr='wait 0.1s, ease-in 20px'>
                                    <!-- Name -->
                                    <div class="form-group">
                                        <label class="control-label">Name <span>*</span></label>
                                        <input type="text" name="name" class="form-control border-none" required="">
                                    </div>

                                    <!-- Phone -->
                                    <div class="form-group">
                                        <label class="control-label">Phone <span>*</span></label>
                                        <input type="text" name="phone" class="form-control border-none" required="">
                                    </div>
                                </div>
                                <div class="col-sm-5" data-sr='wait 0.2s, ease-in 20px'>
                                    <!-- Email -->
                                    <div class="form-group">
                                        <label class="control-label">Email <span>*</span></label>
                                        <input type="email" name="email" class="form-control border-none" required="">
                                    </div>
                                    <!-- Email -->
                                    <div class="form-group">
                                        <label class="control-label">Email <span>*</span></label>
                                        <input type="email" name="email" class="form-control border-none" required="">
                                    </div>
                                </div>
                                <div class="col-md-offset-1 col-sm-10" data-sr='wait 0.2s, ease-in 20px'>
                                    <!-- Message -->
                                    <div class="form-group">
                                        <label class="control-label">Message <span>*</span></label>
                                        <textarea name="description" class="form-control border-none" rows="7" required=""></textarea>
                                    </div>

                                    <!-- Submit Button -->
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn ws-big-btn">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Office Location -->
                </div>
            </div>
        </div>
        <!-- End Page Content -->

@endsection


