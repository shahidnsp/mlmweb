
@extends('web.layouts.app')

@section('title', 'Wayonn | About Us')

@section('content')

        <!-- Page Parallax Header -->
        <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="{{asset('img/About-Us.jpg')}}">
            <div class="ws-overlay">
                <div class="ws-parallax-caption">
                    <div class="ws-parallax-holder">
                        <h1>WAYONN MARKETING LLP</h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Parallax Header -->

        <!-- Page Content -->
        <div class="container ws-page-container">
            <div class="row">
                <div class="ws-about-content col-sm-12">
                    <div class="row vertical-align">
                        <div class="col-sm-7" data-sr='wait 0.1s, ease-in 20px'>
                            <h3>why us</h3>
                            <div class="ws-footer-separator"></div>
                            <p>
                                Wayonn is a system for selling goods or services through a network of distributors marketing is a marketing strategy for sale of goods through individual experience. The typical Wayonn Marketing program works through recruitment of distributing individuals or distributors. The process is as described like this: You are invited to become a distributor (or contractor or consultant or associate), sometimes through another distributor of the company's products and sometimes through a generally advertised meeting.
                            </p>
                            <p>
                                If you choose to become a distributor with the direct selling company, you'll earn monetary benefits both through the sales of the Wayonn products and through the incentives on recruiting other distributors, this incentive is a portion of the income these distributors down the line generate. And when those distributors recruit distributors of their own, you'll earn incentives on the income they generate too. 
                            </p>
                        </div>
                        <div class="col-md-offset-1 col-sm-4">
                            <img src="{{asset('img/wayonn.png')}}" alt="Wayonn Marketing Logo" class="img-responsive">
                        </div>
                    </div>
                    <div class="row vertical-align">
                        <div class="col-sm-6">
                            <img src="{{asset('img/test.jpg')}}" alt="Wayonn Marketing Logo" class="img-responsive">
                        </div>
                        <div class="col-sm-6" data-sr='wait 0.1s, ease-in 20px'>
                            <h3>once upon a day</h3>
                            <div class="ws-footer-separator"></div>
                            <p>
                                Every person receives income from their job or somewhere else. But apart from that the extra income continuously receiving from a part time job will be more beneficial in their day to life.  Then they realized that marketing concept is a better and only solution for this problem.
                            </p>
                            <p>
                                Wayonn is one of th marketing business model in Kerala. The selling of the product is done through a group of people or workforce including promoters, distributors etc. and the earnings of the participants are derived through commission. The great vision of Wayonn is creating a part time job and extra income for all who join in the Wayonn.  It is one of the easiest and simplest way for selling the product.  
                            </p>
                        </div>
                    </div>
                    <!-- certificates -->
                    <div class="padding-top-x70"></div>
                    <!-- <h3>Company Certification</h3>
                    <div class="ws-footer-separator"></div>
                    <div class="row text-center">
                        <div class="ws-about-team">
                            <div class="col-sm-3 ws-about-team-item" data-sr='wait 0.1s, ease-in 20px'>
                                <img src="{{asset('img/certificate.jpg')}}" alt="Wayonn Marketing Certificates" class="img-responsive">
                                <div class="caption">
                                    <h5>Certificate name</h5>
                                </div>
                            </div>
                            <div class="col-sm-3 ws-about-team-item" data-sr='wait 0.1s, ease-in 20px'>
                                <img src="{{asset('img/certificate.jpg')}}" alt="Wayonn Marketing Certificates" class="img-responsive">
                                <div class="caption">
                                    <h5>Certificate name</h5>
                                </div>
                            </div>
                            <div class="col-sm-3 ws-about-team-item" data-sr='wait 0.1s, ease-in 20px'>
                                <img src="{{asset('img/certificate.jpg')}}" alt="Wayonn Marketing Certificates" class="img-responsive">
                                <div class="caption">
                                    <h5>Certificate name</h5>
                                </div>
                            </div>
                            <div class="col-sm-3 ws-about-team-item" data-sr='wait 0.1s, ease-in 20px'>
                                <img src="{{asset('img/certificate.jpg')}}" alt="Wayonn Marketing Certificates" class="img-responsive">
                                <div class="caption">
                                    <h5>Certificate name</h5>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- End Page Content -->

@endsection


