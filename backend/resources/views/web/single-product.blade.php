
@extends('web.layouts.app')

@section('title', 'Wayonn | Single Product')

@section('content')


    <!-- Breadcrumb -->
        <div class="ws-breadcrumb">
            <div class="container">
                <h3>" Purchase your product and join our business network "</h3>
            </div>
        </div>
    <!-- End Breadcrumb -->


    <!-- Product Content -->
    <div class="container ws-page-container">
        <div class="row">
            <!-- Product Image Carousel -->
            <div class="col-lg-5 col-md-5 col-sm-12">
                <div id="ws-products-carousel" class="owl-carousel">
                    @if($product->files->count()>0)
                        @foreach($product->files as $file)
                            <div class="item">
                                <img src="{{asset('images/'.$file->name)}}" class="img-responsive" alt="Wayonn Marketing Products">
                            </div>
                            <div class="item">
                                <img src="{{asset('images/'.$file->name)}}" class="img-responsive" alt="Wayonn MultWayonn Marketing">
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <!-- Product Information -->
            <div class="col-md-offset-1 col-lg-5 col-md-5 col-sm-12">
                <div class="ws-product-content">
                    <div class="shope-header">
                        <div class="backs">
                            <span><i class="fa fa-angle-left"></i>Back to</span>
                            <a href="/products">shope</a>
                        </div>
                        <div class="text-end">
                            <a href="#"><i class="fa fa-angle-left"></i></a>
                            <a href="#"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <header>
                        <!-- Item Category -->
                        <div class="ws-item-category">{{$product->brand}}</div>
                        <!-- Title -->
                        <h3 class="ws-item-title">{{$product->name}}
                            <!-- Price -->
                            <span class="ws-single-item-price pull-right">&#8377;{{$product->price}}</span>
                        </h3>
                        <div class="ws-separator pull-left"></div>
                        
                    </header>

                    <div class="ws-product-details">
                       <p>
                            Width :   58”<br>
                            Length :  Shirt piece – 1.60 mtrs.<br>
                            Product :  Pure Linen cloths
                        </p>
                        <p>Size:
                            <span>Shirt(all Sizes)</span>
                        </p>
                        {!! $product->description !!}
                    </div>
                    <!-- Button -->
                    <a href="/join" class="btn ws-btn-fullwidth">Join Now</a><br><br><br>
                </div>
            </div>
        </div>
    </div>
    <!-- Product Content -->

    <!-- Products Description -->
    <div class="ws-products-description-content text-center">

        <!-- Item -->
        <div class="ws-product-description">
            <h3>Share</h3>
            <div class="ws-product-social-icon">
                <a href="{{$sharelink['twitter']}}" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="{{$sharelink['gplus']}}" target="_blank"><i class="fa fa-google"></i></a>
                <a href="{{$sharelink['facebook']}}" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="{{$sharelink['pinterest']}}" target="_blank"><i class="fa fa-pinterest"></i></a>
                <a href="{{$sharelink['linkedin']}}" target="_blank"><i class="fa fa-linkedin"></i></a>
            </div>
        </div>
    </div>
    <!-- End Products Description -->

    <!-- Related Post -->
    <div class="ws-related-section">
        <div class="container">

            <!-- Title -->
            <div class="ws-related-title">
                <h3>Related Products</h3>
                <div class="ws-separator"></div>
            </div>
             <div id="ws-items-carousel">
                 <!-- Product Listing From Database -->
                 <?php $products=\App\Product::where('active',1)->with('files')->get(); ?>

                 @foreach($products as $prod)
                 <!-- Item -->
                 <div class="ws-works-item" data-sr='wait 0.1s, ease-in 20px'>
                     <a href="/single-product/{{$prod->id}}">
                         <div class="ws-item-offer">
                             <!-- Image -->
                             <figure>
                                 @if($prod->files->count()>0)
                                     <img src="{{asset('images/'.$prod->files[0]->name)}}" alt="Wayonn Multilevel Marketing Products" class="img-responsive">
                                 @endif
                             </figure>
                         </div>
                         <div class="ws-works-caption text-center">
                             <!-- Item Category -->
                             <div class="ws-item-category">{{$prod->brand}}</div>
                             <!-- Title -->
                             <h3 class="ws-item-title">{{$prod->name}}</h3>
                             <div class="ws-item-separator"></div>
                             <!-- Price -->
                             <div class="ws-item-price">&#8377;{{$prod->price}}</div>
                         </div>
                     </a>
                 </div>
                 <!-- Item -->
                 @endforeach
             </div>
        </div>
    </div>
    <!-- End Related Post -->

    <!-- Subscribe Section -->
    <section class="ws-subscribe-section">
        <div class="container">
            <div class="row">
                <!-- Subscribe Content -->
                <div class="ws-subscribe-content text-center clearfix">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h3>Sign up for the newsletter</h3>
                        <div class="ws-separator"></div>
                        <!-- Form -->
                        <form action="{{ route('addmysubscription') }}" class="form-inline" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input name="email" type="email" class="form-control" placeholder="Enter your email" required="">
                            </div>
                            <!-- Button -->
                            <button type="submit" class="btn ws-btn-subscribe">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Subscribe Section -->

@endsection


