
@extends('web.layouts.app')

@section('title', 'Wayonn | Our Business')

@section('content')

     <!-- Page Parallax Header -->
    <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="{{asset('img/business.jpg')}}">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <h1>we describe</h1>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Parallax Header -->

    <!-- Page Content -->
    <div class="container ws-page-container pb0">
        <div class="row">
            <div class="ws-about-content col-sm-12">
                <div class="row vertical-align">
                    <div class="col-sm-8" data-sr='wait 0.1s, ease-in 20px'>
                        <h3>What is Wayonn ?</h3>
                        <div class="ws-footer-separator"></div>
                        <p>
                            Our business is one of the earliest and best marketing strategy ever used in the world of business world. From the early 19th century itself this system has been in practice. It’s actually a marketing plan of making one’s product to reach all over the world with commissions and bonuses.
                        </p>
                        <p>
                            Customer acquisition and awareness of branding with best sales results is the ultimate aim of the business strategy. Wayonn business is thus a network-oriented business that laid a layer of foundation over the member or user line-up. Wayonn does have many forms or various names like Wayonn marketing, Network marketing, Direct selling marketing or you can be a affiliate partner in affiliate marketing.
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('img/mlm.png')}}" alt="Wayonn Marketing Business" class="img-responsive">
                    </div>
                </div>
                <div class="row vertical-align">
                    <div class="col-sm-12" data-sr='wait 0.3s, ease-in 20px'>
                        <p>
                            If you choose to become a distributor with the direct selling company, you'll earn money both through the sales of the Wayonn products and through recruiting other distributors, by receiving a portion of the income these distributors generate.And when those distributors recruit distributors of their own, you'll earn money on the income they generate too.
                        </p>
                        <p>
                            The distributors that you sign up with your Wayonn Marketing plan and the ones they sign up in turn are called your downline. The distributor that originally recruited you and whoever is above him or her in the recruitment chain is called your upline. Often the distributor who recruits you will give you some help getting started, including training.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Content -->
    <!-- Page Content -->
    <div class="container ws-page-container pb0">
        <div class="row">
            <div class="ws-about-content col-sm-12">
                <div class="row vertical-align">
                    <div class="col-sm-8" data-sr='wait 0.1s, ease-in 20px'>
                        <h3>wayonn offer</h3>
                        <div class="ws-footer-separator"></div>
                        <p>
                            Every person joins Wayonn Marketing by giving a subscription amount of 2100 and he/she will be getting products for the amount paid. Thus he or she will be given a task of adding participants.
                        </p>
                        <p>
                            Here the participant directly sells the product to other. They are known as distributor or sponsor. The sponsor is a person who directly recruits another person into marketing. And each distributor should compulsorily add 4 participants to the marketing line.   Thus ability to bring participants and their sales may bring commission to the representative.  The next 4 person bring another four below them. Thus it becomes 16 and next step 64 .This is a chain which continuously flows.   All transactions and processes are done Wayonn App.
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('img/offer.jpg')}}" alt="Wayonn Marketing Business" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Content -->
    <!-- Page Content -->
    <div class="container ws-page-container pb0">
        <div class="row">
            <div class="ws-about-content col-sm-12">
                <div class="row vertical-align">
                    <div class="col-sm-4">
                        <img src="{{asset('img/MLMplan.png')}}" alt="Wayonn Marketing Business" class="img-responsive">
                    </div>
                    <div class="col-sm-8" data-sr='wait 0.2s, ease-in 20px'>
                        <h3>if you choose...</h3>
                        <div class="ws-footer-separator"></div>
                        <p>
                            If you choose to become a distributor with the direct selling company, you'll earn monetary benefits both through the sales of the Wayonn products and through the incentives on recruiting other distributors, this incentive is a portion of the income these distributors down the line generate. And when those distributors recruit distributors of their own, you'll earn incentives on the income they generate too. 
                        </p>
                        <p>
                            The distributors that you sign up with your Wayonn Marketing plan and the ones they sign up in turn are called your down line. The distributor that originally recruited you and whoever is above him or her in the recruitment chain is called your up line. Often the distributor who recruits you will give you some help to get started, including training.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Content -->

    <!-- Page Content -->
    <div class="container ws-page-container pb0">
        <div class="row">
            <div class="ws-about-content col-sm-12">
                <div class="row vertical-align">
                    <div class="col-sm-8" data-sr='wait 0.1s, ease-in 20px'>
                        <h3>we made for you</h3>
                        <div class="ws-footer-separator"></div>
                        <p>
                            Every person joins Wayonn Wayonn by giving a subscription amount of 2100 and he/she will be getting products for the amount paid. Thus he or she will be given a task of adding participants.
                        </p>
                        <p>
                            Here the participant directly sells the product to other. They are known as distributor or sponsor. The sponsor is a person who directly recruits another person into marketing. And each distributor should compulsorily add 4 participants to the marketing line.   Thus ability to bring participants and their sales may bring commission to the representative.  The next 4 person bring another four below them. Thus it becomes 16 and next step 64 .This is a chain which continuously flows.   All transactions and processes are done Wayonn App.
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <img src="{{asset('img/you.png')}}" alt="Wayonn Marketing Business" class="img-responsive">
                    </div>
                </div>
                <div class="row vertical-align">
                    <div class="col-sm-12" data-sr='wait 0.2s, ease-in 20px'>
                        <h3>The coordinators,</h3>
                        <div class="ws-footer-separator"></div>
                        <p>
                            There are coordinators above the distributors.  On joining of each distributor, the coordinator gets a commission of 150 rupees and distributor has 50 rupees. The notifications will be continuously receiving to the coordinators and distributors on how many persons join, how much commission they received, how much amount is transferred to bank account etc.
                        </p>
                        <p>
                            The coordinators will get the designation of director after 45 days. And also they need special qualities including leadership skill, responsibility, convincing ability, proper knowledge and practice, good communication etc. The director thus selected will get extra benefits and commissions from the Wayonn Wayonn marketing.  These coordinators should add 10 people in 45 days.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Content -->
    <!-- Page Content -->
    <div class="container ws-page-container">
        <div class="row">
            <div class="ws-about-content col-sm-12">
                <div class="row vertical-align">
                    <div class="col-sm-6" data-sr='wait 0.2s, ease-in 20px'>
                        <h3>in wayonn</h3>
                        <div class="ws-footer-separator"></div>
                        <p>
                            There are mainly two types of Wayonn marketing. First is binary, where equal number of partners is needed in both left and right side. And if there is any imbalance, participants will not get any cash benefits. Other is sunflower and this type is followed by the Wayonn Marketing. That is adding a minimum 4 participants to the lower level.
                        </p>
                        <p>
                            The transaction of sale in Wayonn is done through an online app Wayonn is doing online sale of 100% Linen cloths pieces. These Linen has a common price in all the district.  The products are of high quality with an affordable price.  Electronic payment can be made for the transactions.
                        </p>
                        <p>
                            The product shipping is done within 7 days.  And if the product received is not as the taste of the consumer, it can be return back and as usual the money will be refunded.  And there will not be any fraudulent activities undertaken in the online transaction. The quality product will be received at the correct place within the correct time.
                        </p>
                        <p>
                            The Wayonn marketing company Wayonn does not have a proper a specific policy created. The common policies of Wayonn marketing are followed by the Wayonn also.
                        </p>
                    </div>
                    <div class="col-sm-6" data-sr='wait 0.1s, ease-in 20px'>
                        <h3>you think ?</h3>
                        <div class="ws-footer-separator"></div>
                        <ul class="list-group">
                          <li class="list-group-item active">which type of product ?</li>
                          <li class="list-group-item">Product –100% Linen cloths </li>
                          <li class="list-group-item">Size-1.60 mtr</li>
                          <li class="list-group-item active">How to order and join WAYONN network ?</li>
                          <li class="list-group-item">Wayonn website </li>
                          <li class="list-group-item">Choose the product</li>
                          <li class="list-group-item">Address filling</li>
                          <li class="list-group-item">Payment (only e payment)</li>
                          <li class="list-group-item">OTP (Link)</li>
                          <li class="list-group-item">Play store</li>
                          <li class="list-group-item">Wayonn app download</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Page Content -->

@endsection


