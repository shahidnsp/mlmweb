
@extends('web.layouts.app')

@section('title', 'Wayonn | Our Business')

@section('content')

    <!-- Page Parallax Header -->
        <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="{{asset('img/product.jpg')}}">
            <div class="ws-overlay">
                <div class="ws-parallax-caption">
                    <div class="ws-parallax-holder">
                        <h1>Our Products</h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Parallax Header -->

        <!-- Page Content -->
        <div class="container ws-page-container">
            <div class="row">
                <div class="ws-shop-page">

                    <!-- Categories Content -->
                    <div class="tab-content">
                        <!-- All -->
                        <div role="tabpanel" class="tab-pane fade in active" id="all">
                            @foreach($products as $product)
                                <!-- Item -->
                                <div class="col-sm-6 col-md-3 ws-works-item">
                                    <a href="/single-product/{{$product->id}}">
                                        <div class="ws-item-offer">
                                            <!-- Image -->
                                            <figure>
                                                @if($product->files->count()>0)
                                                <img src="{{asset('images/'.$product->files[0]->name)}}" alt="Wayonn Marketing Product Image" class="img-responsive">
                                                @endif
                                            </figure>
                                        </div>
                                        <div class="ws-works-caption">
                                            <!-- Item Category -->
                                            <div class="ws-item-category">{{$product->brand}}</div>
                                            <!-- Title -->
                                            <h3 class="ws-item-title">{{$product->name}}</h3>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <!-- End Categories Content -->
                        <!-- Load More -->
                        @if(isset($products->last()->id))
                        <div id="remove-row" class="ws-more-btn-holder col-sm-12">
                            <button id="btn-more" data-id="{{$products->last()->id}}" href="#x" class="btn ws-more-btn"> Load More</button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- warning alert -->
            @if(session()->has('error') and session()->get('error') == true)
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> {{session()->get('message')}}.
                </div>
            @endif
        </div>
        <!-- End Page Content -->
{{--        {{dd(session()->get('payment'))}}--}}


    @if(session()->has('payment.status') and session()->get('payment.status') == false)
    <div id="errorModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header error">
                    <div class="icon-box">
                        <i class="fa fa-exclamation"></i>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body text-center">
                    <h4>Sorry!</h4>
                    <p>Something Went Wrong.Please try again later</p>
                    <button class="btn btn-success" data-dismiss="modal" onclick="javascript:window.location='/wayonnApplication'"><span>Start Exploring</span> <i class="fa fa-long-arrow-right"></i></button>
                </div>
            </div>
        </div>
    </div>
    @endif

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
           $(document).on('click','#btn-more',function(){
               var id = $(this).data('id');
               $("#btn-more").html("Loading....");
               $.ajax({
                   url : '{{ url("web/loadProducts") }}',
                   method : "POST",
                   data : {id:id, _token:"{{csrf_token()}}"},
                   dataType : "text",
                   success : function (data)
                   {
                      if(data != '')
                      {
                          $('#remove-row').remove();
                          $('#all').append(data);
                      }
                      else
                      {
                          $('#btn-more').html("No More Product");
                      }
                   }
               });
           });
        });

    </script>



    @if(session()->has('payment.status') and session()->get('payment.status') == false)
        <script>
            $(document).ready(function(){
                $('#errorModal').modal({backdrop: 'static', keyboard: false,show:true});
            });
        </script>
    @endif
@stop


