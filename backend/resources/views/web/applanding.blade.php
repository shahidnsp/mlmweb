@extends('web.layouts.app')

@section('title', 'Wayonn | Mobile Application')

@section('content')
    
    <section id="ws-instagram-section">
        <div class="container-fluid">
            <div class="row vertical-align">
                <div class="col-sm-4 col-md-pull-1 ws-instagram-item" data-sr='wait 0.1s, ease-in 20px'>
                    <img src="{{asset('img/app-one.jpg')}}" alt="Wayonn Marketing Mobile Application Image" class="img-responsive">
                </div>    
                <div class="col-sm-6">       
                    <div class="ws-instagram-header">
                        <h3>Download Our Mobile App</h3>                              
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                        </p>
                    </div>
                </div>                                  
            </div>
            <div class="row vertical-align">   
                <div class="col-sm-5 col-sm-offset-1">       
                    <div class="ws-instagram-header">
                        <h3>Download Our Mobile App</h3>                              
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                        </p>
                    </div>
                </div> 
                <div class="col-sm-4 col-sm-offset-2 ws-instagram-item" data-sr='wait 0.1s, ease-in 20px'>
                    <img src="{{asset('img/app-right.png')}}" alt="Wayonn Marketing Mobile Application Image" class="img-responsive">
                </div>                                  
            </div>
            <div class="row vertical-align">
                <div class="col-sm-4 col-md-pull-1 ws-instagram-item" data-sr='wait 0.1s, ease-in 20px'>
                    <img src="{{asset('img/app-left.png')}}" alt="Wayonn Marketing Mobile Application Image" class="img-responsive">
                </div>   
                <div class="col-sm-6">       
                    <div class="ws-instagram-header">
                        <h3>Download Our Mobile App</h3>                              
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                        </p>
                    </div>
                </div>                                  
            </div>
        </div>
    </section>

    <section id="ws-instagram-section">
        <div class="container">
            <div class="row vertical-align">

                <div class="col-sm-6">       
                    <div class="ws-instagram-header">
                        <h3>Download Our Mobile App</h3>                              
                        <p>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.
                        </p>
                        <div class="store">
                            <a href="https://play.google.com/store/apps/details?id=com.wayon.nid" target="_blank"><img src="{{asset('img/playstore.png')}}" alt="Wayonn Marketing Mobile Application Image" class="img-responsive"></a>
                            <a href="#"><img src="{{asset('img/appstore.png')}}" alt="Wayonn Marketing Mobile Application Image" class="img-responsive"></a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5 col-sm-offset-2 ws-instagram-item" data-sr='wait 0.1s, ease-in 20px'>
                    <img src="{{asset('img/app-end.png')}}" alt="Wayonn Marketing Mobile Application Image" class="img-responsive">
                </div>    
                                  
            </div>
        </div>
    </section>  

@endsection