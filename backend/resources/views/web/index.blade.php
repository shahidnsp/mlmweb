
@extends('web.layouts.app')

@section('title', 'Wayonn | Home')

@section('content')

    <!-- Slider FullWidth -->      
    <div id="ws-fullwidth-slider" class="rev_slider">
        <ul>
            <?php $sliders=\App\Slider::orderBy('order')->get(); ?>
            @foreach($sliders as $slider)
            <!-- Slide -->
            <li data-transition="fade">  
                <!-- Background Image -->
                <img src="{{asset('images/'.$slider->name)}}"  alt="Wayonn Marketing Slider"  width="1920" height="1280">
                <!-- Layer -->
                <div class="tp-caption ws-hero-title hidden-xs"  
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                    data-y="['middle','middle','middle','middle']" data-voffset="['-72','-72','-72','-48']"                     
                    data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"             
                    data-mask_in="x:0px;y:0px;" 
                    data-mask_out="x:0;y:0;" 
                    data-start="1000" 
                    data-responsive_offset="on" 
                    style="z-index: 6;"><h1>100% Linen cloths</h1>
                </div>
                <!-- Layer -->
                <div class="tp-caption ws-hero-description hidden-xs"                            
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                    data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','1']"
                    data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"          
                    data-mask_in="x:0px;y:0px;" 
                    data-mask_out="x:0;y:0;" 
                    data-start="1000"            
                    data-responsive_offset="on" 
                    style="z-index: 7;"><h4>Sell yourself to your friend ,make them learn to sell to themselves and earn more.</h4>                                
                </div>
                <!-- Button -->
                <div class="tp-caption hidden-xs"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                    data-y="['middle','middle','middle','middle']" data-voffset="['92','92','92','76']"                                    
                    data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeInOut;"             
                    data-start="1000"                      
                    data-responsive_offset="on" 
                    data-responsive="off"
                    style="z-index: 8;"><a class="btn ws-big-btn" href="join">Join Now</a>
                </div>
            </li>
            @endforeach
        </ul>               
    </div>                
    <!-- End Slider FullWidth -->  

    <!-- About Section Start -->
    <section class="ws-about-section">
        <div class="container">
            <div class="row">
                <!-- Description -->
                <div class="ws-about-content clearfix">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h3>Why we are No:1</h3>
                        <div class="ws-separator"></div>
                        <p>
                            Wayonn is one of the multi level marketing business model in Kerala. The selling of the product 
                            is done through a group of people or workforce including promoters, distributors etc,
                            and the earnings of the participants are derived through commission.
                        </p>
                    </div>
                </div>
                <!-- Featured Collections -->
                <div class="ws-contact-page">
                    <div class="col-sm-12">
                        <div class="ws-contact-offices text-center">
                            <div class="row">      
                                <!-- City -->              
                                <div class="col-sm-6 col-sm-4 ws-contact-office-item" data-sr='wait 0.1s, ease-in 20px'>
                                    <div class="thumbnail">
                                        <img src="{{asset('img/vision.jpg')}}" alt="Wayonn Marketing Slider Image">
                                        <div class="ws-overlay">
                                            <div class="caption">
                                                <strong>Our Vision</strong>
                                                <div class="ws-contact-separator"></div>     
                                                <p>
                                                    To make a reputed organization that founded on a culture “You work more and earn more!”
                                                    and to unleash the potential to have a better living and an extra income to all.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>   

                                <!-- City -->
                                <div class="col-sm-6 col-sm-4 ws-contact-office-item" data-sr='wait 0.3s, ease-in 20px'>
                                    <div class="thumbnail">
                                        <img src="{{asset('img/mission.png')}}" alt="Wayonn Marketing Slider Image">
                                        <div class="ws-overlay">
                                            <div class="caption">
                                                <strong>Our Mission</strong>
                                                <div class="ws-contact-separator"></div>      
                                                <p>
                                                    To be a trusted an reputed marketing company in India that believes in transparency, integrity and commitment
                                                </p>                                     
                                            </div>
                                        </div>  
                                    </div>
                                </div>

                                <!-- City -->
                                <div class="col-sm-6 col-sm-4 ws-contact-office-item" data-sr='wait 0.5s, ease-in 20px'>
                                    <div class="thumbnail">
                                        <img src="{{asset('img/values.jpg')}}" alt="Wayonn Marketing Slider Image">
                                        <div class="ws-overlay">
                                            <div class="caption">
                                                <strong>Our Values</strong>
                                                <div class="ws-contact-separator"></div>  
                                                <p>
                                                    Every person receives income from their job. But apart from that the 
                                                    extra income continuously receiving from a part time job will be more beneficial in their day to life.
                                                </p>                              
                                            </div>
                                        </div>  
                                    </div>
                                </div> 
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Section End -->

    <!-- New Arrivals Section -->
    <section class="ws-arrivals-section">
        <div class="ws-works-title clearfix">
            <div class="col-sm-12">
                <h3>New Arrivals</h3>
                <div class="ws-separator"></div>
            </div>
        </div>

        <div id="ws-items-carousel">
            <!-- Product Listing From Database -->
            <?php $products=\App\Product::where('active',1)->with('files')->get(); ?>

            @foreach($products as $product)
            <!-- Item -->
            <div class="ws-works-item" data-sr='wait 0.1s, ease-in 20px'>
                <a href="/single-product/{{$product->id}}">
                    <div class="ws-item-offer">
                        <!-- Image -->
                        <figure>
                            @if($product->files->count()>0)
                                <img src="{{asset('images/'.$product->files[0]->name)}}" alt="Wayonn Marketing Slider Image" class="img-responsive">
                            @endif
                        </figure>
                    </div>
                    <div class="ws-works-caption text-center">
                        <!-- Item Category -->
                        <div class="ws-item-category">{{$product->brand}}</div>
                        <!-- Title -->
                        <h3 class="ws-item-title">{{$product->name}}</h3>
                    </div>
                </a>
            </div>
            <!-- Item -->
            @endforeach
        </div>
    </section>
    <!-- End New Arrivals Section -->

    <!-- Call to Action Section -->
    <section class="ws-call-section">
        <div class="ws-overlay">
            <div class="ws-parallax-caption">
                <div class="ws-parallax-holder">
                    <div class="col-sm-6 col-sm-offset-3">
                        <h2>" refer earn and "</h2>
                        <div class="ws-separator"></div>
                        <p>
                            Happiness does not come from doing easy work but from the afterglow of satisfaction that comes after the achievement of a difficult task that demanded our best.
                        </p>
                        <div class="ws-call-btn">
                            <a href="ourbusiness" >View Our Business Plans</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Call to Action Section -->

    <!-- Subscribe Section -->
    <section class="ws-testimonial-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ws-works-title clearfix">
                        <div class="col-sm-12">
                            <h3>People say's</h3>
                            <div class="ws-separator"></div>
                        </div>
                    </div>
                    <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                        <!-- Carousel Slides / Quotes -->
                        <div class="carousel-inner text-center">
                            <!-- Quote 1 -->
                            <div class="item active">
                                <blockquote>
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. !</p>
                                            <small>Someone famous</small>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                            <!-- Quote 2 -->
                            <div class="item">
                                <blockquote>
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                            <small>Someone famous</small>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                            <!-- Quote 3 -->
                            <div class="item">
                                <blockquote>
                                    <div class="row">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. .</p>
                                            <small>Someone famous</small>
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                        </div>
                        <!-- Bottom Carousel Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="https://s3.amazonaws.com/uifaces/faces/twitter/mantia/128.jpg" alt="">
                            </li>
                            <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg" alt="">
                            </li>
                            <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="">
                            </li>
                        </ol>

                        <!-- Carousel Buttons Next/Prev -->
                        <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                        <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Subscribe Section -->

    <!-- Subscribe Section -->
    <section class="ws-subscribe-section">
        <div class="container">
            <div class="row">
                <!-- Subscribe Content -->
                <div class="ws-subscribe-content text-center clearfix">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h3>Sign up for the newsletter</h3>
                        <div class="ws-separator"></div>
                        <!-- Form -->
                        <form action="{{ route('addmysubscription') }}" class="form-inline" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input name="email" type="email" class="form-control" placeholder="Enter your email" required="">
                            </div>
                            <!-- Button -->
                            <button type="submit" class="btn ws-btn-subscribe">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Subscribe Section -->  

    <section id="ws-instagram-section">
        <div class="container">
            <div class="row vertical-align">

                <div class="col-sm-6">       
                    <div class="ws-instagram-header">
                        <h3>Download Our Mobile App</h3>                              
                        <p>
                            Sell yourself to your friend ,make them learn to sell to themselves and earn more.
                        </p>
                        <div class="store">
                            <a href="https://play.google.com/store/apps/details?id=com.wayon.nid" target="_blank"><img src="{{asset('img/playstore.png')}}" alt="Wayonn Marketing Slider Image" class="img-responsive"></a>
                            <a href="#"><img src="{{asset('img/appstore.png')}}" alt="Wayonn Marketing Slider Image" class="img-responsive"></a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5 col-sm-offset-2 ws-instagram-item" data-sr='wait 0.1s, ease-in 20px'>
                    <img src="{{asset('img/app.png')}}" alt="Wayonn Marketing Slider Image" class="img-responsive">
                </div>    
                                  
            </div>
        </div>
    </section>

@endsection


