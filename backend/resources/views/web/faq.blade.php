
@extends('web.layouts.app')

@section('title', 'Wayonn | FAQ')

@section('content')

    <!-- Page Parallax Header -->
        <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="{{asset('img/question.jpg')}}">
            <div class="ws-overlay">
                <div class="ws-parallax-caption">
                    <div class="ws-parallax-holder">
                        <h1>Got Questions?</h1>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Parallax Header -->

        <!-- About Section Start -->
        <section class="ws-about-section">
            <div class="container">
                <div class="row">
                    <!-- Description -->
                    <div class="ws-about-content clearfix">
                        <div class="col-sm-8 col-sm-offset-2">
                            <h3>best team with you</h3>
                            <div class="ws-separator"></div>
                            <p>
                                The doubts and questions of each and every distributor will be clarified by the coordinator directly. There is a transparency between the coordinators and distributors.
                                Wayonn is working as a team which creates productive working hours among all distributors and also an investment for the marketing.  There is a common ethics followed by the Wayonn among which the right and wrong is distinguished. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Section End -->   

        <!-- Instagram Content -->
        <section id="ws-instagram-section">
            <div class="container">
                <div class="row vertical-align">

                    <!-- Instagram Information -->
                    <div class="col-sm-3">       
                        <div class="ws-instagram-header">
                            <h3>connect us</h3>     
                            <br>                   
                            <a href="https://www.instagram.com/" target="_blank" class="ws-instagram-link"> @artday</a>                         
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
                        </div>
                    </div>

                    <!-- Instagram Item -->
                    <div class="col-md-offset-1 col-sm-4 ws-instagram-item" data-sr='wait 0.1s, ease-in 20px'>
                        <div class="ws-contact-info text-center">
                            <i class="fa fa-map-marker"></i>
                            <p>WAYONN MARKETING LLP  KIZHAKKETHALA,</p>
                            <p>ROOM No. 24/139-T, MATTIL MALL,</p>
                            <p>VENGARA ROAD, DOWNHILL POST,</p>
                            <p>Malappuram, Kerala, 676519, India.</p>
                            
                        </div>
                    </div>    

                    <!-- Instagram Item -->
                    <div class="col-md-offset-1 col-sm-3 ws-instagram-item" data-sr='wait 0.3s, ease-in 20px'>
                        <div class="ws-contact-info text-center">
                            <i class="fa fa-mobile" style="padding: 10px 24px"></i>
                            <p>+91 (756) 093-1458</p>
                            <p>+91 (756) 093-1458</p>
                            <p>+91 (756) 093-1458</p>
                            <p>+91 (756) 093-1458</p>
                        </div>
                    </div>                                  
                </div>
            </div>
        </section>
        <!-- End Instagram Content -->

        <!-- Subscribe Section -->
        <section class="ws-subscribe-section">
            <div class="container">
                <div class="row">
                    <!-- Subscribe Content -->
                    <div class="ws-subscribe-content text-center clearfix">
                        <div class="col-sm-8 col-sm-offset-2">
                            <h3>Sign up for the newsletter</h3>
                            <div class="ws-separator"></div>
                            <!-- Form -->
                            <form action="{{ route('addmysubscription') }}" class="form-inline" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input name="email" type="email" class="form-control" placeholder="Enter your email">
                                </div>
                                <!-- Button -->
                                <button type="submit" class="btn ws-btn-subscribe">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Subscribe Section -->

@endsection


