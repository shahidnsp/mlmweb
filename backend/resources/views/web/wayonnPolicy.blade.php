
@extends('web.layouts.app')

@section('title', 'Wayonn | Shipping and Return Policy')

@section('content')


        <!-- About Section Start -->
        <section class="ws-about-section">
            <div class="container">
                <div class="row">
                    <!-- Description -->
                    <div class="ws-about-content clearfix">
                        <div class="col-sm-8 col-sm-offset-2">
                            <h3>Shipping and Return Policy</h3>
                            <div class="ws-separator"></div>
                            <p class="text-justify">
                                The product shipping is done within 7 days.  And if the product received is not as the taste of the consumer, it can be return back and as usual the money will be refunded.  And there will not be any fraudulent activities undertaken in the online transaction. The quality product will be received at the correct place within the correct time.
                            </p>
                            <p class="text-justify">
                                The Wayonn marketing company Wayonn does not have a proper a specific policy created. The common policies of Wayonn marketing are followed by the Wayonn also.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Section End -->   

        <!-- Subscribe Section -->
        <section class="ws-subscribe-section">
            <div class="container">
                <div class="row">
                    <!-- Subscribe Content -->
                    <div class="ws-subscribe-content text-center clearfix">
                        <div class="col-sm-8 col-sm-offset-2">
                            <h3>Sign up for the newsletter</h3>
                            <div class="ws-separator"></div>
                            <!-- Form -->
                            <form action="{{ route('addmysubscription') }}" class="form-inline" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input name="email" type="email" class="form-control" placeholder="Enter your email">
                                </div>
                                <!-- Button -->
                                <button type="submit" class="btn ws-btn-subscribe">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Subscribe Section -->

@endsection


