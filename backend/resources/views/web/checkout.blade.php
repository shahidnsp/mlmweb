
@extends('web.layouts.app')

@section('title', 'Wayonn | Checkout')

@section('content')

        <!-- Page Parallax Header -->
        <!-- <div class="ws-parallax-header parallax-window" data-parallax="scroll" data-image-src="{{asset('img/backgrounds/shop-header-bg.jpg')}}">
            <div class="ws-overlay">
                <div class="ws-parallax-caption">
                    <div class="ws-parallax-holder">
                        <h1>Checkout</h1>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- End Page Parallax Header -->

        <!-- Page Content -->
        <div class="container ws-page-container" id="f-d-form">
            <div class="row">
                <div class="col-sm-6">
                    <!-- Reference Code -->
                    <div class="ws-checkout-coupon clearfix">
                        <div class="col-sm-12">
                            <div class="coupon-info">
                                <p>Have a Reference? <a data-toggle="collapse" href="#coupon-collapse"> Click here to enter your code</a></p>
                            </div>
                            <div class="collapse in" id="coupon-collapse">
                                <!-- Reference -->
                                <div class="ws-checkout-coupon-code">
                                    <form class="form-inline" method="POST" id="refCodeForm" action="{{route('refer.verify')}}">
                                        <p><input type="text" placeholder="Reference code" name="reference" value="{{(session()->has('join.user.name') ? session()->get('join.user.name') : "")}}" {{(session()->has('join.user.name') ? 'disabled' : '')}} required=""></p>
                                        <span id="error"></span>
                                        <br>
                                        <!-- Button -->
                                        <button class="btn ws-btn-fullwidth {{(session()->has('join.user.name') ? 'hidden' : '')}}">Apply Now</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="padding-top-x50"></div>
                        <div class="col-sm-12">
                            <div class="yrefer">
                                <p>How can Refer you ?</p>
                                <span>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </span>
                                {{--<br /><a href="#successModal" class="trigger-btn" data-toggle="modal">Click to Open Success Modal</a>--}}
                                {{--<br /><a href="#errorModal" class="trigger-btn" data-toggle="modal">Click to Open Error Modal</a>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <!-- Login Form -->                
                    <form class="ws-login-form" name="phoneForm" id="phoneForm" method="POST" action="{{route('otp.send')}}">
                        <div class="bar-head"><p>Verify your contact number</p></div>
                        <!-- Phone number -->
                        <div class="form-group">
                            <label class="control-label">Enter your Phone Number<span>*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control bfh-phone" data-format="+91 (ddd) ddd-dddd" minlength="10" name="phone">
                                <span id="error"></span>
                                <button class="btn btn-phone has-spinner" type="submit">Send OTP</button>
                            </div>
                        </div>
                    </form>
                    <form class="ws-login-form hidden" name="phoneForm" id="otpForm" method="POST" action="{{route('otp.verify')}}">
                        <!-- OTP -->
                        <div class="form-group">
                            <label class="control-label">Enter your OTP <span>*</span></label>
                            <input type="text" class="form-control" name="otp" maxlength="5" required="">
                            <span id="otpError"></span>
                        </div>
                        <!-- Checkbox -->
                        <div class="pull-left">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="terms" required="">I accept the <a href="/faq" target="_blank" >Terms of Service</a>
                                </label>
                            </div>
                        </div>
                        <div class="pull-right hidden" id="anc-resend">
                            <div class="ws-forgot-pass">
                                <a href="#" id="resendOtp">Resend OTP</a> or
                                <a class="sendan" href="#" id="otpToOther">Send OTP to another number</a>
                            </div> 
                        </div>
                        <div class="clearfix"></div>
                        <!-- Button -->
                        <button class="btn ws-small-btn flot-right has-spinner" type="submit">Verify Now</button>
                        <div class="padding-top-x50"></div>
                    </form>
                    <!-- End Login Form -->
                    {{--<!-- success alert -->--}}
                    @if(session()->has('status') and session()->get('status') == true)
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success!</strong> {{session()->get('message')}}
                        </div>
                    @endif
                    <!-- warning alert -->
                    @if(session()->has('status') and session()->get('status') == false)
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Error!</strong> {{session()->get('message')}}.
                        </div>
                    @endif
                </div>

            </div>
        </div>    
        <!-- End Page Content -->

        <form method="post" action="{{ route('proceedToPayment') }}" class="hidden" id="checkout-form">
            {{ csrf_field() }}
        <!-- Page Content -->
        <div class="container ws-page-container">
            <div class="row">
                <div class="ws-checkout-content clearfix">
                    <!-- Cart Content -->
                    <div class="col-sm-7">
                        <!-- Billing Details -->
                        <div class="ws-checkout-billing">
                            <h3>Buyer Details</h3>
                            <!-- Form Inputs -->
                            <section id="form-card" class="form-inline">
                                <!-- Name -->
                                <div class="ws-checkout-first-row">
                                    <div class="col-no-p ws-checkout-input col-sm-6 has-error">
                                        <label>First Name <span> * </span></label><br>
                                        <input type="text" name="name" value="" placeholder="First Name" required="">
                                        <span id="nameError"></span>
                                    </div>
                                    <div class="col-no-p ws-checkout-input col-sm-6">
                                        <label>Last Name</label><br>
                                        <input type="text" name="lastname" value="" placeholder="Last Name">
                                        <span id="lastnameError"></span>
                                    </div>
                                </div>
                                <!-- Email -->
                                <div class="ws-checkout-first-row">
                                    <div class="col-no-p ws-checkout-input col-sm-6">
                                        <label>Phone <span> * </span></label><br>
                                        <input type="tel" name="phone" placeholder="Phone Number" required="" value="{{(session()->has('user.phone') ? session()->get('user.phone') : '')}}" {{(session()->has('user.phone') ? 'disabled' : '')}}>
                                        @if(session()->has('user.phone'))
                                            <input type="hidden" name="phone" value="{{session()->has('user.phone')}}"/>
                                       @endif
                                    </div>
                                    <div class="col-no-p ws-checkout-input col-sm-6">
                                       <label>Email Address <span> * </span></label><br>
                                       <input type="email" name="email" value="" placeholder="yourmail@example.com" required="">
                                       <span id="emailError"></span>
                                   </div>
                                </div>
                                <!-- Adress -->
                                <div class="col-no-p ws-checkout-input col-sm-12">
                                    <label>Shipping Address <span> * </span></label><br>
                                    <textarea name="address" placeholder="Shipping Address" required=""></textarea>
                                    <span id="addressError"></span>
                                </div>

                                <!-- Post Office -->
                                <div class="ws-checkout-first-row">
                                    <div class="col-no-p ws-checkout-input col-sm-6">
                                        <label>Post Office <span> * </span></label><br>
                                        <input type="text" name="postoffice" value="" placeholder="Post Office" required="">
                                        <span id="postofficeError"></span>
                                    </div>
                                    <div class="col-no-p ws-checkout-input col-sm-6">
                                        <label>District <span> * </span></label><br>
                                        <input type="text" name="district" value="" placeholder="District" required="">
                                        <span id="districtError"></span>
                                    </div>
                                </div>

                                <!-- State -->
                                <div class="ws-checkout-first-row">
                                    <div class="col-no-p ws-checkout-input col-sm-6">
                                        <label>State<span> * </span></label><br>
                                        <input type="text" name="state" value="" placeholder="State" required="">
                                        <span id="stateError"></span>
                                    </div>

                                    <div class="col-no-p ws-checkout-input col-sm-6">
                                        <label>Postcode / ZIP <span> * </span></label><br>
                                        <input type="text" name="pincode" value="" placeholder="Postcode / ZIP" required="">
                                        <span id="pincodeError"></span>
                                    </div>
                                </div>
                            </section>

                        </div>
                    </div>

                    <?php
                        $myproduct_id=\Session::get('myproduct_id');
                        $product=\App\Product::find($myproduct_id);
                    ?>
                    <input type="hidden" name="products_id" value="{{$product->id}}"/>
                    <!-- Cart Total -->
                    <div class="col-sm-5">
                        <div class="ws-checkout-order">
                            <h2>Your Oder</h2>

                            <!-- Order Table -->
                            <table>

                                <!-- Title -->
                                <thead>
                                    <tr>
                                        <th class="ws-order-product">Product</th>
                                        <th class="ws-order-total">Total</th>
                                    </tr>
                                </thead>

                                <!-- Products -->
                                <tbody>
                                    <tr>
                                        <th>{{$product->brand}} {{$product->name}}</th>
                                        <td><span> &#8377; {{$product->price}}</span></td>
                                    </tr>
                                </tbody>

                                <!-- Shipping -->
                                <tfoot class="ws-checkout-shipping">
                                    <tr>
                                        <th>Shipping</th>
                                        <td>
                                            <div class="radio">
                                                <label>International Courier: <span>Free</span></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="ws-shipping-total">
                                        <th>Total</th>
                                        <td><span>&#8377; {{$product->price}}</span></td>
                                    </tr>
                                </tfoot>
                            </table>

                            <!-- Payment Metod -->
                            <div class="ws-shipping-payment">
                                <div class="radio">
                                    <label><input type="radio" name="optradio" checked >Make Payment</label>
                                    <img src="{{asset('img/paypal.png')}}" class="img-responsive" alt="PayPal Acceptance Mark">
                                </div>
                                {{--<p>Pay via PayPal, you can pay with your credit card if you don’t have a PayPal account.</p>--}}
                            </div>
                            <button type="submit" class="btn ws-btn-fullwidth">Proceed to Payment</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- End Page Content -->
        </form>

        <div id="errorModal" class="modal fade">
            <div class="modal-dialog modal-confirm">
                <div class="modal-content">
                    <div class="modal-header error">
                        <div class="icon-box">
                            <i class="fa fa-exclamation"></i>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body text-center">
                        <h4>Sorry!</h4> 
                        <p>Your account has been created successfully.</p>
                        <button class="btn btn-success" data-dismiss="modal" onclick="javascript:window.location='/wayonnApplication'"><span>Start Exploring</span> <i class="fa fa-long-arrow-right"></i></button>
                    </div>
                </div>
            </div>
        </div>


@endsection
@section('scripts')
<script>
$(document).ready(function(){
    $('#checkout-form button').click(function(e){
       e.preventDefault();

       $.preloader.start({
           modal: true,
           src: 'sprites1.png'
       });

        var form=$('#checkout-form ');
       var url = $(form).attr('action')+'/validate';
       var data = $(form).serialize();

       $('#emailError',form).html('');
       $('#pincodeError',form).html('');
       $('#postofficeError',form).html('');
       $('#districtError',form).html('');
       $('#stateError',form).html('');
       $('#addressError',form).html('');

       $.post(url, data)
           .done(function (response) {
                $(form).submit();
           })
           .fail(function (response) {
                if(response.responseJSON.name != undefined){
                    $('#nameError',form).html(response.responseJSON.name[0]);
                }
                if(response.responseJSON.lastname != undefined){
                    $('#lastnameError',form).html(response.responseJSON.lastname[0]);
                }
                if(response.responseJSON.email != undefined){
                    $('#emailError',form).html(response.responseJSON.email[0]);
                }
                if(response.responseJSON.pincode != undefined){
                    $('#pincodeError',form).html(response.responseJSON.pincode[0]);
                }
                if(response.responseJSON.postoffice != undefined){
                    $('#postofficeError',form).html(response.responseJSON.postoffice[0]);
                }
                if(response.responseJSON.district != undefined){
                    $('#districtError',form).html(response.responseJSON.district[0]);
                }
                if(response.responseJSON.state != undefined){
                    $('#stateError',form).html(response.responseJSON.state[0]);
                }
                if(response.responseJSON.address != undefined){
                    $('#addressError',form).html(response.responseJSON.address[0]);
                }
           })
           .always(function () {
               $.preloader.stop();
           });

    });
});
</script>
@endsection



