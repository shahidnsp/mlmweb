<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Web Product</h2>
    </div>
    <div class="col-lg-2">
        <br/>
        <button ng-hide="productedit" ng-click="newProduct()" type="button" class="pull-right btn btn-w-m btn-success">Add New</button>
    </div>

</div>

<div ng-show="productedit" class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                New Product
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" ng-submit="addProduct();">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Name</label>
                        <div class="col-sm-11" ng-class="{'has-error' : validationErrors.name}">
                            <input ng-model="newproduct.name" type="text" placeholder="Name" class="form-control">
                            <span class="help-block" ng-if="validationErrors.name"><small>{{validationErrors.name[0]}}</small></span>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Brand</label>
                        <div class="col-sm-11" ng-class="{'has-error' : validationErrors.brand}">
                            <input ng-model="newproduct.brand" type="text" placeholder="Brand" class="form-control" >
                            <span class="help-block" ng-if="validationErrors.brand"><small>{{validationErrors.brand[0]}}</small></span>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Product Code</label>
                        <div class="col-sm-11" ng-class="{'has-error' : validationErrors.pdctcode}">
                            <input ng-model="newproduct.pdctcode" type="text" placeholder="Product Code" class="form-control" ng-disabled="newproduct.id">
                            <span class="help-block" ng-if="validationErrors.pdctcode"><small>{{validationErrors.pdctcode[0]}}</small></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Description</label>
                        <div class="col-sm-11" ng-class="{'has-error' : validationErrors.description}">
                            <!--<textarea ng-model="newproduct.description" type="text" placeholder="Description" class="form-control" required=""></textarea>-->
<!--                            <div ckeditor="options" ng-model="newproduct.description"></div>-->
<!--                            <span class="help-block" ng-if="validationErrors.description"><small>{{validationErrors.description[0]}}</small></span>-->
                            <textarea class="form-control" ng-model="newproduct.description"> </textarea>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Photos</label>
                        <div class="col-sm-11">
                            <input class="form-control" accept="image/*" ng-file-model="newproduct.photos" type="file" multiple/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Price</label>
                        <div class="col-sm-11" ng-class="{'has-error' : validationErrors.price}">
                            <input ng-model="newproduct.price" type="text" placeholder="Price" class="form-control" >
                            <span class="help-block" ng-if="validationErrors.price"><small>{{validationErrors.price[0]}}</small></span>
                        </div>
                     <!--   <label class="col-sm-2 control-label">Offer Price</label>
                        <div class="col-sm-5">
                            <input ng-model="newproduct.offerprice" type="text" placeholder="Offer Price" class="form-control">
                        </div>-->
                    </div>
                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-w-m btn-primary">Save</button>
                        <button ng-click="cancelProduct();" class="btn btn-w-m btn-info">Cancel</button>
                    </div>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-2">
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        </div>
                        <div class="col-md-5">
                            <button class="btn btn-info btn-sm" ng-click="exportToPdf()">Export To Pdf</button>
                        </div>
                        <div class="col-md-5 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Photo</th>
                                    <th>Description</th>
                                    <th>Product Code</th>
                                    <th>Price</th>
                                    <th>Offer Price</th>
                                    <th>Active</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="product in listCount  = (products | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{product.name}}</td>
                                    <td>
                                        <div class="row">
                                            <img ng-repeat="photo in product.files" src="images/{{photo.name}}" style="width: 80px;height: 80px;padding-right: 5px;padding-bottom: 5px;" alt=""/>
                                        </div>
                                    </td>
                                    <td>{{product.description}}</td>
                                    <td>{{product.pdctcode}}</td>
                                    <td>{{product.price}}</td>
                                    <td>{{product.offerprice}}</td>
                                    <td>
                                        <button ng-if="product.active==1" type="button" class="btn btn-success" ng-click="changeProductStatus(product);">
                                            Active
                                        </button>
                                        <button ng-if="product.active==0" type="button" class="btn btn-info" ng-click="changeProductStatus(product);">
                                            InActive
                                        </button>
                                    </td>
                                    <td>
                                        <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-primary" ng-click="editProduct(product);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
<!--                                            <button type="button" class="btn btn-danger" ng-click="deleteProduct(product);">-->
<!--                                                <i class="fa fa-trash-o"></i>-->
<!--                                            </button>-->
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix" ng-show="products.length > numPerPage">
                                <pagination
                                    ng-model="currentPage"
                                    total-items="listCount.length"
                                    max-size="maxSize"
                                    items-per-page="numPerPage"
                                    boundary-links="true"
                                    class="pagination-sm pull-right"
                                    previous-text="&lsaquo;"
                                    next-text="&rsaquo;"
                                    first-text="&laquo;"
                                    last-text="&raquo;"
                                    ></pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
