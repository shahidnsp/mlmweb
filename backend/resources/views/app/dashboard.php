<!--<div class="row wrapper border-bottom white-bg page-heading">-->
<!--    <div class="col-lg-10">-->
<!--        <h2>Dashboard</h2>-->
<!--        <!--<ol class="breadcrumb">-->
<!--            <li>-->
<!--                <a href="index.html">Home</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a>Forms</a>-->
<!--            </li>-->
<!--            <li class="active">-->
<!--                <strong>PDF viewer</strong>-->
<!--            </li>-->
<!--        </ol>-->
<!--    </div>-->
<!--    <div class="col-lg-2">-->
<!---->
<!--    </div>-->
</div>
<div class="row">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Income</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{totalIncome}}</h1>
                <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                <small>Total income</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <a href="#/appuser"><h5>Users</h5></a>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{users}}</h1>
                <div class="stat-percent font-bold text-info">{{members}} <i class="fa fa-level-up"></i></div>
                <small>Total Members</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-primary pull-right">Today</span>
                <h5>Withdrawal</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">106,120</h1>
                <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                <small>New visits</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-danger pull-right">Today</span>
                <a href="#/orderlist"><h5>Orders</h5></a>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">{{todayOrder}}</h1>
                <div class="stat-percent font-bold text-danger">{{orders}}</div>
                <small>Total Order</small>
            </div>
        </div>
    </div>
</div>

<!--<div class="row">-->
<!--    <div class="col-lg-9">-->
<!--        <div class="flot-chart">-->
<!--            <div class="flot-chart-content" id="flot-dashboard-chart" style="padding: 0px; position: relative;">-->
<!--                <div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);">-->
<!--                    <canvas id="bar" class="chart chart-bar"-->
<!--                            chart-data="data" chart-labels="labels" chart-options="options"> chart-series="series"-->
<!--                    </canvas-->
<!--                </div>-->
<!--                <div class="legend">-->
<!--                    <div style="position: absolute; width: 111px; height: 30.9667px; top: 13px; left: 34px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div>-->
<!--                    <table style="position:absolute;top:13px;left:34px;;font-size:smaller;color:#545454">-->
<!--                        <tbody>-->
<!--                        <tr><td class="legendColorBox"><div style="border:1px solid #000000;padding:1px"><div style="width:4px;height:0;border:5px solid #1ab394;overflow:hidden"></div></div></td><td class="legendLabel">Number of orders</td></tr>-->
<!--                        <tr><td class="legendColorBox"><div style="border:1px solid #000000;padding:1px"><div style="width:4px;height:0;border:5px solid #1C84C6;overflow:hidden"></div>        </div></td><td class="legendLabel">Payments</td></tr>-->
<!--                        </tbody>-->
<!--                    </table>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-2">
                        <h1 class="m-b-xs"> {{totalIncome}}</h1>
                    </div>
                    <div class="col-lg-2">
                        <select class="form-control" name="chart" id="chart" ng-model="chartPeriod" ng-change="changeChart(chartPeriod)">
                            <option value="">Select Period</option>
                            <option value="last-week">Last Week</option>
                            <option value="last-month">Last month</option>
                            <option value="last-six-month">Last 6 Months</option>
                            <option value="last-year">Last year</option>
                        </select>
                    </div>
                    <div class="col-lg-8 text-right">
                        <p>Total Users: {{members}}</p>
                    </div>
                </div>
                <div class="row">
                    <h3 class="font-bold no-margins">
                        Half-year revenue margin
                    </h3>
                    <small>marketing.</small>
                </div>
                <div>
                    <canvas id="bar" class="chart chart-bar"
                            chart-data="data" chart-labels="labels" chart-options="options" chart-series="series"  height="25"
                            width="100">
                    </canvas>
                </div>

                <div class="m-t-md">
                    <small class="pull-right">
                        <i class="fa fa-clock-o"> </i>
                        Update on Today
                    </small>
                    <small>
                        <strong>Analysis of sales:</strong> The value has been changed over time, and last month reached a level over 50,000.
                    </small>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-8">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Check User</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form role="form" class="form-inline" ng-submit="searchReferId()">
                    <div class="form-group">
                        <label for="field" class="sr-only">Refer ID</label>
                        <select name="field" id="field" class="form-control" ng-model="refer.field">
                            <option value="" selected>select</option>
                            <option value="referCode" >Refer Code</option>
                            <option value="email" >Email</option>
                            <option value="phone" >Phone</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email" class="sr-only">Value</label>
                        <input placeholder="Enter Value " id="value" class="form-control" type="text" ng-model="refer.value">
                    </div>
                    <button class="btn btn-white" type="submit">Search</button>
                </form>
                <div class="row">
                    <div class="alert alert-success" ng-show="found">User Found</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Modal form <small>Example of login in modal box</small></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#">Config option 1</a>
                        </li>
                        <li><a href="#">Config option 2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="text-center">
                    <a data-toggle="modal" class="btn btn-primary" href="#modal-form">Form in simple modal box</a>
                </div>
                <div id="modal-form" class="modal fade" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-6 b-r"><h3 class="m-t-none m-b">Sign in</h3>

                                        <p>Sign in today for more expirience.</p>

                                        <form role="form">
                                            <div class="form-group"><label>Email</label> <input placeholder="Enter email" class="form-control" type="email"></div>
                                            <div class="form-group"><label>Password</label> <input placeholder="Password" class="form-control" type="password"></div>
                                            <div>
                                                <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Log in</strong></button>
                                                <label> <div class="icheckbox_square-green" style="position: relative;"><input class="i-checks" style="position: absolute; opacity: 0;" type="checkbox"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div> Remember me </label>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-sm-6"><h4>Not a member?</h4>
                                        <p>You can create an account:</p>
                                        <p class="text-center">
                                            <a href=""><i class="fa fa-sign-in big-icon"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>