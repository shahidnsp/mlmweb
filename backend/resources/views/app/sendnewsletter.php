<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Send News Letter</h2>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
              Send News Letters
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" ng-submit="sendNewsLetter();">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="text" ng-model="subject" class="form-control" placeholder="Subject"/>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <!--<text-angular ng-model="message"></text-angular>-->
                            <div ckeditor="options" ng-model="message"></div>
                        </div>
                    </div>
                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-w-m btn-primary">Save</button>
                        <button ng-click="cancelNewsLetter();" class="btn btn-w-m btn-info">Cancel</button>
                    </div>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>


