<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Partners</h2>
    </div>
</div>

<div class="row">
    <div ng-class="{'col-sm-8' : sideBox == true,'col-sm-12' : sideBox == false}">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row" ng-show="showForm">
                    <form class="form-horizontal" ng-submit="updateUser();">
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Name</label>
                            <div class="col-sm-4" ng-class="{'has-error' : validationErrors.name}">
                                <input ng-model="newuser.name" type="text" placeholder="Name" class="form-control">
                                <span class="help-block" ng-if="validationErrors.name"><small>{{validationErrors.name[0]}}</small></span>
                            </div>

                            <label class="col-sm-1 control-label">Email</label>
                            <div class="col-sm-6" ng-class="{'has-error' : validationErrors.email}">
                                <input ng-model="newuser.email" type="email" placeholder="Email Address" class="form-control">
                                <span class="help-block" ng-if="validationErrors.email"><small>{{validationErrors.email[0]}}</small></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Address</label>
                            <div class="col-sm-4" ng-class="{'has-error' : validationErrors.address}">
                                <input ng-model="newuser.address" type="text" placeholder="Address" class="form-control">
                                <span class="help-block" ng-if="validationErrors.address"><small>{{validationErrors.address[0]}}</small></span>

                            </div>
                            <label class="col-sm-1 control-label">Place</label>
                            <div class="col-sm-6" ng-class="{'has-error' : validationErrors.place}">
                                <input ng-model="newuser.place" type="text" placeholder="Place" class="form-control">
                                <span class="help-block" ng-if="validationErrors.place"><small>{{validationErrors.place[0]}}</small></span>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label">Phone</label>
                            <div class="col-sm-4" ng-class="{'has-error' : validationErrors.phone}">
                                <input ng-model="newuser.phone" type="text" placeholder="Phone Number" class="form-control">
                                <span class="help-block" ng-if="validationErrors.phone"><small>{{validationErrors.phone[0]}}</small></span>

                            </div>
                            <label class="col-sm-1 control-label">State</label>
                            <div class="col-sm-6" ng-class="{'has-error' : validationErrors.state}">
                                <input ng-model="newuser.state" type="text" placeholder="State" class="form-control">
                                <span class="help-block" ng-if="validationErrors.state"><small>{{validationErrors.state[0]}}</small></span>

                            </div>
                        </div>
                        <div class="form-group">
                            <!--                        <label class="col-sm-1 control-label">Username</label>-->
                            <!--                        <div class="col-sm-4">-->
                            <!--                            <input ng-model="newuser.username" type="text" placeholder="Username" class="form-control">-->
                            <!--                        </div>-->
                            <label class="col-sm-1 control-label">Password</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <input class="form-control" type="text" ng-model="newuser.password"> <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary" ng-click="newuser.password = createPassword()">Generate Password
                                </button>
                            </span>
                                </div>
                            </div>

                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button ng-click="hideForm()" type="button" class="btn btn-danger">Cancel</button>
                        </div>
                        <br/>
                    </form>
                </div>



                <span class="text-muted small pull-right">Last added: <i class="fa fa-clock-o"></i> 2:10 pm - 12.06.2014</span>
                <h2>Partners</h2>
                <p>
                    All Partners need to be verified before you can send email and set a project.
                </p>
                <div class="input-group">
                    <input placeholder="Search client " class="input form-control" type="text" ng-model="search">
                        <span class="input-group-btn">
                                <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Search</button>
                        </span>
                </div>
                <div class="">
                    <uib-tabset active="active">
                        <uib-tab index="0" heading="Partners">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Partners
                                            <div class="pull-left">
                                                <a ng-click="sort('nodes_count')">
                                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row" dir-paginate="user in users | filter:search | limitTo:users.length | itemsPerPage:numPerPage | orderBy:orderProp:direction" current-page="currentPage" pagination-id="userList">
                                        <td >{{$index+1}}</td>
                                        <td class="client-avatar"><img alt="image" src="img/profile_small.jpg"> </td>
                                        <td><a ng-click="showUser(user)" class="client-link" aria-expanded="true">{{user.name}}</a></td>
                                        <td ><i class="fa fa-phone"> </i> {{user.phone}}</td>
                                        <td>{{user.email}}</td>
                                        <td>{{user.nodes_count}}</td>
                                        <td class="client-status"><span class="label label-primary">Active</span></td>
                                        <td class="client-status"><button class="btn btn-danger btn-xs" ng-click="editUser(user)">Edit</button></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="dataTables_info pagination" role="status" aria-live="polite" ng-if="currentPage == 1">
                                            Showing {{currentPage}} to {{(numPerPage < users.length  ? currentPage*numPerPage :users.length)}} of {{users.length}} entries
                                        </div>
                                        <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                            Showing {{(currentPage-1)*numPerPage+1}} to {{(currentPage*numPerPage)}} of {{users.length}} entries
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="dataTables_length" style="display: inline-flex;">
                                            <p style="width: 150px;line-height: 30px;">Show entries</p>
                                            <select aria-controls="DataTables_Table_0" class="form-control input-sm" ng-model="numPerPage" ng-options="numPage for numPage in itemPerPage"></select>
                                        </div>

                                    </div>
                                    <div class="col-md-4 text-right">
                                        <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">

                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true"
                                                pagination-id="userList">
                                            </dir-pagination-controls>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </uib-tab>
                        <uib-tab index="1" heading="Cordinates">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Partners
                                            <div class="pull-left">
                                                <a ng-click="sort('nodes_count')">
                                                    <i class="fa fa-sort" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr role="row" dir-paginate="executive in executives | filter:search | limitTo:executives.length | itemsPerPage:numPerPage | orderBy:orderProp:direction" current-page="currentPage" pagination-id="executiveList">
                                        <td>{{$index+1}} </td>
                                        <td class="client-avatar"><img alt="image" src="img/profile_small.jpg"> </td>
                                        <td><a data-toggle="tab" href="#contact-1" class="client-link" aria-expanded="true">{{executive.name}}</a></td>
                                        <td ><i class="fa fa-phone"> </i> {{executive.phone}}</td>
                                        <td>{{executive.email}}</td>
                                        <td>{{executive.nodes_count}}</td>
                                        <td class="client-status"><span class="label label-primary">Active</span></td>
                                        <td class="client-status"><button class="btn btn-danger btn-xs" ng-click="editUser(executive)">Edit</button></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="dataTables_info pagination" role="status" aria-live="polite" ng-if="currentPage == 1">
                                            Showing {{currentPage}} to {{(numPerPage < executives.length  ? currentPage*numPerPage :executives.length)}} of {{executives.length}} entries
                                        </div>
                                        <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                            Showing {{(currentPage-1)*numPerPage+1}} to {{(currentPage*numPerPage)}} of {{executives.length}} entries
                                        </div>
                                    </div>

                                    <div class="col-md-3 text-right">
                                        <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                            <dir-pagination-controls
                                                max-size="5"
                                                direction-links="true"
                                                boundary-links="true"
                                                pagination-id="executiveList">
                                            </dir-pagination-controls>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </uib-tab>
                    </uib-tabset>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4" ng-show="sideBox">
        <div class="ibox">
            <div class="ibox-content">
                <div class="ibox-tools">
                    <a class="close-link" ng-click="sideBox=false">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="tab-content">
                    <div class="">
                        <div class="row m-b-lg">
                            <div class="col-lg-4 text-center">
                                <h2>{{user.name}}</h2>
                                <div class="m-b-sm">
                                    <img alt="image" class="img-circle" src="img/profile_small.jpg" style="width: 62px">
                                </div>
                            </div>
                            <div class="col-lg-8"  ng-show="userdetails.directpartners > 10">
                                <button type="button" class="btn btn-primary btn-sm btn-block"><i class="fa fa-envelope"></i> Upgrade to Coordinator
                                </button>
                            </div>
                        </div>
                        <div class="client-detail">
                            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">

                                    <strong>Last activity</strong>

                                    <ul class="list-group clear-list">
                                        <li class="list-group-item fist-item">
                                            <span class="pull-right"> {{userdetails.directpartners}} </span>
                                            Direct Partners
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right"> {{userdetails.totalpartners}}</span>
                                            All Partners
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right"> <strong>{{userdetails.totalpartners}}</strong></span>
                                            Available Balance
                                        </li>

                                        <li class="list-group-item">
                                            <span class="pull-right"> {{userdetails.totalpartners}}</span>
                                            <a uib-popover-template="'template/withdrawDetails-tpl'" popover-title="details" >Total Withdrawal Amount</a>
                                        </li>
                                        <li class="list-group-item">
                                            <span class="pull-right"> {{userdetails.income}}</span>
                                            Total Income
                                        </li>

                                        <li class="list-group-item">
                                            <span class="pull-right"> {{userdetails.totalpartners}}</span>
                                            Ready to withdraw
                                        </li>
                                    </ul>
                                    <hr>
                                    <strong>Partners Details</strong>
                                    <div id="vertical-timeline" class="vertical-container dark-timeline">
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-coffee"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>Many desktop publishing packages and web page editors now use Lorem.
                                                </p>
                                                <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014 </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-bolt"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>There are many variations of passages of Lorem Ipsum available.
                                                </p>
                                                <span class="vertical-date small text-muted"> 06:10 pm - 11.03.2014 </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon navy-bg">
                                                <i class="fa fa-warning"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>The generated Lorem Ipsum is therefore.
                                                </p>
                                                <span class="vertical-date small text-muted"> 02:50 pm - 03.10.2014 </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-coffee"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>Conference on the sales results for the previous year.
                                                </p>
                                                <span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014 </span>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-block">
                                            <div class="vertical-timeline-icon gray-bg">
                                                <i class="fa fa-briefcase"></i>
                                            </div>
                                            <div class="vertical-timeline-content">
                                                <p>Many desktop publishing packages and web page editors now use Lorem.
                                                </p>
                                                <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014 </span>
                                            </div>
                                        </div>
                                    </div>
                                </div><div class="slimScrollBar" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 346.306px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
