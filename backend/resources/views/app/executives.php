<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-xs-6">
        <h2>Executives</h2>

        <!--<ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Forms</a>
            </li>
            <li class="active">
                <strong>PDF viewer</strong>
            </li>
        </ol>-->
    </div>
    <div class="col-xs-6">
        <br/>
        <button ng-hide="executiveedit" ng-click="newExecutive()" type="button" class="pull-right btn btn-w-m btn-success">Add New</button>
    </div>
</div>

<div ng-show="executiveedit" class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
               New Executive
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" ng-submit="addExecutive();">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Name</label>
                        <div class="col-sm-4" ng-class="{'has-error' : validationErrors.name}">
                            <input ng-model="newexecutive.name" type="text" placeholder="Name" class="form-control">
                            <span class="help-block" ng-if="validationErrors.name"><small>{{validationErrors.name[0]}}</small></span>
                        </div>

                        <label class="col-sm-1 control-label">Email</label>
                        <div class="col-sm-6" ng-class="{'has-error' : validationErrors.email}">
                            <input ng-model="newexecutive.email" type="email" placeholder="Email Address" class="form-control">
                          <span class="help-block" ng-if="validationErrors.email"><small>{{validationErrors.email[0]}}</small></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Address</label>
                        <div class="col-sm-4" ng-class="{'has-error' : validationErrors.address}">
                            <input ng-model="newexecutive.address" type="text" placeholder="Address" class="form-control">
                            <span class="help-block" ng-if="validationErrors.address"><small>{{validationErrors.address[0]}}</small></span>

                        </div>
                        <label class="col-sm-1 control-label">Place</label>
                        <div class="col-sm-6" ng-class="{'has-error' : validationErrors.place}">
                            <input ng-model="newexecutive.place" type="text" placeholder="Place" class="form-control">
                            <span class="help-block" ng-if="validationErrors.place"><small>{{validationErrors.place[0]}}</small></span>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Phone</label>
                        <div class="col-sm-4" ng-class="{'has-error' : validationErrors.phone}">
                            <input ng-model="newexecutive.phone" type="text" placeholder="Phone Number" class="form-control">
                            <span class="help-block" ng-if="validationErrors.phone"><small>{{validationErrors.phone[0]}}</small></span>

                        </div>
                        <label class="col-sm-1 control-label">State</label>
                        <div class="col-sm-6" ng-class="{'has-error' : validationErrors.state}">
                            <input ng-model="newexecutive.state" type="text" placeholder="State" class="form-control">
                            <span class="help-block" ng-if="validationErrors.state"><small>{{validationErrors.state[0]}}</small></span>

                        </div>
                    </div>
                    <div class="form-group">
<!--                        <label class="col-sm-1 control-label">Username</label>-->
<!--                        <div class="col-sm-4">-->
<!--                            <input ng-model="newexecutive.username" type="text" placeholder="Username" class="form-control">-->
<!--                        </div>-->
                        <label class="col-sm-1 control-label">Password</label>
                        <div class="col-sm-6" ng-class="{'has-error' : validationErrors.password}">
<!--                            <input  type="text" placeholder="Password" class="form-control">-->
                            <div class="input-group">
                                <input class="form-control" type="text" ng-model="newexecutive.password">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary" ng-click="newexecutive.password = createPassword()">Generate Password
                                    </button>
                                </span>
                            </div>
                            <span class="help-block" ng-if="validationErrors.password"><small>{{validationErrors.password[0]}}</small></span>
                        </div>

                    </div>
                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-w-m btn-primary">Save</button>
                        <button ng-click="cancelExecutive()" type="button" class="btn btn-w-m btn-info">Cancel</button>
                    </div>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-2">
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        </div>
                        <div class="col-md-10 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Place</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="user in listCount  = (users | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                        <td>{{user.name}}</td>
                                        <td>{{user.username}}</td>
                                        <td>{{user.email}}</td>
                                        <td>{{user.place}}</td>
                                        <td>
                                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                                <button type="button" class="btn btn-primary" ng-click="editExecutive(user);">
                                                    <i class="fa fa-pencil"></i>
                                                </button>
                                                <!--<button ng-if="user.id!='4'" type="button" class="btn btn-danger" ng-click="deleteExecutive(user);">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>-->
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="clearfix" ng-show="users.length > numPerPage">
                                <pagination
                                    ng-model="currentPage"
                                    total-items="listCount.length"
                                    max-size="maxSize"
                                    items-per-page="numPerPage"
                                    boundary-links="true"
                                    class="pagination-sm pull-right"
                                    previous-text="&lsaquo;"
                                    next-text="&rsaquo;"
                                    first-text="&laquo;"
                                    last-text="&raquo;"
                                    ></pagination>
                            </div>
                    </div>
                </div>
        </div>
</div>
