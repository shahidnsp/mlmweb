<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Web Product</h2>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-2">
                            <select class="form-control pagination" ng-model="numPerPage" ng-options="numPage for numPage in itemPerPage" ng-change="changeNum(numPerPage);"></select>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-8">
                                    <select class="form-control" ng-model="search" ng-init="search=''" ng-change="searchOrder(search)">
                                        <option value="">Filter by status</option>
                                        <option value="all">All</option>
                                        <option value="today">Today</option>
                                        <option value="neworder">New order</option>
                                        <option value="packing">Packing</option>
                                        <option value="shipping">Shipping</option>
                                        <option value="delivered">Delivered</option>
                                        <option value="canceled">Canceled</option>
                                    </select>
                                </div>
                                <!--<div class="col-md-4">
                                    <button class="btn btn-default" ng-click="searchOrder(search)">Search</button>
                                </div>-->
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <button class="btn btn-info btn-sm" ng-click="exportToPdf(search)">Pdf</button>
                            <button class="btn btn-info btn-sm" ng-click="printAll(search)">Print all Invoice</button>
                        </div>
                        <div class="col-md-4 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTables-example table-responsive" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User</th>
                                    <th>Email</th>
                                    <th>Product Code</th>
                                    <th>Transaction No</th>
                                    <th>Date</th>
                                    <th>amount</th>
                                    <th>Address</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="order in listCount  = (orders | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage" ng-class="{'text-info' : order.order_status == 'delivered','text-danger' : order.order_status == 'canceled','text-primary' : order.order_status == 'packing','text-success' : order.order_status == 'shipping'}">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td><a href="">{{order.user.name}}</a></td>
                                    <td>{{order.user.email}}</td>
                                    <td>{{order.order_items[0].product.name}} (<strong>{{order.order_items[0].product.pdctcode}}</strong>)</td>
                                    <td>{{order.transaction_id}}</td>
                                    <td>{{order.date}}</td>
                                    <td>{{order.amount}}</td>
                                    <td><strong>{{order.user.name}}, {{order.pickup_address.address}},PH:{{order.pickup_address.phone}}</strong></td>
                                    <td>{{order.order_status}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Action <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li><a class="disabled" ng-click="orderStatusUpdate(order,'packing')">packing</a></li>
                                                <li><a ng-click="orderStatusUpdate(order,'shipping')">Shipping</a></li>
                                                <li><a ng-click="orderStatusUpdate(order,'delivered')">Delivered</a></li>
                                                <li class="divider"></li>
                                                <li><a ng-click="orderStatusUpdate(order,'canceled')" class="text-danger">Cancel</a></li>
                                            </ul>
                                            <a ng-click="printInvoice(order.invoice.id)"><i class="fa fa-print" aria-hidden="true"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix" ng-show="orders.length > numPerPage">
                                <pagination
                                    ng-model="currentPage"
                                    total-items="listCount.length"
                                    max-size="maxSize"
                                    items-per-page="numPerPage"
                                    boundary-links="true"
                                    class="pagination-sm pull-right"
                                    previous-text="&lsaquo;"
                                    next-text="&rsaquo;"
                                    first-text="&laquo;"
                                    last-text="&raquo;"
                                    ></pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
