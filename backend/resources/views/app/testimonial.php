<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Web Testimonial</h2>
    </div>
    <div class="col-lg-2">
        <br/>
        <button ng-hide="testimonialedit" ng-click="newTestimonial()" type="button" class="pull-right btn btn-w-m btn-success">Add New</button>
    </div>
</div>

<div ng-show="testimonialedit" class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                New Testimonial
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" ng-submit="addTestimonial();">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Name</label>
                        <div class="col-sm-11" ng-class="{'has-error' : validationErrors.name}">
                            <input ng-model="newtestimonial.name" type="text" placeholder="Name" class="form-control" >
                            <span class="help-block" ng-if="validationErrors.name"><small>{{validationErrors.name[0]}}</small></span>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-1 control-label">Designation</label>

                        <div class="col-sm-11" ng-class="{'has-error' : validationErrors.designation}">
                            <input ng-model="newtestimonial.designation" type="text" placeholder="Designation" class="form-control" >
                            <span class="help-block" ng-if="validationErrors.designation"><small>{{validationErrors.designation[0]}}</small></span>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-1 control-label">Photo</label>
                        <div class="col-sm-11">
                            <input class="form-control" accept="image/*" ng-file-model="newtestimonial.photos" type="file" multiple/>
                        </div>
                    </div>
                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-w-m btn-primary">Save</button>
                        <button ng-click="cancelTestimonial();" class="btn btn-w-m btn-info">Cancel</button>
                    </div>
                    <br/>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-2">
                            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
                        </div>
                        <div class="col-md-10 text-right">
                            <div class="form-inline form-group">
                                <label for="filter-list">Search </label>
                                <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
                            </div>
                        </div>
                        <br/><br/>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Designation</th>
                                    <th>Photo</th>
                                    <th>Active</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="testimonial in listCount  = (testimonials | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                                    <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                                    <td>{{testimonial.name}}</td>
                                    <td>{{testimonial.designation}}</td>
                                    <td>
                                        <img src="images/{{testimonial.photo}}" style="width: 50px;height: 40px;" class="img-responsive"/>
                                    </td>
                                    <td>
                                        <button ng-if="testimonial.active==1" type="button" class="btn btn-success" ng-click="changeTestimonialStatus(testimonial);">
                                            Active
                                        </button>
                                        <button ng-if="testimonial.active==0" type="button" class="btn btn-info" ng-click="changeTestimonialStatus(testimonial);">
                                            InActive
                                        </button>
                                    </td>
                                    <td>
                                        <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                            <button type="button" class="btn btn-primary" ng-click="editTestimonial(testimonial);">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger" ng-click="deleteTestimonial(testimonial);">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="clearfix" ng-show="testimonials.length > numPerPage">
                                <pagination
                                    ng-model="currentPage"
                                    total-items="listCount.length"
                                    max-size="maxSize"
                                    items-per-page="numPerPage"
                                    boundary-links="true"
                                    class="pagination-sm pull-right"
                                    previous-text="&lsaquo;"
                                    next-text="&rsaquo;"
                                    first-text="&laquo;"
                                    last-text="&raquo;"
                                    ></pagination>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
