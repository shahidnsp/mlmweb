app.controller('ExecutiveController', function($scope,$http,$anchorScroll,ngNotify,User,randomString){

    $scope.executiveedit=false;
    $scope.users=[];

    User.query({'type': 'executive'}, function (user) {
        $scope.users = user;
        console.log(user);
    });


    $scope.newExecutive = function () {
        $scope.executiveedit = true;
        $scope.newexecutive = new User();
        $scope.curExecutive = {};
    };

    $scope.editExecutive = function (thisExecutive) {
        $scope.executiveedit = true;
        $scope.curExecutive =  thisExecutive;
        $scope.newexecutive = angular.copy(thisExecutive);
        $anchorScroll();
    };

    $scope.addExecutive = function () {
        //console.log($scope.newexecutive);
        $scope.newexecutive.usertype = 'executive';

        if ($scope.curExecutive.id) {
            $scope.newexecutive.$update(function(executive){
                angular.extend($scope.curExecutive, $scope.curExecutive, executive);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Executive Updated Successfully');
            });
        } else{
            $scope.newexecutive.username = $scope.newexecutive.email;
            $scope.newexecutive.$save().
                then(function (response) {
                    $scope.users.push(response);
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });
                    ngNotify.set('Executive Saved Successfully');
                    $scope.executiveedit = false;
                }, function (response) {
                    $scope.validationErrors = response.data;
                });
        }

        $scope.newexecutive = new User();
    };

    $scope.deleteExecutive = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.users.indexOf(item);
                $scope.users.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Executive Removed Successfully');
            });
        }
    };

    $scope.cancelExecutive=function(){
        $scope.executiveedit=false;
        $scope.newexecutive = new User();
        $scope.validationErrors = {};
    };

    /**
     * Create random password
     **/
    $scope.createPassword = function () {
        return randomString(8);
    };


});