app.controller('SendNewsLetterController', function($scope,$http,ngNotify){
    $scope.message='';
    $scope.subject='';

    // Editor options.
    $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false
    };

    $scope.sendNewsLetter=function(){
        $http.post('/app/sendNewsLetter',{message: $scope.message,subject:$scope.subject}).
            success(function(data,status,headers,config){
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('News Letter Sent Successfully');
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.cancelNewsLetter=function(){
        $scope.message='';
        $scope.subject='';
    };
});