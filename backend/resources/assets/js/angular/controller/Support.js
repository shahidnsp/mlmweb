app.controller('SupportController', function($scope,$http,$anchorScroll,ngNotify,Support){

    $scope.supportedit=false;
    $scope.supports=[];

    Support.query(function(support){
        $scope.supports=support;
        console.log(support);
    });

    $scope.deleteSupport = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.supports.indexOf(item);
                $scope.supports.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Support Removed Successfully');
            });
        }
    };


    $scope.changeSupportStatus=function(support){
        $http.post('/app/changeSupportStatus',{id:support.id}).
            success(function(data,status,headers,config){
                angular.extend(support,support,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };


});