app.controller('AppUserController', function($scope,$http,$anchorScroll,ngNotify,User,$uibModal, $log, $document,$ngConfirm){

    $scope.useredit=false;
    $scope.users=[];

    User.query({type: 'user'}, function (user) {
        $scope.users = user;
    });

    User.query({type: 'executive'}, function (executive) {
        $scope.executives = executive;
    });


    $scope.deleteUser = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.users.indexOf(item);
                $scope.users.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('User Removed Successfully');
            });
        }
    };


    $scope.changeUserStatus=function(user){
        $http.post('/app/changeUserStatus',{id:user.id}).
            success(function(data,status,headers,config){
                angular.extend(user,user,data);
            }).error(function(data,status,headers,config){
                console.log(data);
            });
    };

    $scope.openUser = function (item) {
        $http.post('/app/getUserDetails', {id: item.id})
            .success(function (data, status, headers, config) {
                angular.extend(user, user, data);
            })
            .error(function (data, status, headers, config) {
                console.log(data);
            });
        $scope.modalitem = item;
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'template/showUser',
            scope: $scope,
            size: 'lg',
            backdrop: 'static'
        });
    };

    $scope.close = function () {
        $scope.modalInstance.dismiss();
    };

    $scope.showUser=function(item) {
        $scope.sideBox = true;
        $scope.user = item;
        $http.post('/app/getUserDetails', {id: item.id})
            .success(function (data, status, headers, config) {
                $scope.userdetails = data;
            })
            .error(function (data, status, headers, config) {
                console.log(data);
            });
    };

    $scope.dynamicPopover = {
        content: 'Hello, World!',
        templateUrl: 'myPopoverTemplate.html',
        title: 'Title'
    };

    $scope.editUser = function (user) {
        $scope.showForm = true;
        $scope.curUser = user;
        $scope.newuser = angular.copy(user);
        $anchorScroll();
    };

    $scope.updateUser = function () {
        $scope.newuser.$update(function (user) {
            angular.extend($scope.curUser, $scope.curUser, user);
            $scope.showForm = false;
            $ngConfirm({
                title: 'Message',
                content: 'User Updates',
                escapeKey: true,
                autoClose: 'close|5000',
                buttons: {
                    close: function () {

                    }
                }
            });
        });
    };


    $scope.hideForm=function() {
        $scope.showForm = false;
        $scope.neweuser = new User();
    };

});