/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('ContactService',[]).factory('Contact',['$resource',
    function($resource){
        return $resource('/app/contact/:contactId',{
            contactId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);