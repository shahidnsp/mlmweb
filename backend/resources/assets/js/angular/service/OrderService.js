/**
 * Created by psybo-03 on 18/1/18.
 */

angular.module('OrderService',[]).factory('Order',['$resource',
    function($resource){
        return $resource('/app/order/:orderId',{
            orderId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);