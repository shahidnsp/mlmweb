/**
 * Created by Shahid Neermunda on 8/1/18.
 */

angular.module('SupportService',[]).factory('Support',['$resource',
    function($resource){
        return $resource('/app/support/:supportId',{
            supportId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);