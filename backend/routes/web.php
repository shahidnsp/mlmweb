<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        $user=\Illuminate\Support\Facades\Auth::user();
        if($user->usertype=='Admin')
            return view('app.index');
        else
            return view('web.index');
    }
    else{
        return view('web.index');
    }
});

Route::get('images/{filename}', function ($filename)
{
    if (\Illuminate\Support\Facades\Storage::exists($filename)) {
        $file = \Illuminate\Support\Facades\Storage::get($filename);
        return response($file, 200)->header('Content-Type', 'image/jpeg');
    }
    //return base64_encode($file);
});

Route::get('/mytest/{id}', function ($id)
{
    /*$node=\App\Node::with('user')->findOrfail($id);
    if($node) {
        $lists=[];
        $lists['name']=$node->user->name;
        $joints = \App\Joint::where('node_id', $id)->with('child.user')->get();
        $childs=[];
        foreach ($joints as $joint) {

            $level3=[];
            $sectionThirds = \App\Joint::where('parentnode_id', $joint->node_id)->with('child.user')->get();
            $thirdChilds=[];
            foreach($sectionThirds as $sectionThird){
                $level4=[];
                $section4s = \App\Joint::where('parentnode_id', $sectionThird->node_id)->with('child.user')->get();
                $fourthChilds=[];
                foreach($section4s as $section4){
                    array_push($fourthChilds,$section4->child->user);
                }
                $level4=$sectionThird->child->user;
                $level4['childs']=$fourthChilds;
                array_push($thirdChilds,$level4);
            }
            $level3=$joint->child->user;
            $level3['childs']=$thirdChilds;
            array_push($childs,$level3);
        }
        $lists['childs']=$childs;

        return $lists;
    }*/
    /*$lists='#1#2#3#4#5#6#7#8#9#10';
    $subLists = explode("#", $lists);
    $revered=array_reverse($subLists);
    $myLists=[];
    foreach($revered as $index => $revere){

        if($revere==''){
            array_push($myLists,'NULL');
            continue;
        }
        array_push($myLists,$revere);
    }

    return $myLists;*/

   $result = \RobinCSamuel\LaravelMsg91\Facades\LaravelMsg91::message('9633720788', 'Thank you.Your Username : Shahid And Password : 1234');

    var_dump($result);
});



//TODO add middleware to authenticate
//Route::group(['middleware'=>'auth','prefix'=>'api'],function(){
Route::group(['prefix' => 'app'], function () {
    //Rest resources
    Route::resource('slider', 'Auth\SliderController');
    Route::resource('member', 'Auth\MemberController');
    Route::resource('product', 'Auth\ProductController');
    Route::resource('testimonial', 'Auth\TestimonialController');
    Route::resource('contact', 'Auth\ContactController');
    Route::resource('support', 'Auth\SupportController');
    Route::resource('newsletter', 'Auth\NewsletterController');
    Route::resource('order', 'Auth\OrderController');
    Route::resource('user', 'Auth\UserController');


    Route::post('changeSliderOrder', 'Auth\SliderController@changeSliderOrder');
    Route::post('changeProductStatus', 'Auth\ProductController@changeProductStatus');
    Route::post('changeTestimonialStatus', 'Auth\TestimonialController@changeTestimonialStatus');
    Route::post('changeContactStatus', 'Auth\ContactController@changeContactStatus');
    Route::post('changeSupportStatus', 'Auth\SupportController@changeSupportStatus');
    Route::post('changeNewsLetterStatus', 'Auth\NewsletterController@changeNewsLetterStatus');
    Route::post('changeUserStatus', 'Auth\UserController@changeUserStatus');
    Route::post('changeOrderStatus', 'Auth\OrderController@changeOrderStatus');
    Route::post('searchOrder', 'Auth\OrderController@searchOrder');
    Route::post('sendNewsLetter', 'Auth\NewsletterController@sendNewsLetter');

    Route::get('countUser', 'Auth\UserController@countUser');
    Route::get('Today_Users', 'Auth\UserController@Today_Users');

    Route::get('Order', 'Auth\OrderController@Order');
    Route::get('Todays_orders', 'Auth\OrderController@Todays_orders');

    Route::post('order/exportToPdf', 'Auth\OrderController@exportToPdf');
    Route::post('product/exportToPdf', 'Auth\ProductController@exportToPdf');
    Route::post('searchReferId', 'Auth\DashboardController@searchReferId');

    Route::get('utility', 'Auth\DashboardController@utility');

    Route::post('getUserDetails', 'Auth\UserController@getDetails');
    Route::post('invoice/pdf', 'Auth\OrderController@invoicePdf');
    Route::post('order/printAll', 'Auth\OrderController@printAll');

    Route::post('chartData', 'Auth\DashboardController@chartData');



});


Route::group(['prefix' => 'web'], function () {

    Route::post('addmysubscription', 'Auth\NewsletterController@addMySubscription')->name('addmysubscription');
    Route::post('sendmessage', 'Auth\ContactController@sendMessage')->name('sendmessage');
    Route::post('loadProducts', 'Auth\ProductController@loadProducts');
    /*old start*/
    Route::post('proceedToPayment', 'Auth\OrderController@proceedToPayment')->name('proceedToPayment');
    Route::post('proceedToPayment/validate', 'Auth\OrderController@validateInput');
    /*end*/

    Route::post('userRegister', 'HomeController@register')->name('userRegister');
    Route::post('userRegister/validate', 'HomeController@validateInput');
    Route::get('userRegister/cancel', 'HomeController@cancelRegistration');

});

Route::post('wayonn/success_response', 'Auth\OrderController@successResponse');
Route::post('wayonn/failure_response', 'Auth\OrderController@failureResponse');


Route::get('/payment_failed', function () {
//    if(\Illuminate\Support\Facades\Auth::check()){
//        $user=\Illuminate\Support\Facades\Auth::user();
//        if($user->usertype=='user')
//            return view('web.payment_failed');
//        else
//            return view('web.index');
//    }
//    else{
//        return view('web.index');
//    }

    $data['status'] = false;
    $payment['payment'] = $data;

    session()->forget('user');
    session()->forget('join');
    session()->forget('newuser');
    return redirect('products')->with($payment);
});

Route::get('/payment_success', function () {
    /*if(\Illuminate\Support\Facades\Auth::check()){
        $user=\Illuminate\Support\Facades\Auth::user();
        if($user->usertype=='user')
            return view('web.payment_success');
        else
            return view('web.index');
    }
    else{
        return view('web.index');
    }*/

    if (session()->has('status') and session()->get('status') == false) {
        $data['status'] = false;
    }else{
        $data['status'] = true;

    }
    $data['phone'] = session('user.phone');
    $data['email'] = session('user.email');
    $data['invoice'] = session('invoice');

    $payment['payment'] = $data;

    session()->forget('user');
    session()->forget('join');
    session()->forget('newuser');
    session()->forget('invoice');
    return redirect('products')->with($payment);
});


//Load angular templates
//TODO user permission validation
Route::get('template/{name}', ['as' => 'templates', function ($name) {
    return view('app.'.$name);
}]);





Route::get('/home', 'HomeController@index')->name('home');
Route::get('web/home', 'HomeController@index');


//WEB PAGE ROUTE
Route::get('/index', function () {
    return view('web.index');
});
Route::get('/about-us', function () {
    return view('web.about');
});

Route::get('/wayonnApplication', function () {
    return view('web.applanding');
});

Route::get('/wayonnPolicy', function () {
    return view('web.wayonnPolicy');
});

Route::get('/ourbusiness', function () {
    return view('web.business');
});

Route::get('/join', 'HomeController@join')->name('join');

Route::get('/products', function () {

    $products=\App\Product::orderBy('created_at','DESC')->with('files')->limit(8)->get();

    return view('web.products',compact('products'));
});

Route::get('/single-product/{id}', function ($id) {
    $product=\App\Product::findOrfail($id);
    if($product) {
        $sharelink=Share::load(url('/single-product/'.$id), 'Shop and Earn')->services('twitter', 'facebook', 'pinterest','gplus','linkedin');
        return view('web.single-product', compact('product','sharelink'));
    }
    else
        return view('web.index');
});

Route::get('/contactus', function () {
    return view('web.contactus');
});

Route::get('/checkout', 'CheckoutController@index');
Route::get('/purchase/{id}', 'CheckoutController@purchase');

Route::post('send-otp', ['as' => 'otp.send', 'uses' => 'HomeController@sendOtp']);
Route::post('verify-otp', ['as' => 'otp.verify', 'uses' => 'HomeController@verifyOtp']);

Route::post('refcodeverify', ['as' => 'refer.verify', 'uses' => 'HomeController@refCodeVerify']);





Route::get('/faq', function () {
    return view('web.faq');
});


//Route::get('join/', 'HomeController@join');

Route::get('verify-email/{code}', 'HomeController@verifyEmail');
Route::get('logout', function () {
    return redirect('/');
});

Route::get('/reset-session/{value}', 'HomeController@resetSession');

Route::get('way8089/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('invoice/create/{invoice_id}', 'HomeController@createInvoicePdf');

Route::get('invoice/createfull', 'HomeController@createInvoicePdfFull');
Route::get('invoice/view', 'HomeController@viewInvoice');

Route::get('/test', 'HomeController@test');
Route::get('/reset-session/{value}', 'HomeController@resetSession');


Auth::routes();