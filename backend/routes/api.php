<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Current
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('login', 'API\PassportController@login');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'API\PassportController@logout');
    Route::post('get-details', 'API\PassportController@getDetails');
    Route::post('change-password', 'API\PassportController@changePassword');
    Route::get('getmynodelist', 'API\PassportController@getMyNodeList');
    Route::post('getnodedetails', 'API\PassportController@getNodeDetails');

    Route::post('get-child', 'API\PassportController@getChild');

    Route::get('profile', 'API\PassportController@profile');
    Route::post('update-profile', 'API\PassportController@UpdateProfile');
    Route::post('upload-profile', 'API\PassportController@uploadProfile');
});


